#ProjectZero Main Repository#

Welcome to the ProjectZero repository.

!! Warning : this project is not finished (yet), it wil require at least several months before an acceptable state !!
I am currently alone working on this, and I'm working on it by small burst, so it will be long !
That's why this project is not yet "public", I wanted to finish the language part before presenting it.


The first goal of this project was to test out if a program could be self sufficient by writing its own code (classes and interfaces) with commonly spoken language (english for now)
So the main goal is... an AI !!

For example, if the user inputs "This is a car, it has wheels" :

* The program will know that "car" should be a class, represented by instances stored in a database (DONE)
* The program will check if the class "car" exists, if not, creating it (TODO)
* The program will check the class trying to find a member "wheel", if not, creating it (TODO)

###For now :###

* The program is able to write its own class/interface, but not by commonly spoken language (only by using the provided script engine)
* Find a lot of useful infos in a sentence (like mails, names, ...), tokenizing them for a further use (eg. check if data is correct in a class instance)
* Documentation is partial, but should be usable
* A lot of specific subsystems done, like an archive manager, a dedicated script system(JavaScript) with custom contexts, a REAL bash-like command line interface (auto completion soon)
* A powerfull equation parser (why not), with a guaranteed precision of 200 decimals, with specific implementation of trigonometric methods

###TODO :###

* Maybe a complete rewrite of the language analysis part
* Documentation !!!

###Long term goals :###

* Have the program being able to take decisions by itslef, depending on precomputed informations
* Finish the language analysis !!!


If you want to help me on this project, contact me by any means

mail : guillaume (dot) gravetot (at) free (dot) fr