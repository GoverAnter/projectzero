function main() {
  var ProgramImport = new JavaImporter(
      Packages.xx.projectzero.core.ui.MainViewController.MVTabs, //enum for io.print method
      Packages.xx.projectzero.sourcewriter.ClassWriter.ClassType, //enum for classWriter.Write* methods
      Packages.xx.projectzero.sourcewriter.ClassWriter.ClassTemplate, //object for classWriter.Write* methods
      Packages.xx.projectzero.sourcewriter.ClassWriter.MemberTemplate, //object for classWriter.Write* methods
      Packages.xx.projectzero.sourcewriter.ClassWriter.MethodTemplate, //object for classWriter.Write* methods
      Packages.java.lang.reflect.Modifier,
      Packages.java.util.ArrayList);

  with (ProgramImport) {
    io.printlntotab("Creating a class from script", MVTabs.Console);

    var Members = new ArrayList();
    Members.add(new MemberTemplate("TestMember1", Modifier.PUBLIC, 12));
    Members.add(new MemberTemplate("TestMember2", Modifier.PUBLIC, 10));
    
    var methods = new ArrayList();

    var parentclass = java.lang.Object.class;
    var IParent = new ArrayList();
    IParent.add(Packages.xx.projectzero.learning.core.MasterClass.class);

    var ct = new ClassTemplate("xx.projectzero.learning.classes.ScriptWritedClass", Members, methods, parentclass, IParent, false);

    classWriter.WriteClass(ct, ClassType.Class, false);

    io.printlntotab("class created", MVTabs.Console);
  }
}