package xx.projectzero.console;

public class Command
{
	private String commandName;
	
	public Command(String Name)
	{
		commandName = Name;
	}
	
	public String CommandName()
	{
		return commandName;
	}
	
	/** You need to override this method at object declaration with anonymous declaration */
	public void Exec(String[] params) {}
}
