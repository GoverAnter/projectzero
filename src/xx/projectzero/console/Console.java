package xx.projectzero.console;

import java.util.ArrayList;
import java.util.TreeMap;

import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.Utils;
import xx.projectzero.core.ui.MainViewController.MVTabs;

/**
 * Class managing events.
 * @author Guillaume Gravetot
 * @version 1.0.0, 20/06/2016
 * @since 1.1.3, 20/06/2016
 */
public class Console
{
	private ArrayList<String> History;
	private int CurrentHistoryPos;
	private TreeMap<String,Command> Commands;
	
	public Console()
	{
		History = new ArrayList<String>();
		History.add("");
		ResetHistoryPos();
		
		Commands = new TreeMap<String,Command>();
		
		AddCommand(new Command("list") {
			@Override
			public void Exec(String[] params)
			{
				if(params.length == 0)
					ProgramCore.io().printlntotab(SerializeAllCommands(), MVTabs.Console);
				else if(Utils.ArrayContains(params,"-h") || Utils.ArrayContains(params,"--help"))
					PrintHelp();
				else if(Utils.ArrayContains(params,"-a") || Utils.ArrayContains(params,"--all"))
					ProgramCore.io().printlntotab(SerializeAllCommands(), MVTabs.Console);
			}
			
			private void PrintHelp()
			{
				ProgramCore.io().printlntotab("Help text for list command.\n\nlist [-options]\n\nOptions :\n\t-h, --help : Display this help. Ignores other options.\n\t-a, --all : Lists all commands. Similar to no parameters.\n", MVTabs.Console);
			}
			
			private String SerializeAllCommands()
			{
				String[] commands = ProgramCore.GetConsole().GetCommands();
				StringBuilder b = new StringBuilder();
				b.append("List of all commands :\n");
				
				for(String string : commands)
				{
					b.append(string+"\n");
				}
				
				return b.toString();
			}
		});
		
		AddCommand(new Command("program") {
			@Override
			public void Exec(String[] params)
			{
				if(params.length == 0)
					PrintHelp();
				else if(Utils.ArrayContains(params,"-h") || Utils.ArrayContains(params,"--help"))
					PrintHelp();
				else if(Utils.ArrayContains(params,"-V") || Utils.ArrayContains(params,"--Version"))
					ProgramCore.io().printlntotab("ProjectZero version " + ProgramCore.Version, MVTabs.Console);
			}
			
			public void PrintHelp()
			{
				ProgramCore.io().printlntotab("Help text for program command.\n\nprogram [-options]\n\nOptions :\n\t-h, --help : Display this help. Ignores other options.\n\t-V, --Version : Display the program version. Ignores other opions.\n", MVTabs.Console);
			}
		});
	}
	
	/** Method to register a new command 
	 * @see Command
	 */
	public void AddCommand(Command command)
	{
		Commands.put(command.CommandName(), command);
	}
	
	/** Internal method : do not use it ! */
	public void Input(String in)
	{
		History.add(History.size()-1, in);
		ResetHistoryPos();
		
		if(in == null || in.length() == 0)
		{
			ProgramCore.io().printlntotab("Command not found", MVTabs.Console);
			return;
		}
		
		String[] s = in.split(" ");
		
		if(Commands.containsKey(s[0]))
		{
			if(s.length > 1)
			{
				String[] p = new String[s.length-1];
				
				for(int i = 1; i < s.length; i++)
					p[i-1] = s[i];
				
				Commands.get(s[0]).Exec(p);
			}
			else
				Commands.get(s[0]).Exec(new String[0]);
		}
	}
	
	/** Internal method : do not use it ! */
	public void ResetHistoryPos()
	{
		CurrentHistoryPos = History.size() - 1;
	}
	
	/** Internal method : do not use it ! */
	public String GetNextHistory()
	{
		if(CurrentHistoryPos-1 == -1)
			return History.get(CurrentHistoryPos);
		
		return History.get(--CurrentHistoryPos);
	}
	
	/** Internal method : do not use it ! */
	public String GetPreviousHistory()
	{
		if(CurrentHistoryPos+1 == History.size())
			return History.get(CurrentHistoryPos);
		
		return History.get(++CurrentHistoryPos);
	}
	
	/** Returns all registered commands. */
	public String[] GetCommands()
	{
		String[] s = new String[Commands.size()];
		int i = 0;
		
		for(String string : Commands.keySet())
		{
			s[i++] = string;
		}
		
		return s;
	}
}
