package xx.projectzero.iomodule;

import xx.projectzero.annotations.JSCallable;
import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.ui.MainViewController.MVTabs;

/**
 * Classe g�rant l'entr�e/sortie et les interactions avec l'utilisateur.
 * @author Guillaume Gravetot
 * @version 1.0.0, 12/04/2016
 * @since 1.0.0, 12/04/2016
 */
public class IOCore
{
	/**
	 * Constructeur principal de la classe.
	 */
	public IOCore() {}

	//TODO Reimplement println to use other types than String(internal treatment to String)
	/**
	 * Affiche le String en argument puis effectue un retour � la ligne.
	 * @param s <b>String</b> : String � afficher.
	 */
	@JSCallable
	public synchronized void println(String s)
	{
		printlntotab(s, MVTabs.Main);
	}
	
	/**
	 * Print the argument String and then adds a line ending.
	 * @param s <b>String</b> : String to print.
	 */
	@JSCallable
	public synchronized void printlntotab(String s, MVTabs tab)
	{
		if(!ProgramCore.GetMainViewController().isTabPanelEmpty(tab))
			s = "\n" + s;
		
		ProgramCore.GetMainViewController().addTextToTab(s, tab);
	}
	
	/**
	 * Print the argument String.
	 * @param s <b>String</b> : String to print.
	 */
	@JSCallable
	public synchronized void printtotab(String s, MVTabs tab)
	{
		ProgramCore.GetMainViewController().addTextToTab(s, tab);
	}
}
