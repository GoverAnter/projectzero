package xx.projectzero.sourcewriter;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.Utils;
import xx.projectzero.core.ErrorReporter.ClassWriterReportObject;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.Utils.ProgramClass;
import xx.projectzero.core.ui.MainViewController.MVTabs;
import xx.projectzero.sourcewriter.ClassWriter.*;

public class ClassWriterWorker implements Runnable
{
	private Thread t;
	private ClassTemplate ct;
	private int chindex;
	
	/** Main constructor of this class. */
	public ClassWriterWorker(ClassTemplate cTemplate, int classHeaderIndex)
	{
		ct = cTemplate;
		chindex = classHeaderIndex;
	}
	
	@Override
	public void run()
	{
		ProgramCore.io().printlntotab("-- WORKER -- Writing class", MVTabs.Writer);
		long startTime = System.nanoTime();
		
		String filePath = ProgramCore.BaseSRCPath+Utils.GetCompletePathFromCompleteName(ct.CompleteName)+".java";
		
		int c = 0;
		StringBuilder argCore = new StringBuilder(); //corps des arguments du constructeur
		StringBuilder CCore = new StringBuilder(); //corps du constructeur
		StringBuilder rmCore = new StringBuilder(); //corps des d�clarations des membres
		StringBuilder tsCore = new StringBuilder(); //corps de la m�thode toString
		StringBuilder liCore = new StringBuilder(); //corps de la m�thode loadInstance
		StringBuilder tCore = new StringBuilder(); //corps de la requete SQL de cr�ation de la table
		tCore.append("InstanceName VARCHAR(255), ");
		StringBuilder setupVars = new StringBuilder(); //corps de l'update de saveCurrentInstance
		StringBuilder vars = new StringBuilder(); //corps de la requete d'update de la methode saveCurrentInstance
		StringBuilder setVars = new StringBuilder(); //corps de l'insert de saveCurrentInstance
		StringBuilder nVal = new StringBuilder(); //corps de la requete d'insert de la methode saveCurrentInstance
		nVal.append("?");
		StringBuilder insVars = new StringBuilder(); //corps des rows d'insertion dans la methode saveCurrentInstance
		insVars.append("InstanceName");
		StringBuilder ssVars = new StringBuilder(); //corps des d�clarations de vars dans safeSaveCurrentInstance
		StringBuilder sttVars = new StringBuilder(); //corps des insertions dans safeSaveCurrentInstance
		
		for(MemberTemplate mt : ct.Members)
		{
			if(c != 0)
			{
				argCore.append(", ");
				vars.append(", ");
				ssVars.append(" AND ");
			}
			
			boolean specObject = !Utils.IsPrimitiveType(mt.Value);
			
			c++;
			
			mt.Type = Utils.GetCorrespondingPrimitiveType(mt.Value);

			argCore.append(mt.Type); argCore.append(" M"); argCore.append(mt.Name);
			CCore.append(Utils.nltt); CCore.append(mt.Name); CCore.append(" = M"); CCore.append(mt.Name); CCore.append(";");
			rmCore.append(Utils.nlt); rmCore.append(Modifier.toString(mt.Modifiers)); rmCore.append(" "); rmCore.append(mt.Type); rmCore.append(" "); rmCore.append(mt.Name); rmCore.append(" = "); rmCore.append(Utils.GetStringFromObject(mt.Value)); rmCore.append(";");
			tsCore.append(Utils.tt); tsCore.append("s += "); tsCore.append("\""); tsCore.append(mt.Name); tsCore.append(", "); tsCore.append(mt.Type); tsCore.append(": \" + Utils.GetStringFromObject("); tsCore.append(mt.Name); tsCore.append(", true) + Utils.nl;\n");
			String str = Utils.GetCorrespondingPrimitiveType(mt.Value, false);
			String cap = str.substring(0, 1).toUpperCase() + str.substring(1);
			liCore.append(Utils.nltttt); liCore.append(mt.Name); liCore.append(specObject?" = (" + mt.Type + ")Utils.Deserialize(":" = "); liCore.append("rs.get"); liCore.append(cap); liCore.append("(\""); liCore.append(mt.Name); liCore.append(specObject?"\"));":"\");");
			tCore.append(mt.Name); tCore.append(" "); tCore.append(Utils.GetSQLType(mt.Value)); tCore.append(", ");
			setupVars.append(Utils.nlttttt); setupVars.append("st.set"); setupVars.append(cap); setupVars.append("("); setupVars.append(c); setupVars.append(", "); setupVars.append(specObject?"Utils.Serialize("+mt.Name+")":mt.Name); setupVars.append(");");
			vars.append(mt.Name); vars.append("=?");
			setVars.append(Utils.nlttttt); setVars.append("st.set"); setVars.append(cap); setVars.append("("); setVars.append(c+1); setVars.append(", "); setVars.append(specObject?"Utils.Serialize("+mt.Name+")":mt.Name); setVars.append(");");
			nVal.append(", ?");
			insVars.append(", "); insVars.append(mt.Name);
			ssVars.append(mt.Name); ssVars.append("=?");
			sttVars.append(Utils.nlttt); sttVars.append("ps.set"); sttVars.append(cap); sttVars.append("("); sttVars.append(c); sttVars.append(", "); sttVars.append(specObject?"Utils.Serialize("+mt.Name+")":mt.Name); sttVars.append(");");
		}
		
		StringBuilder builder = new StringBuilder(3500);
		builder.append("package ");
		builder.append(Utils.GetPackageNameFromCompleteName(ct.CompleteName));
		builder.append(";");
		builder.append(ProgramCore.GetCache().Get(chindex, String.class));
		
		String ext = "";
		
		if(ct.Parent != null)
		{
			ext += " extends " + ct.Parent.getSimpleName();
			builder.append("import "); builder.append(ct.Parent.getName()); builder.append(";\n");
		}
		
		if(ct.IParent != null)
		{
			ext += " implements ";
			boolean t = false;
			for(Class<?> cl : ct.IParent)
			{
				if(t)
					ext += ", ";
				else
					t = true;
				
				ext += cl.getSimpleName();
				
				builder.append("import "); builder.append(cl.getName()); builder.append(";\n");
			}
		}
		
		builder.append("\n");
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		
		String begComment = ProgramCore.GetSnippetsFilesManager().LoadSnippet("ClassBeginingComment");
		begComment = begComment.replace("#version", "1.0");
		begComment = begComment.replace("#programversion", ProgramCore.Version);
		begComment = begComment.replace("#date", dateFormat.format(date));
		begComment = begComment.replace("#firstdate", dateFormat.format(date));
		
		builder.append(begComment);
		builder.append(Utils.nl);
		
		if(ct.HasSingleInstance)
			builder.append("@SingleInstance\n");
		
		builder.append("public class ");
		builder.append(Utils.GetClassNameFromCompleteName(ct.CompleteName));
		builder.append(ext);

		builder.append("\n{\n\t//REGION Constructors\n\tpublic "); builder.append(Utils.GetClassNameFromCompleteName(ct.CompleteName)); builder.append("()");
		
		if(ct.HasSingleInstance)
			builder.append("\n\t{\n\t\tloadInstance(\"main\");\n\t}");
		else
		{
			builder.append(" {}\n\n\tpublic ");
			builder.append(Utils.GetClassNameFromCompleteName(ct.CompleteName));
			builder.append("(String InstanceName)\n\t{\n\t\tloadInstance(InstanceName);\n\t}");
		}
		
		builder.append("\n\n\tpublic "); builder.append(Utils.GetClassNameFromCompleteName(ct.CompleteName)); builder.append("(");
		
		builder.append(argCore.toString());
		
		//TODO Si member contient une treemap, mettre les ajouts dans le constructeur
		
		setupVars.append("\n\t\t\t\t\tst.setString("); setupVars.append(c+1); setupVars.append(", CurrentInstanceName);"); rmCore.append("\n\tpublic String CurrentInstanceName = \"");
		if(ct.HasSingleInstance)
			rmCore.append("main\";");
		else
			rmCore.append("base\";");
		
		builder.append(")\n\t{"); builder.append(CCore.toString()); builder.append("\n\t}\n\t//ENDREGION\n\n\t//REGION Members"); builder.append(rmCore.toString());
		builder.append("\n\t//ENDREGION\n\n\t//REGION Methods");
		
		CCore = new StringBuilder();
		
		if(ct.Methods != null)
		{
			for(int i = 0; i < ct.Methods.size(); i++)
			{
				if(i > 0)
					CCore.append(Utils.nl);
					
				CCore.append(Utils.nlt); CCore.append(Modifier.toString(ct.Methods.get(i).Modifiers)); CCore.append(ct.Methods.get(i).Return); CCore.append(" "); CCore.append(ct.Methods.get(i).Name); CCore.append("(");
				
				for(int ii = 0; ii < ct.Methods.get(i).Arguments.length; ii++)
				{
					if(ii != 0)
						CCore.append(", ");
					
					CCore.append(ct.Methods.get(i).Arguments[ii]);
				}
				
				if(Modifier.isAbstract(ct.Methods.get(i).Modifiers) && ct.Methods.get(i).Core.equals(""))
				{
					CCore.append(");");
					continue;
				}
				
				CCore.append(")\n\t{\n");
				
				boolean doReturn = false;
				
				for(Map.Entry<String, Object> entry : ct.Methods.get(i).LocalVariables.entrySet())
				{
					if(Utils.IsPrimitiveType(entry.getValue()))
					{
						CCore.append(Utils.tt); CCore.append(Utils.GetCorrespondingPrimitiveType(entry.getValue())); CCore.append(" "); CCore.append(entry.getKey()); CCore.append(" = "); CCore.append(Utils.GetStringFromObject(entry.getValue())); CCore.append(";\n");
						doReturn = true;
					}
				}
				
				if(doReturn)
					CCore.append(Utils.nl);
				
				CCore.append("\t\t"); CCore.append(ct.Methods.get(i).Core);
				
				if(!ct.Methods.get(i).Core.endsWith(";"))
					CCore.append(";");
				
				if(!ct.Methods.get(i).Return.equals("void"))
				{
					CCore.append("\n\n\t\treturn "); CCore.append(ct.Methods.get(i).VarToReturn); CCore.append(";");
				}
				
				CCore.append("\n\t}");
			}
		}
		
		if(CCore.length() != 0)
			CCore.append(Utils.nl);
		
		Method[] ms = new Method[0];
		
		if(ct.Parent != null)
			ms = ct.Parent.getDeclaredMethods();
		if(ct.IParent != null)
		{
			for(Class<?> cc : ct.IParent)
			{
				ms = Utils.concatenate(ms, cc.getDeclaredMethods());
			}
		}
		
		for(int i = 0; i < ms.length; i++)
		{
			if((Modifier.isAbstract(ms[i].getModifiers()) || ms[i].getDeclaringClass().isInterface()) && !ms[i].isDefault())
			{
				boolean match = false;
				if(ct.Methods != null)
				{
					for(int j = 0; j < ct.Methods.size(); j++)
					{
						if(ct.Methods.get(j).Name.equals(ms[i].getName()) && Utils.GetClassName(ms[i].getReturnType()).equals(ct.Methods.get(j).Return) && ct.Methods.get(j).Arguments.length == ms[i].getParameterCount())
						{
							int qsd = 0;
							for(int j2 = 0; j2 < ms[i].getParameterCount(); j2++)
							{
								String[] str = ct.Methods.get(j).Arguments[j2].split(" ");
								if(!Utils.GetClassName(ms[i].getParameters()[j2].getClass()).equals(str[0]))
									break;
								else
									qsd++;
							}
							
							if(qsd == ms[i].getParameterCount())
								match = true;
						}
					}
				}

				if(!match)
				{
					if(ms[i].getName().equals("toString") || ms[i].getName().equals("loadInstance") || ms[i].getName().equals("saveCurrentInstance") || ms[i].getName().equals("safeSaveCurrentInstance") || ms[i].getName().equals("reloadInstance"))
					{
						String mcore = ProgramCore.GetSnippetsFilesManager().LoadSnippet(ms[i].getName() + Utils.GetClassName(ms[i].getDeclaringClass()) + "Method");
						
						mcore = mcore.replace("#classname", ms[i].getName().equals("toString")?Utils.GetClassNameFromCompleteName(ct.CompleteName):Utils.GetClassNameFromCompleteName(ct.CompleteName).toLowerCase());
						
						if(ms[i].getName().equals("toString"))
							mcore = mcore.replace("#members", tsCore.toString());
						else if(ms[i].getName().equals("loadInstance"))
							mcore = mcore.replace("#vars", liCore.toString());
						else if(ms[i].getName().equals("saveCurrentInstance"))
						{
							mcore = mcore.replace("#vars", vars.toString());
							mcore = mcore.replace("#nval", nVal.toString());
							mcore = mcore.replace("#setvars", setVars.toString());
							mcore = mcore.replace("#setupvars", setupVars.toString());
							mcore = mcore.replace("#insvars", insVars.toString());
						}
						else if(ms[i].getName().equals("safeSaveCurrentInstance"))
						{
							mcore = mcore.replace("#vars", ssVars.toString());
							mcore = mcore.replace("#setupvars", sttVars.toString());
						}
						
						if(CCore.length() != 0)
							CCore.append(Utils.nl);
						
						CCore.append(mcore.toString());
					}
					else
					{
						StringBuilder fn = new StringBuilder();
						fn.append(Utils.nlt); fn.append(Modifier.toString(ms[i].getModifiers())); fn.append(" "); fn.append(ms[i].getName());
					}
				}
			}
		}
		
		builder.append(CCore.toString()); builder.append("\n\t//ENDREGION\n}");
		
		try
		{
			FileWriter writer = new FileWriter(filePath);
			writer.write(builder.toString());
			writer.close();
		} catch(IOException e)
		{
			ProgramCore.io().println("Error Code : 0x031000");
			ProgramCore.GetErrorReporter().Report(new ClassWriterReportObject(ct, false, 2, null), e, ProgramClass.ClassWriter);
			return;
		}
		
		if(!Utils.CompileFile(filePath))
		{
			ProgramCore.io().println("Error Code : 0x031001");
			ProgramCore.GetErrorReporter().Report(new ClassWriterReportObject(ct, false, 3, Paths.get(filePath)), null, ProgramClass.ClassWriter);
			return;
		}
		
		try
		{
			Files.move(Paths.get(ProgramCore.BaseSRCPath+Utils.GetCompletePathFromCompleteName(ct.CompleteName)+".class"), Paths.get(ProgramCore.BaseBINPath+Utils.GetCompletePathFromCompleteName(ct.CompleteName)+".class"), StandardCopyOption.REPLACE_EXISTING);
		} catch(IOException e)
		{
			ProgramCore.io().println("Error Code : " + "0x031002");
			ProgramCore.GetErrorReporter().Report(new ClassWriterReportObject(ct, false, 4, Paths.get(filePath)), e, ProgramClass.ClassWriter);
			return;
		}
		
		Connection conn = Utils.PrepareConnection();
		
		if(conn != null)
		{
			StringBuilder request = new StringBuilder();
			request.append("CREATE TABLE ");
			request.append(Utils.GetClassNameFromCompleteName(ct.CompleteName));
			request.append(" (id INT NOT NULL AUTO_INCREMENT, ");
			request.append(tCore.toString());
			request.append("PRIMARY KEY (id))");
			
			Statement st = null;
			
			try
			{
				st = conn.createStatement();
			} catch(SQLException e)
			{
				ProgramCore.io().println("Error Code : 0x031003");
				return;
			}
			
			try
			{
				st.executeUpdate(request.toString());
			} catch(SQLException e)
			{
				ProgramCore.io().println("Error Code : 0x031004");
				return;
			}
			
			try
			{
				conn.close();
			} catch (SQLException e)
			{
				ProgramCore.io().println("Error Code : 0x030005");
			}
		}
		else
			return;
		
		long endTime = System.nanoTime();
		ProgramCore.io().printlntotab("-- WORKER -- Writing done in " + String.valueOf((endTime - startTime)/1000000d) + "ms", MVTabs.Writer);
		ProgramCore.GetLogger().log(new LogObject("Finished writing class " + Utils.GetClassNameFromCompleteName(ct.CompleteName) + " in " + String.valueOf((endTime - startTime)/1000000d) + "ms", "ClassWriter", LogType.Action, Priority.Low));
		
		ProgramCore.GetClassReloader().loadClass(ct.CompleteName);
		ProgramCore.GetClassManager().CallMethodFromObject(Utils.GetClassNameFromCompleteName(ct.CompleteName), "saveCurrentInstance");
	}
	
	public void start()
	{
		if(t == null)
		{
			ProgramCore.io().printlntotab("-- WORKER -- Starting Class Writer Thread", MVTabs.Writer);
			t = new Thread(this, "ClassWriterWorker");
			t.start();
		}
	}
}
