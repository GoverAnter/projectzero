package xx.projectzero.sourcewriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.Utils;
import xx.projectzero.annotations.JSCallable;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.ui.MainViewController.MVTabs;

/**
 * Classe g�rant l'�criture et la modification des fichiers classes.
 * @author Guillaume Gravetot
 * @version 1.1.1, 18/04/2016
 * @since 1.0.0, 12/04/2016
 */
@SuppressWarnings("rawtypes")
public class ClassWriter
{
	/** Enum Containing possible class types. */
	public static enum ClassType
	{
		Class,
		Interface
	}
	
	/**
	 * Structure de donn�es pour l'�criture d'une classe.
	 * @see MethodTemplate
	 * @see MemberTemplate
	 * @see ClassWriter
	 * @see ClassTemplate#ClassTemplate(String, ArrayList, ArrayList, Class, boolean)
	 */
	public static class ClassTemplate implements Serializable
	{
		/**
		 * Constructeur par d�faut de la structure de donn�es.<br>
		 * Cr�� par commodit�.
		 */
		public ClassTemplate()
		{
			HasSingleInstance = false;
		}
		
		/**
		 * Constructeur complet de la structure de donn�es.
		 * @param ClassCompleteName <b>String</b> : Nom complet de la classe <i>(eg. com.package.class)</i>.
		 * @param ClassMembers <b>ArrayList(MemberTemplate)</b> : Liste des membres de la classe (voir r�f�rences).
		 * @see MemberTemplate
		 * @param ClassMethods <b>ArrayList(MethodTemplate)</b> : Liste des m�thodes de la classe (voir r�f�rences).
		 * @see MethodTemplate
		 * @param ClassParent <b>Class</b> : Classe parente de la classe � �crire, envoyer <i>null</i> si aucun parent.
		 * @param InterfaceParent <b>ArrayList(Class)</b> : Parent interfaces of th class to write, <i>null</i> if no parent interfaces.
		 * @param ClassHasSingleInstance <b>boolean</b> : Sp�cifie si la classe ne pourra posseder su'une instance de donn�es.
		 */
		public ClassTemplate(String ClassCompleteName, ArrayList<MemberTemplate> ClassMembers, ArrayList<MethodTemplate> ClassMethods, Class ClassParent, ArrayList<Class> InterfaceParents, boolean ClassHasSingleInstance)
		{
			CompleteName = ClassCompleteName;
			if(ClassMembers != null && ClassMembers.size() > 0)
				Members = ClassMembers;
			if(ClassMethods != null && ClassMethods.size() > 0)
				Methods = ClassMethods;
			if(ClassParent != null && !ClassParent.equals(Object.class))
				Parent = ClassParent;
			IParent = InterfaceParents;
			HasSingleInstance = ClassHasSingleInstance;
		}
		
		/** Nom complet de la classe <i>(eg. com.package.class)</i>. */
		public String CompleteName;
		/** Tableau des membres
		 * @see MemberTemplate
		 */
		public ArrayList<MemberTemplate> Members;
		/** Tableau des methodes
		 * @see MethodTemplate
		 */
		public ArrayList<MethodTemplate> Methods;
		/** Parent de la classe � construire.<br>Envoyer <i>null</i> pour aucun h�ritage. */
		public Class Parent;
		/** Parent interfaces of the class to build. <i>null</i> if no parent interfaces. */
		public ArrayList<Class> IParent;
		/** Indique si la classe ne pourra poss�der qu'une seule instance de donn�es. */
		public boolean HasSingleInstance;
	}
	
	/**
	 * Structure de donn�es pour l'�criture des membres.
	 * @see ClassTemplate
	 * @see MethodTemplate
	 * @see MethodTemplate#MethodTemplate(String, int, String, String, String[], TreeMap, String)
	 */
	public static class MemberTemplate implements Serializable
	{
		/**
		 * Constructeur par d�faut de la structure de donn�es.<br>
		 * Cr�� par commodit�.
		 */
		public MemberTemplate()
		{
			Modifiers = Modifier.PUBLIC;
			Type = "";
		}
		
		/**
		 * Constructeur complet de la structure de donn�es.
		 * @param MemberName <b>String</b> : Nom du membre.
		 * @param MemberAccessModifier <b>int</b> : Modifiers du membre (voir r�f�rences).
		 * @see Modifier
		 * @param MemberValue <b>Object</b> : Valeur du membre.
		 */
		public MemberTemplate(String MemberName, int MemberModifiers, Object MemberValue)
		{
			Name = MemberName;
			Modifiers = MemberModifiers;
			Type = Utils.GetCorrespondingPrimitiveType(MemberValue, true);
			Value = MemberValue;
		}
		
		/**
		 * Constructeur r�duit de la structure de donn�es.
		 * @param MemberName <b>String</b> : Nom du membre.
		 * @param MemberAccessModifier <b>int</b> : Modifiers du membre (voir r�f�rences).
		 * @see Modifier
		 * @param MemberType <b>String</b> : Type du membre <b><i>(Type primitif recommand�)</i></b>.
		 */
		public MemberTemplate(String MemberName, int MemberModifiers, String MemberType)
		{
			Name = MemberName;
			Modifiers = MemberModifiers;
			Type = MemberType;
			Value = null;
		}
		
		/** Nom du membre. */
		public String Name;
		/** Modifiers du membre.
		 * @see Modifier
		 */
		public int Modifiers;
		/** Type du membre */
		public String Type;
		/** Valeur du membre */
		public Object Value;
	}
	
	/**
	 * Structure de donn�es pour l'�criture des m�thodes.
	 * @see ClassTemplate
	 * @see MemberTemplate
	 * @see MemberTemplate#MemberTemplate(String, int, String, Object)
	 */
	public static class MethodTemplate implements Serializable
	{
		/**
		 * Constructeur par d�faut de la structure de donn�es.<br>
		 * Cr�� par commodit�.
		 */
		public MethodTemplate() {}
		
		/**
		 * Constructeur complet de la structure de donn�es.
		 * @param MethodName <b>String</b> : Nom de la m�thode.
		 * @param MethodModifiers <b>int</b> : Modifiers de la m�thode (voir r�f�rences).<br>
		 * @see Modifier
		 * @param MethodReturn <b>String</b> : Type de retour de la m�thode <i><b>(format : "Type")</b></i>.
		 * @param MethodVarToReturn <b>String</b> : Nom de la variable � retourner <i><b>(non pris en compte si MethodReturn == "void")</b></i>.
		 * @param MethodArguments <b>String[]</b> : Liste des arguments <i><b>(format : "Type Name")</b></i>.
		 * @param MethodLocalVariables <b>TreeMap(String, Object)</b> : Liste des variables � d�clarer localement et de leurs valeurs d'initialisation.
		 * @param MethodCore <b>String</b> : Corps de la m�thode
		 */
		public MethodTemplate(String MethodName, int MethodModifiers, String MethodReturn, String MethodVarToReturn, String[] MethodArguments, TreeMap<String, Object> MethodLocalVariables, String MethodCore)
		{
			Name = MethodName;
			Modifiers = MethodModifiers;
			Return = MethodReturn;
			Arguments = MethodArguments;
			LocalVariables = MethodLocalVariables;
			Core = MethodCore;
			VarToReturn = MethodVarToReturn;
		}
		
		/**
		 * Constructeur r�duit de la structure de donn�es.
		 * @param MethodName <b>String</b> : Nom de la m�thode.
		 * @param MethodModifiers <b>int</b> : Modifiers de la m�thode (voir r�f�rences).<br>
		 * @see Modifier
		 * @param MethodReturn <b>String</b> : Type de retour de la m�thode <i><b>(format : "Type")</b></i>.
		 * @param MethodArguments <b>String[]</b> : Liste des arguments <i><b>(format : "Type Name")</b></i>.
		 */
		public MethodTemplate(String MethodName, int MethodModifiers, String MethodReturn, String[] MethodArguments)
		{
			Name = MethodName;
			Modifiers = MethodModifiers;
			Return = MethodReturn;
			Arguments = MethodArguments;
			LocalVariables = null;
			Core = "";
			VarToReturn = "";
		}
		
		/** Nom de la m�thode */
		public String Name;
		/** Modifiers de la m�thode.
		 * @see Modifier
		 */
		public int Modifiers;
		/** Type de retour de la m�thode */
		public String Return;
		/** Liste des arguments, structure : <i><b>"Type Nom"</b></i> */
		public String[] Arguments;
		/** Liste des variables locales et de leurs valeurs de d�claration */
		public TreeMap<String, Object> LocalVariables;
		/** Corps de la m�thode */
		public String Core;
		/** Nom de la variable � retourner, non utilis� si <i>return == void</i> */
		public String VarToReturn;
	}
	
	/**
	 * Constructeur principal de la classe.
	 * @see ClassWriter
	 */
	public ClassWriter()
	{ 
		ProgramCore.io().printlntotab("Starting ClassWriter main instance", MVTabs.Writer);
		
		chindex = ProgramCore.GetCache().CacheObject("\n\nimport java.lang.reflect.Method;\nimport java.sql.Connection;\nimport java.sql.PreparedStatement;\nimport java.sql.ResultSet;\nimport java.sql.SQLException;\nimport java.sql.Statement;\nimport java.util.*;\n\nimport xx.projectzero.core.ProgramCore;\nimport xx.projectzero.core.Utils;\nimport xx.projectzero.core.Logger.LogObject;\nimport xx.projectzero.core.Utils.Priority;\nimport xx.projectzero.core.Logger.LogType;\n");
	}
	
	/**
	 * M�thode pour ajouter une m�thode � une classe existante.
	 * @see ClassWriter#AddMethodToClass(String, MethodTemplate[])
	 */
	public boolean AddMethodToClass(Class classToModify, MethodTemplate Method)
	{
		MethodTemplate[] t = {Method};
		return AddMethodToClass(Utils.GetClassName(classToModify), t);
	}
	
	/**
	 * M�thode pour ajouter une m�thode � une classe existante.
	 * @see ClassWriter#AddMethodToClass(String, MethodTemplate[])
	 */
	public boolean AddMethodToClass(String className, MethodTemplate Method)
	{
		MethodTemplate[] t = {Method};
		return AddMethodToClass(className, t);
	}
	
	/**
	 * M�thode pour ajouter une m�thode � une classe existante.
	 * @param className <b>String</b> : Nom complet de la classe � modifier (eg. com.package.class).
	 * @param Methods <b>MethodTemplate[]</b> : Tableau des m�thodes � ajouter � la classe.
	 * @return <b>boolean</b> : <b>true</b> si les m�thodes ont �t� ajout�es.
	 * @see MethodTemplate
	 */
	public boolean AddMethodToClass(String className, MethodTemplate[] Methods)
	{
		String filePath = ProgramCore.BaseSRCPath+Utils.GetCompletePathFromCompleteName(className)+".java";
		
		byte[] fileArray = null;

		try
		{
			fileArray = Files.readAllBytes(Paths.get(filePath));
		} catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		
		String fileContent = new String(fileArray);
		String comment = "//REGION Methods";
		String[] parts = fileContent.split(comment);
		
		int sdfg = 0; int i = 0;
		
		parts[0] += comment;
		
		for(i = 0; i < Methods.length; i++)
		{
			if(parts[1].matches("[a-zA-Z0-9\"=,.+\\-*/{}\\[\\]();\\n\\t\\s]*public\\s[a-z0-9A-Z]+\\s"+Methods[i].Name+"[a-zA-Z0-9\"=,.+\\-*/{}\\[\\]();\\n\\t\\s]*"))
			{
				if(parts[1].matches("[a-zA-Z0-9\"=,.+\\-*/{}\\[\\]();\\n\\t\\s]*public\\s"+Methods[i].Return+"\\s"+Methods[i].Name+"[a-zA-Z0-9\"=,.+\\-*/{}\\[\\]();\\n\\t\\s]*"))
				{
					String temp = parts[1].replaceAll("[^(),]*", "");
					String[] temp2 = temp.split("[)]", 2);
					temp = temp2[0].substring(1);
					
					if(temp.length() > 0 && Methods[i].Arguments.length == temp.length()+1)
					{
						temp2 = parts[1].split("[)]", 2);
						String[] temp3 = temp2[0].split("[(]");
						temp2 = temp3[1].split(",\\s");
						boolean match = false;
						
						for(int dsf = 0; dsf < temp2.length; dsf++)
						{
							temp3 = temp2[dsf].split("\\s");
							String[] temp4 = Methods[i].Arguments[dsf].split("\\s");
							
							if(!temp3[0].equalsIgnoreCase(temp4[0]))
							{
								match = true;
								break;
							}
						}
						
						if(!match)
						{
							sdfg++;
							continue;
						}
					}
					else if(temp.length() == 0)
					{
						temp2 = parts[1].split("[)]", 2);
						
						if((temp2[0].length() == 1 && Methods[i].Arguments.length == 0) || (temp2[0].length() > 0 && Methods[i].Arguments.length == 1))
						{
							sdfg++;
							continue;
						}
					}
				}
				else
				{
					sdfg++;
					continue;
				}
			}
			
			parts[0] += "\n\tpublic " + Methods[i].Return + " " + Methods[i].Name + "(";
			
			for(int ii = 0; ii < Methods[i].Arguments.length; ii++)
			{
				if(ii != 0)
					parts[0] += ", ";
				
				parts[0] += Methods[i].Arguments[ii];
			}
			
			parts[0] += ")\n\t{\n";
			
			boolean doReturn = false;
			
			for(Map.Entry<String, Object> entry : Methods[i].LocalVariables.entrySet())
			{
				if(Utils.IsPrimitiveType(entry.getValue()))
				{
					parts[0] += "\t\t" + Utils.GetCorrespondingPrimitiveType(entry.getValue()) + " " + entry.getKey() + " = " + Utils.GetStringFromObject(entry.getValue()) + ";\n";
					doReturn = true;
				}
			}
			
			if(doReturn)
				parts[0] += "\n";
			
			parts[0] += "\t\t" + Methods[i].Core;
			
			if(!Methods[i].Core.endsWith(";"))
				parts[0] += ";";
			
			if(!Methods[i].Return.equals("void"))
				parts[0] += "\n\n\t\treturn " + Methods[i].VarToReturn + ";";
			
			parts[0] += "\n\t}";
		}
		
		if(i == sdfg)
			return true;
		
		fileContent = parts[0] + parts[1];
		
		try
		{
			FileWriter writer = new FileWriter(filePath);
			writer.write(fileContent);
			writer.close();
		} catch(IOException e)
		{
			e.printStackTrace();
			return false;
		}
		
		if(!Utils.CompileFile(filePath))
			return false;
		
		try
		{
			Files.move(Paths.get(ProgramCore.BaseSRCPath+Utils.GetCompletePathFromCompleteName(className)+".class"), Paths.get(ProgramCore.BaseBINPath+Utils.GetCompletePathFromCompleteName(className)+".class"), StandardCopyOption.REPLACE_EXISTING);
		} catch(IOException e)
		{
			e.printStackTrace();
			return false;
		}
		
		ProgramCore.GetClassReloader().loadClass(className);
		ProgramCore.GetLogger().log(new LogObject("Method added to class " + Utils.GetClassNameFromCompleteName(className), "ClassWriter", LogType.Action, Priority.Low));
		
		return true;
	}
	
	private int chindex;
	
	/**
	 * M�thode pour �crire une nouvelle classe.
	 * @param ct <b>ClassTemplate</b> : Structure de la classe � �crire.
	 * @see ClassWriter#WriteClass(ClassTemplate, ClassType, boolean)
	 * @see ClassTemplate
	 */
	@JSCallable
	public void WriteClass(ClassTemplate ct)
	{
		WriteClass(ct, ClassType.Class, false);
	}
	
	/**
	 * M�thode pour �crire une nouvelle interface.
	 * @param ct <b>ClassTemplate</b> : Structure de l'interface � �crire.
	 * @see ClassWriter#WriteClass(ClassTemplate, ClassType, boolean)
	 * @see ClassTemplate
	 */
	@JSCallable
	public void WriteInterface(ClassTemplate ct)
	{
		WriteClass(ct, ClassType.Interface, false);
	}
	
	/** Method to write a class with a ClassTemplate from cache. */
	@JSCallable
	public void WriteClass(int cacheAddress, ClassType type, boolean overwrite)
	{
		Object o = ProgramCore.GetCache().GetObject(cacheAddress);
		if(o != null)
		{
			ClassTemplate ct = null;
			try { ct = ClassTemplate.class.cast(o);
			} catch(ClassCastException e) { return; }
			
			WriteClass(ct, type, overwrite);
		}
	}
	
	/**
	 * M�thode pour �crire une nouvelle classe.
	 * @param ct <b>ClassTemplate</b> : Structure de la classe � �crire.
	 * @param overwrite <b>boolean</b> : <b>true</b> pour r��crire si la classe existe d�ja.
	 * @see ClassTemplate
	 */
	@JSCallable
	public void WriteClass(ClassTemplate ct, ClassType type, boolean overwrite)
	{
		if(type.equals(ClassType.Class))
		{
			if(Utils.GetPackageNameFromCompleteName(ct.CompleteName).equals(Utils.Packages.LearningClasses))
			{
				if(ProgramCore.GetClassManager().IsClassReferenced(Utils.GetClassNameFromCompleteName(ct.CompleteName)) && !overwrite)
					return;
	
				ProgramCore.io().printlntotab("Writing Class " + Utils.GetClassNameFromCompleteName(ct.CompleteName) + " from package " + Utils.GetPackageNameFromCompleteName(ct.CompleteName), MVTabs.Writer);
				
				//TODO Test si table existe
				//TODO Ajout/suppression membres
				
				ClassWriterWorker ww = new ClassWriterWorker(ct, chindex);
				ww.start();
			}
			else
			{
			}
		}
		else if(type.equals(ClassType.Interface))
		{
			ProgramCore.io().printlntotab("Writing Interface " + Utils.GetClassNameFromCompleteName(ct.CompleteName) + " from package " + Utils.GetPackageNameFromCompleteName(ct.CompleteName), MVTabs.Writer);
			
			InterfaceWriterWorker ww = new InterfaceWriterWorker(ct);
			ww.start();
		}
	}
}
