package xx.projectzero.sourcewriter;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.Utils;
import xx.projectzero.core.ErrorReporter.ClassWriterReportObject;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.Utils.ProgramClass;
import xx.projectzero.core.ui.MainViewController.MVTabs;
import xx.projectzero.sourcewriter.ClassWriter.ClassTemplate;
import xx.projectzero.sourcewriter.ClassWriter.MemberTemplate;
import xx.projectzero.sourcewriter.ClassWriter.MethodTemplate;


public class InterfaceWriterWorker implements Runnable
{
	private Thread t;
	private ClassTemplate ct;
	
	/** Main constructor of this class. */
	public InterfaceWriterWorker(ClassTemplate cTemplate)
	{
		ct = cTemplate;
	}
	
	@Override
	public void run()
	{
		ProgramCore.io().printlntotab("-- WORKER -- Writing interface", MVTabs.Writer);
		long startTime = System.nanoTime();
		
		String filepath = ProgramCore.BaseSRCPath+Utils.GetCompletePathFromCompleteName(ct.CompleteName)+".java";
		
		StringBuilder builder = new StringBuilder(3500);
		builder.append("package ");
		builder.append(Utils.GetPackageNameFromCompleteName(ct.CompleteName));
		builder.append(";\n\nimport java.util.*;");

		boolean b = false; String si = "";
		if(ct.IParent != null)
		{
			for(Class<?> c : ct.IParent)
			{
				if(!c.isInterface())
					continue;
				
				if(b)
					si += ", ";
				
				b = true;
				si += c.getSimpleName();
				
				builder.append("import "); builder.append(c.getName()); builder.append(";\n");
			}
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		
		String begComment = ProgramCore.GetSnippetsFilesManager().LoadSnippet("InterfaceBeginingComment");
		begComment = begComment.replace("#version", "1.0");
		begComment = begComment.replace("#programversion", ProgramCore.Version);
		begComment = begComment.replace("#date", dateFormat.format(date));
		begComment = begComment.replace("#firstdate", dateFormat.format(date));
		
		builder.append("\n\n");
		builder.append(begComment); builder.append("\n");
		builder.append("public interface ");
		builder.append(Utils.GetClassNameFromCompleteName(ct.CompleteName));
		
		if(b)
		{
			builder.append(" extends ");
			builder.append(si);
		}
		
		builder.append("\n{\n");
		
		//Interface core
		//Members
		if(ct.Members != null)
		{
			for(MemberTemplate m : ct.Members)
			{
				builder.append("\t");
				builder.append(Modifier.toString(m.Modifiers));			//public
				builder.append(" ");									//public 
				builder.append(m.Type);									//public String
				builder.append(" ");									//public String 
				builder.append(m.Name);									//public String s
				
				if(m.Value == null)
					builder.append(" = null;\n");						//public String s = null;
				else
				{
					builder.append(" = ");								//public String s = 
					builder.append(Utils.GetStringFromObject(m.Value));	//public String s = ""
					builder.append(";\n");								//public String s = "";
				}
			}
		}
		
		//Methods
		if(ct.Methods != null)
		{
			for(MethodTemplate m : ct.Methods)
			{
				builder.append("\n\t");
				builder.append(Modifier.toString(m.Modifiers));				//public
				if(!m.Core.equals(("")) && !Modifier.isStatic(m.Modifiers))	//public default
					builder.append(" default");
				builder.append(" ");										//public default 
				builder.append(m.Return);									//public default String
				builder.append(" ");										//public default String 
				builder.append(m.Name);										//public default String s
				builder.append("(");										//public default String s(
	
				b = false;
				if(m.Arguments != null)
				{
					for(String s : m.Arguments)
					{
						if(b)
							builder.append(", ");
							
						b = true;
						builder.append(s);									//public default String s(String t
					}
				}
				
				builder.append(")");										//public default String s(String t)
				
				if(m.Core.equals(("")))
					builder.append(";\n");									//public String s(String t);
				else
				{
					builder.append("\n\t{\n");
					boolean doreturn = false;
					
					if(m.LocalVariables != null)
					{
						for(Map.Entry<String, Object> entry : m.LocalVariables.entrySet())
						{
							if(Utils.IsPrimitiveType(entry.getValue()))
							{
								builder.append(Utils.tt); builder.append(Utils.GetCorrespondingPrimitiveType(entry.getValue())); builder.append(" "); builder.append(entry.getKey()); builder.append(" = "); builder.append(Utils.GetStringFromObject(entry.getValue())); builder.append(";\n");
								doreturn = true;
							}
						}
					}
					
					if(doreturn)
						builder.append("\n");
					
					builder.append("\t\t"); builder.append(m.Core);
					
					if(!m.Core.endsWith(";"))
						builder.append(";");
					
					if(!m.Return.equals("void"))
					{
						builder.append("\n\n\t\treturn "); builder.append(m.VarToReturn); builder.append(";");
					}
					
					builder.append("\n\t}\n");
				}
			}
		}
		
		builder.append("}");
		
		try
		{
			FileWriter writer = new FileWriter(filepath);
			writer.write(builder.toString());
			writer.close();
		} catch(IOException e)
		{
			ProgramCore.io().println("Error Code : 0x031A00");
			ProgramCore.GetErrorReporter().Report(new ClassWriterReportObject(ct, true, 2, null), e, ProgramClass.ClassWriter);
			return;
		}
		
		if(!Utils.CompileFile(filepath))
		{
			ProgramCore.io().println("Error Code : 0x031A01");
			ProgramCore.GetErrorReporter().Report(new ClassWriterReportObject(ct, true, 3, Paths.get(filepath)), null, ProgramClass.ClassWriter);
			return;
		}
		
		try
		{
			Files.move(Paths.get(ProgramCore.BaseSRCPath+Utils.GetCompletePathFromCompleteName(ct.CompleteName)+".class"), Paths.get(ProgramCore.BaseBINPath+Utils.GetCompletePathFromCompleteName(ct.CompleteName)+".class"), StandardCopyOption.REPLACE_EXISTING);
		} catch(IOException e)
		{
			ProgramCore.io().println("Error Code : " + "0x031A02");
			ProgramCore.GetErrorReporter().Report(new ClassWriterReportObject(ct, true, 4, Paths.get(filepath)), e, ProgramClass.ClassWriter);
			return;
		}
		
		long endTime = System.nanoTime();
		ProgramCore.io().printlntotab("-- WORKER -- Writing done in " + String.valueOf((endTime - startTime)/1000000d) + "ms", MVTabs.Writer);
		ProgramCore.GetLogger().log(new LogObject("Finished writing interface " + Utils.GetClassNameFromCompleteName(ct.CompleteName) + " in " + String.valueOf((endTime - startTime)/1000000d) + "ms", "ClassWriter", LogType.Action, Priority.Low));
		
		ProgramCore.GetClassReloader().loadClass(ct.CompleteName);
	}
	
	public void start()
	{
		if(t == null)
		{
			ProgramCore.io().printlntotab("-- WORKER -- Starting Interface Writer Thread", MVTabs.Writer);
			t = new Thread(this, "InterfaceWriterWorker");
			t.start();
		}
	}
}
