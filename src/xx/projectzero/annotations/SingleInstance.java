package xx.projectzero.annotations;

import java.lang.annotation.Documented;

/**
 * Sp�cifie si la classe annot�e est pr�vue pour n'utiliser qu'une seule instance de donn�es.
 * @author Guillaume Gravetot
 * @version 1.0.0, 12/04/2016
 * @since 1.0.0, 12/04/2016
 */
@Documented
public @interface SingleInstance {}
