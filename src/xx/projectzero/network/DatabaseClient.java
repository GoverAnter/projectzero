package xx.projectzero.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

import org.icmp4j.IcmpPingUtil;
import org.icmp4j.IcmpPingRequest;
import org.icmp4j.IcmpPingResponse;

import xx.projectzero.core.ProgramCore;

public class DatabaseClient implements Runnable
{
	public static enum State
	{
		Busy,
		Idle,
		Syncing
	}
	
	private Thread t;
	
	private int ID;
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	
	public volatile int DBVersion = -1;
	public volatile State state;
	public volatile boolean Busy;
	/** Designate this client's priority.<br>- 100 is the highest priority, used by the program to do direct updates.<br>- 0 is the lowest priority, used by the program to sync DBs. */
	public volatile int Priority;
	public InetAddress Address;
	
	public DatabaseClient(int id, Socket s)
	{
		socket = s; ID = id;
		
		try { in = new BufferedReader(new InputStreamReader(s.getInputStream())); } catch (IOException e) {}
		try { out = new PrintWriter(s.getOutputStream(), true); } catch (IOException e) {}
		
		Busy = true;
		state = State.Busy;
		Address = s.getInetAddress();
		
		//TODO ping for priority/if local : priority -> 100
		IcmpPingResponse response = IcmpPingUtil.executePingRequest(s.getInetAddress().getHostAddress(), 32, 1000L);

		
		String line;
		
		try
		{
			if((line = in.readLine()) != null)
			{
				String[] sline = line.split("\r");
				if(sline[0].equals("v"))
					DBVersion = Integer.parseInt(sline[1]);
			}
		} catch (IOException e) {}
		catch(NumberFormatException e) { DBVersion = 0; }
	}
	
	/** Dont call this.<br>Call start instead. 
	 * @see DatabaseClient#start()*/
	@Override
	public void run()
	{
		//TODO Request a host to sync with if version mismatch
	}
	
	/** Method to call to start a new DatabaseClientThread. */
	public void start(int id)
	{
		if(t == null)
		{
			t = new Thread(this, "DatabaseClientThread"+id);
			t.start();
		}
	}
}
