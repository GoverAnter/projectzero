package xx.projectzero.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;

public class DatabaseConnectionListener implements Runnable
{
	private Thread t;
	
	private ServerSocket server;
	
	public volatile DatabaseClient PriorityDB;
	public volatile ArrayList<DatabaseClient> ConnectedClients;
	public volatile boolean StopNeeded;
	
	/** Main constructor of this class. */
	public DatabaseConnectionListener()
	{
		StopNeeded = false;
		ConnectedClients = new ArrayList<DatabaseClient>();
		
		try
		{
			server = new ServerSocket(34561, 500);
		} catch (IOException e)
		{
			ProgramCore.io().println("Error Code : 0x0F1001");
			ProgramCore.GetLogger().log(new LogObject("0x0F1001 : Cannot create server : " + e.getMessage(), "DatabaseManager", LogType.Error, Priority.High));
		}
	}
	
	/** Dont call this.<br>Call start instead. 
	 * @see DatabaseConnectionListener#start()*/
	@Override
	public void run()
	{
		while(!StopNeeded)
		{
			Socket newClient = null;
			try
			{
				newClient = server.accept();
			} catch (IOException e)
			{
				ProgramCore.io().println("Error Code : 0x0F1100");
				ProgramCore.GetLogger().log(new LogObject("0x0F1100 : Error at db client connection : " + e.getMessage(), "DatabaseManager", LogType.Error, Priority.High));
			}
			
			if(newClient == null || newClient.isClosed() || !newClient.isConnected() || newClient.isInputShutdown() || newClient.isOutputShutdown())
				continue;
			
			DatabaseClient c = new DatabaseClient(ConnectedClients.size(), newClient);
			
			if(PriorityDB == null)
			{
				c.start(-1);
				PriorityDB = c;
			}
			else
			{
				c.start(ConnectedClients.size());
				ConnectedClients.add(c);
			}
		}
	}
	
	/** Method to call to start a new DatabaseConnectionListenerThread. */
	public void start()
	{
		if(t == null)
		{
			t = new Thread(this, "DatabaseConnectionListenerThread");
			t.start();
		}
	}
}
