package xx.projectzero.network;

import xx.projectzero.core.ProgramCore;

import java.net.InetAddress;
import java.sql.ResultSet;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.exceptions.InvalidArgumentException;
import xx.projectzero.exceptions.UnknownKeyException;

public class DatabaseManager implements Runnable
{
	private Thread t;
	
	private DatabaseConnectionListener listener;
	public volatile int CurrentDBVersion;
	
	public DatabaseManager()
	{
		try
		{
			ProgramCore.GetLogger().log(new LogObject("Registering Exit event", "DatabaseManager", LogType.Action, Priority.Medium));
			ProgramCore.GetEventManager().Register("Exit", this, this.getClass().getMethod("Stop"));
		} catch(NoSuchMethodException | SecurityException e)
		{
			ProgramCore.io().println("Error Code : 0x0F1000");
			ProgramCore.GetLogger().log(new LogObject("0x0F1000 : Error while registering Exit event : " + e.getMessage(), "DatabaseManager", LogType.Error, Priority.High));
		}
		
		try
		{
			CurrentDBVersion = Integer.parseInt(ProgramCore.GetParamSaver().Load("DBVersion"));
		} catch (NumberFormatException | InvalidArgumentException | UnknownKeyException e)
		{
			CurrentDBVersion = 0;
		}
		
		listener = new DatabaseConnectionListener();
		listener.start();
	}
	
	/** Dont call this.<br>Call start instead. 
	 * @see DatabaseManager#start()*/
	@Override
	public void run()
	{
		
	}
	
	/** Method to call to start a new DatabaseClientThread. */
	public void start()
	{
		if(t == null)
		{
			t = new Thread(this, "DatabaseManagerThread");
			t.start();
		}
	}
	
	/** Event Callback function to stop the database manager. */
	public void Stop()
	{
		//Program End code here
		ProgramCore.GetEventManager().Fire("Exiting");
		System.out.println("Exiting DatabaseManager");
	}
	
	public synchronized ResultSet Query(String query)
	{
		//TODO Implement this.
		
		return null;
	}
	
	public void Execute(String query)
	{
		
	}
	
	public InetAddress RequestSyncHost()
	{
		while(true)
		{
			for(DatabaseClient dbc : listener.ConnectedClients)
			{
				if(dbc.Busy == false && dbc.DBVersion == CurrentDBVersion)
					return dbc.Address;
			}
			
			try { Thread.sleep(100L); } catch (InterruptedException e) {}
		}
	}
}
