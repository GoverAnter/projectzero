package xx.projectzero.SchedulerThread;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.TreeMap;

import xx.equationparser.Expression;
import xx.projectzero.SchedulerThread.AnalyzeObjects.MonetaryObject.MonetaryCurrency;
import xx.projectzero.SchedulerThread.AnalyzeObjects.MonetaryObject.MonetaryFormat;
import xx.projectzero.core.Pair;
import xx.projectzero.core.Utils;

public final class AnalyzeObjects
{
	public static class MonetaryObject
	{
		public static enum MonetaryCurrency
		{
			EUR,
			USD,
			CAD,
			GBP
		}
		
		public static enum MonetaryFormat
		{
			FR, // 1 000 000,00
			EN  // 1,000,000.00
		}
		
		public static String CurrencySymbol(MonetaryCurrency c)
		{
			switch(c)
			{
				case CAD:
					return "$";
				case EUR:
					return "�";
				case GBP:
					return "�";
				case USD:
					return "$";
				default:
					return "$";
			}
		}
		
		public static String CurrencyAbrev(String currencySymbol)
		{
			if(currencySymbol.equals("$"))
				return "USD"; //Cant define by the symbol if its CAD or USD
			else if(currencySymbol.equals("�"))
				return "EUR";
			else if(currencySymbol.equals("�"))
				return "GBP";
			else return "USD";
		}
		
		public int integer;
		public byte floating;
		public MonetaryCurrency currency;
		
		public MonetaryObject() {}
		public MonetaryObject(int intpart, byte floatpart, MonetaryCurrency curr)
		{ integer = intpart; floating = floatpart; currency = curr; }
		
		public String readableMonetary(MonetaryFormat format, boolean symbol)
		{
			String price = "";
			String ksep = ""; String fsep = "";
			
			if(format.equals(MonetaryFormat.EN)) { ksep = ","; fsep = "."; }
			else { ksep = " "; fsep = ","; }
			
			price = String.valueOf(integer);
			
			int ntimes = Integer.divideUnsigned(price.length()-1, 3);
			
			for(int i = 1; i < ntimes+1; i++)
			{
				int ind = price.length()-(i*3+(i-1));
				String lst = price.substring(ind, price.length());
				String fst = price.substring(0, ind);
				price = fst + ksep + lst;
			}
			
			price += fsep + (String.valueOf(floating).length()==2?String.valueOf(floating):"0"+String.valueOf(floating));
			
			String cur = symbol?CurrencySymbol(currency):currency.toString();
			
			if(format.equals(MonetaryFormat.EN)) price = cur + price;
			else price += " " + cur;
			
			return price;
		}
	}
	
	public static class FirstPassObject
	{
		public ArrayList<String> cleanedsentences;
		
		public ArrayList<Boolean> hasSpecificPonctuation;
		public ArrayList<Boolean> hasInterrogationPoint;
		public ArrayList<Boolean> hasExclamationPoint;
		
		public ArrayList<String> detectedNames;
		public ArrayList<String> detectedMails;
		public ArrayList<LocalDate> detectedDates;
		public ArrayList<LocalTime> detectedTimes;
		public ArrayList<MonetaryObject> detectedPrices;
		public ArrayList<String> detectedQuotes;
		public ArrayList<String> detectedBrackets;
		public ArrayList<String> detectedSqrBrackets;
		public ArrayList<String> detectedCurBrackets;
		public ArrayList<Pair<Expression, TreeMap<String, BigDecimal>>> detectedEquations;
		
		public FirstPassObject(String sentence)
		{
			cleanedsentences = new ArrayList<String>();
			
			hasSpecificPonctuation = new ArrayList<Boolean>();
			hasInterrogationPoint = new ArrayList<Boolean>();
			hasExclamationPoint = new ArrayList<Boolean>();
			
			detectedNames = new ArrayList<String>();
			detectedMails = new ArrayList<String>();
			detectedDates = new ArrayList<LocalDate>();
			detectedTimes = new ArrayList<LocalTime>();
			detectedPrices = new ArrayList<MonetaryObject>();
			detectedQuotes = new ArrayList<String>();
			detectedBrackets = new ArrayList<String>();
			detectedSqrBrackets = new ArrayList<String>();
			detectedCurBrackets = new ArrayList<String>();
			detectedEquations = new ArrayList<Pair<Expression, TreeMap<String, BigDecimal>>>();
			
			FirstPass(sentence);
		}
		
		/** Returns a clean, unponctuated, contractionless sentence */
		public void FirstPass(String in)
		{
			in = in.replaceAll("'ve", " have");
			in = in.replaceAll("won't", "will not");
			in = in.replaceAll("n't", " not");
			in = in.replaceAll("'m", " am");
			in = in.replaceAll("'re", " are");
			in = in.replaceAll("'ll", " will");
			
			//ISSUE #6
			in = in.replaceAll("'s", " is"); //--> is/has/belongs to
			in = in.replaceAll("'d", " would"); //--> had/would
			
			in.replaceAll("\\\\", "");
			
			in = detectEnclosingChar("({(", ")})", in); //Equations
			in = detectEnclosingChar("\"", "\"", in);
			in = detectEnclosingChar("(", ")", in);
			in = detectEnclosingChar("{", "}", in);
			in = detectEnclosingChar("[", "]", in);
			
			in = stdDates("/", in);
			in = stdDates("-", in);
			in = stdDates(".", in);
			
			in = textDates(in);
			
			in = mailDetect(in);
			in = nameDetect(in);
			
			in = stdHours(":", in);
			in = stdHours("h", in);
			
			in = prices("�", in);
			in = prices("$", in);
			in = prices("�", in);
			
			in = prices("EUR", in);
			in = prices("USD", in);
			in = prices("CAD", in);
			in = prices("GBP", in);

			//Breaking the sentence
			//ISSUE #4
			in = in.replaceAll(",", " "); //ISSUE #5
			in = in.replaceAll(";", ".");
			int lastID = 0;
			int exid = in.indexOf("!", lastID), intid = in.indexOf("?", lastID), pid = in.indexOf(".", lastID);

			while(!(exid == -1 && intid == -1 && pid == -1) || lastID > in.length()-1)
			{
				int min = Math.min(pid==-1?Integer.MAX_VALUE:pid, Math.min(exid==-1?Integer.MAX_VALUE:exid, intid==-1?Integer.MAX_VALUE:intid));
				
				String sentence = in.substring(lastID, min);
				sentence = Utils.CleanSpaces(sentence).trim();
				cleanedsentences.add(sentence);
				
				if(min == pid)
				{
					hasExclamationPoint.add(false);
					hasInterrogationPoint.add(false);
				}
				else if(min == exid)
				{
					hasExclamationPoint.add(true);
					hasInterrogationPoint.add(false);
				}
				else
				{
					hasExclamationPoint.add(false);
					hasInterrogationPoint.add(true);
				}
				
				if(min+1 > in.length()-1)
					break;
				
				lastID = min+1;
				
				exid = in.indexOf("!", lastID); intid = in.indexOf("?", lastID); pid = in.indexOf(".", lastID);
			}
			
			if(cleanedsentences.size() == 0)
			{
				cleanedsentences.add(in.trim());
				hasExclamationPoint.add(false);
				hasInterrogationPoint.add(false);
			}
		}
		
		private String detectEnclosingChar(String begchar, String closechar, String sentence)
		{
			int id = sentence.indexOf(begchar);
			
			while(id != -1)
			{
				int nxid = sentence.indexOf(closechar, id+1);
				
				if(nxid != -1)
				{
					String quote = sentence.substring(id+1, nxid);
					sentence = sentence.replace(begchar + quote + closechar, setInArray(begchar, quote));
				}
				
				id = sentence.indexOf(begchar, id+1);
			}
			
			return sentence;
		}
		
		private String setInArray(String begchar, String sentence)
		{
			switch (begchar)
			{
				case "({(":
					TreeMap<String, BigDecimal> t = new TreeMap<String, BigDecimal>();
					Pair<Expression, TreeMap<String, BigDecimal>> p = new Pair<Expression, TreeMap<String, BigDecimal>>(xx.equationparser.Parser.parse(sentence.replaceAll(" ", ""),t), t);
					detectedEquations.add(p);
					return "\\equation" + String.valueOf(detectedEquations.size()-1);
				case "\"":
					detectedQuotes.add(sentence);
					return "\\quote" + String.valueOf(detectedQuotes.size()-1);
				case "(":
					detectedBrackets.add(sentence);
					return "\\bracket" + String.valueOf(detectedBrackets.size()-1);
				case "{":
					detectedCurBrackets.add(sentence);
					return "\\curbracket" + String.valueOf(detectedCurBrackets.size()-1);
				case "[":
					detectedSqrBrackets.add(sentence);
					return "\\sqrbracket" + String.valueOf(detectedSqrBrackets.size()-1);
				default:
					return "";
			}
		}
		
		private String stdDates(String separator, String sentence)
		{
			String pattern = "[*\\/+,:\\.\\-;?!=\\[\\]\\{\\}\\(\\)]";
			pattern = pattern.replace("\\" + separator, "");
			String ind = sentence.replaceAll(pattern, " ");
			
			int id = ind.indexOf(separator);

			while(id != -1)
			{
				if((ind.substring((id-2)<0?0:id-2, id)).matches("^[0-3\\s]?[0-9]"))
				{
					int lsid = ind.lastIndexOf(" ", id);
					int nsid = ind.indexOf(" ", id);
					
					String potdate = "";
					
					if(nsid != -1)
						potdate = ind.substring(lsid+1, nsid);
					else
						potdate = ind.substring(lsid+1);

					if(potdate.matches("^[0-3]?[0-9]\\" + separator + "[0-3]?[0-9]\\" + separator + "(?:[0-9]{2})?[0-9]{2}$"))
					{
						//Date parse
						String[] dnum = potdate.split("\\" + separator);
						int dom = Integer.parseInt(dnum[0]);
						int mod = Integer.parseInt(dnum[1]);
						int y = Integer.parseInt(dnum[2]);

						LocalDate d;

						//unknown state, taking first as month and second as day
						if(dom <= 12 && mod <= 12)
							d = LocalDate.of(y, dom, mod);
						//dom is month, mod is day
						else if(dom <= 12 && mod > 12 && mod <= 31)
							d = LocalDate.of(y, dom, mod);
						//mod is month, dom is day
						else if(mod <= 12 && dom > 12 && dom <= 31)
							d = LocalDate.of(y, mod, dom);
						//invalid date
						else
						{
							id = ind.indexOf(separator, id+1);
							continue;
						}

						detectedDates.add(d);

						sentence = sentence.replace(potdate, "\\date" + String.valueOf(detectedDates.size()-1));
						ind = ind.replace(potdate, "\\date" + String.valueOf(detectedDates.size()-1));
					}
				}

				id = ind.indexOf(separator, id+1);
			}
			
			return sentence;
		}
		
		private String textDates(String sentence)
		{
			String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
			String inz = sentence.replaceAll("[*/+\\.\\-;?:!=\\[\\]\\{\\}\\(\\)]", " ");
			
			//possible formats :
			//Month (D)D, YYYY
			//(D)D Month YYYY
			
			int mon = 1;
			for(String m : months)
			{
				int id = inz.indexOf(m);
				
				while(id != -1)
				{
					int nid = id+m.length()+1;
					
					if(nid >= inz.length())
						break;
					
					int nxid = inz.indexOf(" ", nid);
					String potdateend;
					
					if(nxid != -1)
						potdateend = inz.substring(nid, nxid);
					else
						potdateend = inz.substring(nid);
					
					if(potdateend.matches("\\d{1,4},?") && (id+m.length()+potdateend.length()+2 > inz.length() || new String(new byte[] {(byte)inz.charAt(id+m.length()+potdateend.length()+2)}).matches("\\D")))
					{
						int pid = id-2;
						
						if(pid < 0)
						{
							id = inz.indexOf(m, id+2);
							continue;
						}
						
						int ppid = inz.lastIndexOf(" ", pid);
						
						if(ppid < 0)
						{
							id = inz.indexOf(m, id+2);
							continue;
						}
						
						String potdatebeg = inz.substring(ppid+1, pid+1);
						
						if(potdatebeg.matches("\\d{1,2}"))
						{
							LocalDate d = LocalDate.of(Integer.parseInt((potdateend.matches("\\d{1,4}")?potdateend:potdateend.substring(0, potdateend.length()-1))), mon, Integer.parseInt(potdatebeg));

							detectedDates.add(d);
							String potdate = potdatebeg + " " + m + " " + (potdateend.matches("\\d{1,4}")?potdateend:potdateend.substring(0, potdateend.length()-1));

							inz = inz.replace(potdate, "\\date" + String.valueOf(detectedDates.size()-1));
							sentence = sentence.replace(potdate, "\\date" + String.valueOf(detectedDates.size()-1));
							id = inz.indexOf(m, ppid);
						}

						id = inz.indexOf(m, ppid);
					}
					else if(potdateend.matches("\\d{1,2},"))
					{
						int nnid = id+potdateend.length()+2;
						
						if(nnid >= sentence.length())
						{
							id = inz.indexOf(m, id+2);
							continue;
						}
						
						int nsid = inz.indexOf(" ", nnid);
						String potdateeend;
						
						if(nsid != -1)
							potdateeend = inz.substring(nnid, nsid);
						else
							potdateeend = inz.substring(nnid);
						
						if(potdateeend.matches("\\d{1,4},?"))
						{
							LocalDate d = LocalDate.of(Integer.parseInt(potdateeend.matches("\\d{1,4}")?potdateeend:potdateeend.substring(0,potdateeend.length()-1)), mon, Integer.parseInt(potdateend.substring(0,potdateend.length()-1)));

							detectedDates.add(d);
							String potdate = m + " " + potdateend + " " + (potdateeend.matches("\\d{1,4}")?potdateeend:potdateeend.substring(0,potdateeend.length()-1));

							inz = inz.replace(potdate, "\\date" + String.valueOf(detectedDates.size()-1));
							sentence = sentence.replace(potdate, "\\date" + String.valueOf(detectedDates.size()-1));
							id = inz.indexOf(m, id+2);
						}
					}
					
					id = inz.indexOf(m, (id+2)>inz.length()?inz.length():id+2);
				}
				mon++;
			}
			
			return sentence;
		}
		
		private String mailDetect(String sentence)
		{
			int id = sentence.indexOf("@");
			
			while(id != -1)
			{
				int lsid = sentence.lastIndexOf(" ", id);
				int nsid = sentence.indexOf(" ", id);
				
				String potmail = "";

				if(nsid != -1)
					potmail = sentence.substring(lsid+1, nsid);
				else
					potmail = sentence.substring(lsid+1);
				
				if(potmail.matches("^[a-zA-Z0-9!#$%&'*+\\-/=?\\^_`\\{\\|\\}~]+@[a-zA-Z0-9\\-]+\\.?[a-zA-Z0-9\\-]*\\.[a-zA-Z0-9]{2,}$"))
				{
					detectedMails.add(potmail);
					sentence = sentence.replace(potmail, "\\mail" + String.valueOf(detectedMails.size()-1));
				}

				id = sentence.indexOf("@", id+1);
			}
			
			return sentence;
		}
		
		private String nameDetect(String sentence)
		{
			String ind = sentence.replaceAll("[*/+,\\-;?:!=\\[\\]\\{\\}\\(\\)]", " ");
			int id = ind.indexOf(".");
			
			while(id != -1)
			{
				int lsid = ind.lastIndexOf(" ", id);
				int nxid = ind.indexOf(" ", (id+2)>=ind.length()?ind.length():id+2);
				
				String potname = "";
				
				if(nxid != -1)
					potname = ind.substring(lsid+1, nxid);
				else
					potname = ind.substring(lsid+1);
				
				if(potname.matches("[a-zA-Z]{1}\\.\\s[a-zA-Z]+\\.?"))
				{
					detectedNames.add(potname);
					
					sentence = sentence.replace(potname, "\\name" + String.valueOf(detectedNames.size()-1));
					ind = ind.replace(potname, "\\name" + String.valueOf(detectedNames.size()-1));
				}
				
				id = ind.indexOf(".", id+1);
			}
			
			return sentence;
		}
		
		private String stdHours(String separator, String sentence)
		{
			String pattern = "[*/+,:\\.\\-;?!=\\[\\]\\{\\}\\(\\)]";
			pattern = pattern.replace(separator, "");
			String inv = sentence.replaceAll(pattern, " ");
			int id = inv.indexOf(separator);
			
			while(id != -1)
			{
				int lsid = inv.lastIndexOf(" ", id);
				int nxid = inv.indexOf(" ", id);
				
				String pothour = "";

				if(nxid == -1)
					pothour = inv.substring(lsid+1);
				else
					pothour = inv.substring(lsid+1, nxid);
				
				if(pothour.matches("[0-2]?[0-9]" + separator + "[0-5][0-9]"))
				{
					String[] ph = pothour.split(separator);
					int h = Integer.parseInt(ph[0]);
					int m = Integer.parseInt(ph[1]);
					
					int cid = nxid; boolean lastWasP = false; boolean lastWasM = false; boolean lastWasA = false;
					
					while(cid < inv.length())
					{
						if(cid == -1)
							break;
						
						String l = inv.substring(cid,cid+1);

						if(lastWasM)
						{
							if(l.equals(" "))
							{
								if(lastWasP)
								{
									if(h < 12)
										h += 12;
									
									nxid = cid;
								}
								else if(lastWasA)
									nxid = cid;
							}
							
							break;
						}
						else if(lastWasP || lastWasA)
						{
							if(l.equalsIgnoreCase("m"))
							{
								lastWasM = true;
								
								if(cid==(inv.length()-1))
								{
									if(lastWasP)
									{
										if(h < 12)
											h += 12;
										
										nxid = cid+1;
									}
									else if(lastWasA)
										nxid = cid+1;
								}
							}
							else
								break;
						}
						else
						{
							if(l.equalsIgnoreCase("p"))
								lastWasP = true;
							else if(l.equalsIgnoreCase("a"))
								lastWasA = true;
							else
								if(!l.equals(" "))
									break;
								
						}
						
						cid++;
					}
					
					if(h < 24 && h >= 0 && m < 60 && m >= 0)
					{
						LocalTime lt = LocalTime.of(h, m);
						detectedTimes.add(lt);
						
						sentence = sentence.replace((nxid<=-1)?sentence.substring(lsid+1):sentence.substring(lsid+1, nxid), "\\time"+String.valueOf(detectedTimes.size()-1));
						inv = inv.replace((nxid<=-1)?inv.substring(lsid+1):inv.substring(lsid+1, nxid), "\\time"+String.valueOf(detectedTimes.size()-1));
					}
				}
				
				id = inv.indexOf(separator, id+1);
			}
			
			return sentence;
		}
		
		private String prices(String currencyMark, String sentence)
		{
			String in = sentence.replaceAll("[*/+:\\-;?!=\\[\\]\\{\\}\\(\\)]", " ");
			int id = in.indexOf(currencyMark);

			while(id != -1)
			{
				//Format : CURX or CUR X
				if((id+currencyMark.length()+1 < in.length() && in.substring(id+currencyMark.length(), id+currencyMark.length()+1).matches("\\d")) || (id+currencyMark.length()+2 < in.length() && in.substring(id+currencyMark.length()+1, id+currencyMark.length()+2).matches("\\d")))
				{
					//Detect price boundaries
					String potnum = "";
					int lsid = in.substring(id+currencyMark.length(), id+currencyMark.length()+1).matches("\\d")?id:id+currencyMark.length()+1;
					
					while(true)
					{
						int nxid = in.indexOf(" ", lsid);

						if(nxid == -1)
						{
							potnum = in.substring(id);
							break;
						}
						else if(nxid+1 == in.length() || in.substring(nxid, nxid+1).matches("\\d"))
						{
							potnum = in.substring(id, nxid);
							break;
						}
						
						lsid = nxid+1;
					}
					
					//Pattern match
					if(potnum.matches((currencyMark.length()==1?"["+currencyMark+"]":currencyMark) + "\\s?\\d{1,3}(?:[\\s,]\\d{3})*(?:[\\.,]\\d{2})?"))
					{
						String bnum = potnum;
						
						//Unserialization
						if(in.charAt(id+currencyMark.length()) == ' ')
							potnum = potnum.substring(currencyMark.length()+1);
						else
							potnum = potnum.substring(currencyMark.length());
						
						String decpart = "00";
						
						//if the number has a decimal part, take it first
						if(potnum.matches("\\d{1,3}(?:[\\s,]\\d{3})*[\\.,]\\d{2}"))
						{
							decpart = potnum.substring(potnum.length()-3);
							potnum = potnum.substring(0, potnum.length()-3);
							decpart = decpart.substring(1);
						}
						
						//determine if number is bigger than 999
						if(potnum.length() > 3)
						{
							//determine the separator
							String separ = potnum.substring(potnum.length()-4, potnum.length()-3);

							potnum = potnum.replaceAll(separ, "");
							
							//valid number
							if(potnum.matches("\\d*"))
							{
								//if the currency mark is a symbol, change it to a currency abrev
								String cmark = currencyMark;
								if(currencyMark.length() == 1)
									cmark = MonetaryObject.CurrencyAbrev(currencyMark);
								
								MonetaryObject price = new MonetaryObject(Integer.parseInt(potnum), Byte.parseByte(decpart), MonetaryCurrency.valueOf(cmark));
								detectedPrices.add(price);
								in = in.replace(bnum, "\\price" + String.valueOf(detectedPrices.size()-1));
								sentence = sentence.replace(bnum, "\\price" + String.valueOf(detectedPrices.size()-1));
								System.out.println(price.readableMonetary(MonetaryFormat.FR, true));
							}
						}
					}
				}
				//Format : XCUR || X CUR
				else if((id-1 >= 0 && in.substring(id-1, id).matches("\\d")) || (id-2 >= 0 && in.substring(id-2, id-1).matches("\\d")))
				{
					//Detect price boundaries
					String potnum = "";
					int lsid = (in.substring(id-1, id).matches("\\d")?id:id-1);
					id += currencyMark.length();
					
					while(true)
					{
						int nxid = in.lastIndexOf(" ", lsid);
						
						if(nxid == -1)
						{
							potnum = in.substring(0, id);
							break;
						}
						else if(nxid-1 < 0 || !in.substring(nxid-1, nxid).matches("\\d"))
						{
							potnum = in.substring(nxid+1, id);
							break;
						}

						lsid = nxid-1;
					}

					//pattern match
					if(potnum.matches("\\d{1,3}(?:[\\s\\.,]\\d{3})*(?:[\\.,]\\d{2})?\\s?" + (currencyMark.length()==1?"["+currencyMark+"]":currencyMark)))
					{
						String bnum = potnum;
						
						//removing currency mark
						if(in.charAt(id-currencyMark.length()-1) == ' ')
							potnum = potnum.substring(0, potnum.length()-currencyMark.length()-1);
						else
							potnum = potnum.substring(0, potnum.length()-currencyMark.length());
						
						String decpart = "00";
						
						//if the number has a decimal part, take it first
						if(potnum.matches("\\d{1,3}(?:[\\s\\.,]\\d{3})*[\\.,]\\d{2}"))
						{
							decpart = potnum.substring(potnum.length()-3);
							potnum = potnum.substring(0, potnum.length()-3);
							decpart = decpart.substring(1);
						}
						
						//determine if number is bigger than 999
						if(potnum.length() > 3)
						{
							//determine the separator
							String separ = potnum.substring(potnum.length()-4, potnum.length()-3);

							potnum = potnum.replaceAll("\\" + separ, "");
							
							//valid number
							if(potnum.matches("\\d*"))
							{
								//if the currency mark is a symbol, change it to a currency abrev
								String cmark = currencyMark;
								if(currencyMark.length() == 1)
									cmark = MonetaryObject.CurrencyAbrev(currencyMark);
								
								MonetaryObject price = new MonetaryObject(Integer.parseInt(potnum), Byte.parseByte(decpart), MonetaryCurrency.valueOf(cmark));
								detectedPrices.add(price);
								in = in.replace(bnum, "\\price" + String.valueOf(detectedPrices.size()-1));
								sentence = sentence.replace(bnum, "\\price" + String.valueOf(detectedPrices.size()-1));
								System.out.println(price.readableMonetary(MonetaryFormat.FR, true));
							}
						}
					}
				}

				id = in.indexOf(currencyMark, id+1);
			}
			
			return sentence;
		}
	}
}
