package xx.projectzero.SchedulerThread;

import java.util.ArrayList;

import xx.projectzero.core.ProgramCore;
import xx.projectzero.linguistic.LWord;

public class ComparatorThread implements Runnable
{
	private Thread t;
	private int CIndex;
	private ComparatorDataStruct cds;
	
	private ArrayList<String> classnamematches;
	private String[] classlist;
	
	public volatile int UserResponseID;
	
	/** Main constructor of this class. */
	public ComparatorThread(ComparatorDataStruct CDS, int urid)
	{
		cds = CDS;
		UserResponseID = urid;
	}
	
	/** Dont call this.<br>Call start instead. 
	 * @see ComparatorThread#start(int)*/
	@Override
	public void run()
	{
		//TODO Comparator treatment.
		classlist = ProgramCore.GetClassManager().GetClassList();
		classnamematches = new ArrayList<String>();
		
		if(cds.GetValue() == null)
		{
			ClassNameMatch();
		}
		
		ProgramCore.io().println("found " + classnamematches.size() + " elements");
		
		ProgramCore.GetScheduler().ScheduleComparatorDestruction(CIndex);
	}
	
	private void ClassNameMatch()
	{
		for(String string : classlist)
		{
			if(LWord.Compare(cds.GetPreparedWord(), string) >= 85)
				classnamematches.add(string);
			
			try { Thread.sleep(50L); } catch (InterruptedException e) {}
		}
	}
	
	/** Method to call to start a new ComparatorThread.
	 * @param index <b>int</b> : Index of this thread, given by the <i>Scheduler</i> class.
	 */
	public void start(int index)
	{
		if(t == null)
		{
			CIndex = index;
			String name = "ComparatorThread" + index;
			t = new Thread(this, name);
			t.setDaemon(true);
			t.start();
		}
	}
}
