package xx.projectzero.SchedulerThread;

import xx.projectzero.SchedulerThread.AnalyzeObjects.FirstPassObject;

public class ComparatorDataStruct
{
	private String searchWord;
	private String preparedWord;
	private boolean multipleWords;
	private String[] words;
	private Object value;
	
	private FirstPassObject fpobject;
	private int sentindex;
	
	/** Same as other constructor, with args (SearchWords, null). */
	public ComparatorDataStruct(String SearchWord, FirstPassObject firstpasso, int sentenceindex)
	{
		searchWord = SearchWord;
		value = null;
		
		fpobject = firstpasso;
		sentindex = sentenceindex;
		
		prepare();
	}
	
	public ComparatorDataStruct(String SearchWord, Object Value, FirstPassObject firstpasso, int sentenceindex)
	{
		searchWord = SearchWord;
		value = Value;
		
		fpobject = firstpasso;
		sentindex = sentenceindex;
		
		prepare();
	}
	
	private void prepare()
	{
		words = searchWord.split(" ");
		multipleWords = words.length!=1;
		
		if(multipleWords)
		{
			preparedWord = "";
			
			for(String w : words)
				preparedWord += w.substring(0, 1).toUpperCase() + w.substring(1);
		}
		else
			preparedWord = searchWord.substring(0, 1).toUpperCase() + searchWord.substring(1);
	}
	
	public String GetSearchWord() { return searchWord; }
	public String GetPreparedWord() { return preparedWord; }
	public boolean GetMultipleWords() { return multipleWords; }
	public String[] GetWords() { return words; }
	public Object GetValue() { return value; }
	
	public FirstPassObject fpo() { return fpobject; }
	public int SentenceIndex() { return sentindex; }
}

/* Si value == null -> chercher si un objet ou un membre existe
 * Si value != null -> chercher si objet(avec comme nom potentialObject, sinon chercher dans tous les objets) dont membre(name) existe, puis comparer les valeurs
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */