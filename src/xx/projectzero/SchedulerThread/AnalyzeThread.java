package xx.projectzero.SchedulerThread;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.Utils;
import xx.projectzero.SchedulerThread.AnalyzeObjects.FirstPassObject;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.ui.MainViewController.MVTabs;
import xx.projectzero.linguistic.LWord;
import xx.projectzero.linguistic.LWordPattern;

public class AnalyzeThread implements Runnable
{
	private Thread t;
	
	@Override
	public void run()
	{
		while(true)
		{
			ProgramCore.io().printlntotab("Waiting for a new input", MVTabs.Analyze);
			while(!ProgramCore.GetScheduler().HasNextInput())
			{
				try
				{
					Thread.sleep(100L);
				} catch(InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			ProgramCore.io().printlntotab("\nNew input found", MVTabs.Analyze);
			
			String in = ProgramCore.GetScheduler().GetNextInput();
			
			if(!in.equals(""))
			{
				ProgramCore.io().printlntotab("Analyzing sentence " + in, MVTabs.Analyze);
				
				if(LWord.equals(in, "exit"))
				{
					ProgramCore.GetEventManager().Fire("Exit");
					continue;
				}
				
				long startTime = System.nanoTime();
				
				ProgramCore.io().printlntotab("-------------------------------------------", MVTabs.Analyze);
				
				FirstPassObject fpo = new FirstPassObject(in);
				ProgramCore.io().printlntotab("First pass done, found :\n\t- " + fpo.detectedDates.size() + " dates.\n\t- " + fpo.detectedTimes.size() + " times.\n\t- " + fpo.detectedMails.size() + " mails.\n\t- " + fpo.detectedNames.size() + " names.\n  In " + fpo.cleanedsentences.size() + " sentences.", MVTabs.Analyze);
				
				for(String s : fpo.cleanedsentences)
				{
					ArrayList<LWordPattern> mp = ProgramCore.GetPatternDictionary().FindMatchingPatterns(s);
					
					if(mp.size() != 0)
					{
						int index = fpo.cleanedsentences.indexOf(s);
						char ponct = fpo.hasSpecificPonctuation.get(index)?(fpo.hasInterrogationPoint.get(index)?'?':'!'):'.';
						LWordPattern p = mp.remove(0);
						p.Treat(s, mp, ponct, fpo, index);
					}
					else
					{
						
					}
				}
				
				ProgramCore.io().printlntotab("-------------------------------------------", MVTabs.Analyze);
				
				long endTime = System.nanoTime();
				ProgramCore.io().printlntotab("Analysis done in " + String.valueOf((endTime - startTime)/1000000d) + "ms", MVTabs.Analyze);
				ProgramCore.GetLogger().log(new LogObject("Analysis done in " + String.valueOf((endTime - startTime)/1000000d) + "ms", "AnalyzeThread", LogType.Action, Priority.Low));
			}
		}
	}
	
	public void start()
	{
		if(t == null)
		{
			ProgramCore.io().printlntotab("Creating Analyze thread as a deamon", MVTabs.Analyze);
			t = new Thread(this, "AnalyzeThread");
			t.setDaemon(true);
			t.start();
			ProgramCore.GetLogger().log(new LogObject("Thread started", "AnalyzeThread", LogType.Action, Priority.Low));
		}
	}
}
