package xx.projectzero.linguistic;

import java.io.Serializable;
import java.util.ArrayList;

import xx.projectzero.core.Pair;
import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.Utils;
import xx.projectzero.SchedulerThread.AnalyzeObjects.FirstPassObject;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;

/**
 * This Class represents a word pattern.
 * @author Guillaume Gravetot
 * @version 1.1.0, 20/05/2016
 * @since 1.1.0, 19/04/2016
 */
public class LWordPattern implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	protected volatile ArrayList<Pair<Integer,Integer>> PatternWordsIndexes;
	protected volatile String LiteralPattern;
	
	public final int index;
	
	public synchronized String GetLiteralPattern() { return LiteralPattern; }
	
	/**
	 * Main constructor of this class.
	 * @param patternWords <b>ArrayList(LWord)</b> : Pattern words list.
	 */
	public LWordPattern(ArrayList<Pair<Integer, Integer>> patternWordsIndexes, int Index)
	{
		index = Index;
		PatternWordsIndexes = patternWordsIndexes;
		ComputeLiteral();
	}
	
	public synchronized void Treat(String sentence, ArrayList<LWordPattern> otherPatternsFound, char sentencePonctuation, FirstPassObject fpo, int sentenceindex) {}
	
	/**
	 * Method to compute word objects to a literal, human-readable sentance.
	 */
	public synchronized void ComputeLiteral()
	{
		LiteralPattern = "";
		int i = 0;
		
		for(Pair<Integer, Integer> ii : PatternWordsIndexes)
		{
			if(i > 0)
				LiteralPattern += " ";
			
			if(ii.GetKey().intValue() == 0)
			{
				if(ii.GetValue().intValue() == -1)
					break;
				else
					LiteralPattern += ProgramCore.GetWordDictionary().GetWord(ii.GetValue().intValue()).GetLiteral();
			}
			else
				LiteralPattern += LEnums.LWordType.values()[ii.GetValue().intValue()].name();
			
			i++;
		}
	}

	/**
	 * Checks if the sentence contains the pattern represented by this object.
	 * @param sentence <b>String</b> : Sentence to test.
	 * @return <b>int</b> : The position of the pattern in the sentence, -1 if the pattern is not present.
	 */
	public synchronized int Match(String sentence)
	{
		sentence = Utils.CleanSentence(sentence);
		String sWords[] = sentence.split(" ");
		
		ProgramCore.GetLogger().log(new LogObject("Trying to match pattern \"" + LiteralPattern + "\" with sentence \"" + sentence + "\"", "WordPattern", LogType.Action, Priority.Low));
		
		for(int i = 0; i < sWords.length; i++)
		{
			if((PatternWordsIndexes.size()-1)+i >= sWords.length || PatternWordsIndexes.size() == 0)
				return -1;
			
			if(PatternWordsIndexes.get(0).GetKey().intValue() == 0)
			{
				if(ProgramCore.GetWordDictionary().GetWord(PatternWordsIndexes.get(0).GetValue().intValue()).equals(sWords[i]))
				{
					boolean match = true;
					
					for(int ii = 1; ii < PatternWordsIndexes.size(); ii++)
					{
						if(PatternWordsIndexes.get(ii).GetKey().intValue() == 0)
						{
							if(!ProgramCore.GetWordDictionary().GetWord(PatternWordsIndexes.get(ii).GetValue().intValue()).equals(sWords[i+ii]))
							{
								match = false;
								break;
							}
						}
						else
						{
							if(ProgramCore.GetWordDictionary().getMatchInType(LEnums.LWordType.values()[PatternWordsIndexes.get(ii).GetValue().intValue()], sWords[i+ii]) == null)
							{
								match = false;
								break;
							}
						}
					}
					
					if(match)
						return i;
				}
			}
			else
			{
				if(ProgramCore.GetWordDictionary().getMatchInType(LEnums.LWordType.values()[PatternWordsIndexes.get(0).GetValue().intValue()], sWords[i]) != null)
				{
					boolean match = true;
					
					for(int ii = 1; ii < PatternWordsIndexes.size(); ii++)
					{
						if(PatternWordsIndexes.get(ii).GetKey().intValue() == 0)
						{
							if(!ProgramCore.GetWordDictionary().GetWord(PatternWordsIndexes.get(ii).GetValue().intValue()).equals(sWords[i+ii]))
							{
								match = false;
								break;
							}
						}
						else
						{
							if(ProgramCore.GetWordDictionary().getMatchInType(LEnums.LWordType.values()[PatternWordsIndexes.get(ii).GetValue().intValue()], sWords[i+ii]) == null)
							{
								match = false;
								break;
							}
						}
					}
					
					if(match)
						return i;
				}
			}
		}
		
		return -1;
	}
	
	/** Returns <b>true</b> if the sentence contains each words of the pattern in no specific order.
	 * @see LWordPattern#Match(String)
	 */
	public synchronized boolean MatchUnordered(String sentence)
	{
		/*sentence = Utils.CleanSentence(sentence);
		String sWords[] = sentence.split(" ");
		
		if(sWords.length < PatternWordsIndexes.size())
			return false;
		
		ArrayList<LWord> PatternWords = new ArrayList<LWord>(PatternWordsIndexes.size());
		
		for(Integer i : PatternWordsIndexes)
		{
			ProgramCore.GetWordDictionary().GetWord(i.intValue());
		}
		
		for(LWord w : PatternWords)
		{
			boolean m = false;
			
			for(int i = 0; i < sWords.length; i++)
			{
				if(w.equals(sWords[i]))
				{
					m = true;
					break;
				}
			}
			
			if(!m)
				return false;
		}
		*/
		return false;
	}
	
	
	public synchronized String RemovePatternFromSentence(String sentence)
	{
		if(Match(sentence) == -1)
			return sentence;
		
		String[] csentence = Utils.CleanSentence(sentence).split(" ");
		
		int index = 0;
		
		for(String word : csentence)
		{
			boolean remove = false;
			
			if(PatternWordsIndexes.get(0).GetKey() == 0)
				if(ProgramCore.GetWordDictionary().GetWord(PatternWordsIndexes.get(0).GetValue().intValue()).equals(word))
					remove = true;
			else
				if(ProgramCore.GetWordDictionary().matchInType(LEnums.LWordType.values()[PatternWordsIndexes.get(0).GetValue().intValue()], word))
					remove = true;
			
			if(remove)
			{
				String nsentence = "";
				
				for(int i = 0; i < index; i++)
				{
					if(i > 0)
						nsentence += " ";
					
					nsentence += csentence[i];
				}
				
				for(int i = index + PatternWordsIndexes.size(); i < csentence.length; i++)
				{
					if(!(i == index + PatternWordsIndexes.size() && nsentence.isEmpty()) || i > index + PatternWordsIndexes.size())
						nsentence += " ";
					
					nsentence += csentence[i];
				}
				
				return nsentence;
			}
			
			index++;
		}
		
		ProgramCore.GetLogger().log(new LogObject("Pattern \"" + LiteralPattern + "\" removed from sentence \"" + sentence + "\"", "WordPattern", LogType.Action, Priority.Low));
		
		return sentence;
	}
}
