package xx.projectzero.linguistic;

import java.util.ArrayList;
import java.util.Arrays;

public final class LEnums
{
	public static enum LWordType
	{
		Unknown,
		Noun,
		Verb,
		Adverb,
		PersonalPronoun,
		DemonstrativePronoun,
		IndefinitePronoun,
		RelativePronoun,
		ReciprocalPronoun,
		InterrogativePronoun,
		Adjective,
		Preposition,
		Interjection,
		Conjonction,
		Article
	}
	
	public static enum LPerson
	{
		FSingular,
		SSingular,
		TSingular,
		FPlural,
		SPlural,
		TPlural
	}
	
	public static enum LTense
	{
		Infinitive,
		SimplePresent,
		Imperative,
		Subjonctive
	}
	
	public static enum LVerbType
	{
		Action,
		Knowing,
		Lang,
		Person,
		Other,
		Unknown
	}
	
	public static final LPerson RespectMark = LPerson.FPlural;
	
	public static ArrayList<Byte> A = new ArrayList<Byte>(Arrays.<Byte>asList((byte)65, (byte)97, (byte)-63, (byte)-62, (byte)-64, (byte)-60, (byte)-61, (byte)-59, (byte)-32, (byte)-31, (byte)-30, (byte)-28, (byte)-29, (byte)-27));
	public static ArrayList<Byte> C = new ArrayList<Byte>(Arrays.<Byte>asList((byte)67, (byte)99, (byte)-25, (byte)-57));
	public static ArrayList<Byte> E = new ArrayList<Byte>(Arrays.<Byte>asList((byte)69, (byte)101, (byte)-24, (byte)-23, (byte)-22, (byte)-21, (byte)-56, (byte)-55, (byte)-54, (byte)-53));
	public static ArrayList<Byte> I = new ArrayList<Byte>(Arrays.<Byte>asList((byte)73, (byte)105, (byte)-20, (byte)-17, (byte)-18, (byte)-19, (byte)-52, (byte)-51, (byte)-50, (byte)-49));
	public static ArrayList<Byte> N = new ArrayList<Byte>(Arrays.<Byte>asList((byte)78, (byte)110, (byte)-15, (byte)-47));
	public static ArrayList<Byte> O = new ArrayList<Byte>(Arrays.<Byte>asList((byte)79, (byte)111, (byte)-12, (byte)-10, (byte)-14, (byte)-11, (byte)-44, (byte)-42, (byte)-46, (byte)-43));
	public static ArrayList<Byte> U = new ArrayList<Byte>(Arrays.<Byte>asList((byte)85, (byte)117, (byte)-7, (byte)-4, (byte)-5, (byte)-39, (byte)-36, (byte)-37));

	public static boolean belongsToSameGroup(byte b1, byte b2)
	{
		if(A.contains(b1))
			if(A.contains(b2))
				return true;
		
		if(C.contains(b1))
			if(C.contains(b2))
				return true;
		
		if(E.contains(b1))
			if(E.contains(b2))
				return true;
		
		if(I.contains(b1))
			if(I.contains(b2))
				return true;
		
		if(N.contains(b1))
			if(N.contains(b2))
				return true;
		
		if(O.contains(b1))
			if(O.contains(b2))
				return true;
		
		if(U.contains(b1))
			if(U.contains(b2))
				return true;
		
		return false;
	}
}
