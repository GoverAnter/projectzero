package xx.projectzero.linguistic;

import java.io.Serializable;
import java.util.ArrayList;

import xx.projectzero.linguistic.LEnums.LWordType;

/**
 * This Class represents a word.
 * @author Guillaume Gravetot
 * @version 1.0.0, 19/04/2016
 * @since 1.1.1, 19/04/2016
 */
public class LWord implements Serializable
{
	private static final long serialVersionUID = 1L;
	protected String Literal;
	protected LWordType Type;
	protected int SynonymArrayIndex;
	protected boolean dataChecked;
	
	public LWord(String LiteralWord, LWordType WordType)
	{
		Literal = LiteralWord;
		Type = WordType;
		
		dataChecked = false;
	}
	
	/** Getter for the literal representation of this word */
	public String GetLiteral() { return Literal; }
	/** Getter for the type of this word */
	public LWordType GetType() { return Type; }
	
	/**
	 * Return a match score between this object and the word to compare with.
	 * @param word
	 * @return
	 */
	public int Compare(LWord word)
	{
		if(word == null || word.GetLiteral() == null)
			return 0;
		
		int match = 100;
		
		if(word.GetLiteral().equals(Literal))
			if(word.GetType() == Type) return match; else return match-5;
		
		if(word.GetLiteral().equalsIgnoreCase(Literal))
			if(word.GetType() == Type) return match-1; else return match-5-1;
		
		int maxLength = Literal.length()>=word.GetLiteral().length()?Literal.length():word.GetLiteral().length();
		int minLength = Literal.length()<=word.GetLiteral().length()?Literal.length():word.GetLiteral().length();
		
		//Threadable Letter comparison
		byte[] wb = word.GetLiteral().getBytes();
		byte[] twb = Literal.getBytes();

		for(int i = 0; i < minLength; i++)
		{
			if(wb[i] == twb[i] || wb[i]+32 == twb[i] || wb[i]-32 == twb[i])
				match -= 0;
			else if(LEnums.belongsToSameGroup(wb[i], twb[i]))
				match -= Math.round((100/maxLength)/5);
			else
				match -= Math.round((105/maxLength));
		}
		
		if(match > 100)
			match = 100;
		if(match < 0)
			match = 0;
		//Threadable end
		
		//Threadable pattern test
		LWord shortest = Literal.length()<word.GetLiteral().length()?this:word;
		LWord longest = Literal.length()>=word.GetLiteral().length()?this:word;
		int currentPos = 0;
		int currentPosT = 0;
		
		while(longest.GetLiteral().length()-currentPos > 3)
		{
			byte[] lb = longest.GetLiteral().getBytes();
			byte[] sb = shortest.GetLiteral().getBytes();
			boolean mat = false;
			
			for(int i = 0; i < sb.length; i++)
			{
				if(i+2 >= sb.length)
					break;
				
				if(lb[currentPos] == sb[i] || lb[currentPos]+32 == sb[i] || lb[currentPos]-32 == sb[i] || LEnums.belongsToSameGroup(lb[currentPos], sb[i]))
				{
					if(lb[currentPos+1] == sb[i+1] || lb[currentPos]+32 == sb[i+1] || lb[currentPos+1]-32 == sb[i+1] || LEnums.belongsToSameGroup(lb[currentPos+1], sb[i+1]))
					{
						if(lb[currentPos+2] == sb[i+2] || lb[currentPos]+32 == sb[i+2] || lb[currentPos+2]-32 == sb[i+2] || LEnums.belongsToSameGroup(lb[currentPos+2], sb[i+2]))
						{
							mat = true;
							currentPosT = i;
							break;
						}
					}
				}
			}
			
			currentPos++;
			
			if(mat)
			{
				int currentPatternSize = 3;
				while(true)
				{
					//Check si le prochain pattern est plus grand que la taille du mot
					if(currentPatternSize+1+currentPosT > shortest.GetLiteral().length())
						break;
					
					//test de la derniere lettre ajout�e au pattern
					if(lb[currentPosT+currentPatternSize] == sb[currentPosT+currentPatternSize] || lb[currentPosT+currentPatternSize]+32 == sb[currentPosT+currentPatternSize] || lb[currentPosT+currentPatternSize]-32 == sb[currentPosT+currentPatternSize] || LEnums.belongsToSameGroup(lb[currentPosT+currentPatternSize], sb[currentPosT+currentPatternSize]))
						currentPatternSize++;
					else
						break;
				}
				
				match += Math.round((currentPatternSize*100)/(maxLength*4));
				currentPos += currentPatternSize;
			}
		}
		
		if(match > 100)
			match = 100;
		if(match < 0)
			match = 0;
		//Threadable end
		
		//Threadable double letter test
		ArrayList<Integer> kpos = new ArrayList<Integer>();
		boolean mat;
		
		for(int i = 0; i < twb.length-1; i++)
		{
			mat = false;
			if(twb[i] == twb[i+1] || twb[i]+32 == twb[i+1] || twb[i]-32 == twb[i+1] || LEnums.belongsToSameGroup(twb[i], twb[i+1]))
			{
				for(int ii = 0; ii < wb.length-1; ii++)
				{
					if((twb[i] == wb[ii] || twb[i]+32 == wb[ii] || twb[i]-32 == wb[ii] || LEnums.belongsToSameGroup(twb[i], wb[ii])) && (twb[i] == wb[ii+1] || twb[i]+32 == wb[ii+1] || twb[i]-32 == wb[ii+1] || LEnums.belongsToSameGroup(twb[i], wb[ii+1])))
					{
						mat = true;
						kpos.add(ii);
						break;
					}
				}
				
				if(mat)
					match += Math.round(200/(maxLength*5));
				else
					match -= Math.round(200/(maxLength*5));
			}
		}
		
		for(int i = 0; i < wb.length-1; i++)
		{
			if(kpos.contains(i))
			{
				i++;
				continue;
			}
			
			mat = false;
			if(wb[i] == wb[i+1] || wb[i]+32 == wb[i+1] || wb[i]-32 == wb[i+1] || LEnums.belongsToSameGroup(wb[i], wb[i+1]))
			{
				for(int ii = 0; ii < twb.length-1; ii++)
				{
					if((wb[i] == twb[ii] || wb[i]+32 == twb[ii] || wb[i]-32 == twb[ii] || LEnums.belongsToSameGroup(wb[i], twb[ii])) && (wb[i] == twb[ii+1] || wb[i]+32 == twb[ii+1] || wb[i]-32 == twb[ii+1] || LEnums.belongsToSameGroup(wb[i], twb[ii+1])))
					{
						mat = true;
						break;
					}
				}
				
				if(mat)
					match += Math.round(200/(maxLength*5));
				else
					match -= Math.round(200/(maxLength*5));
			}
		}
		
		if(match > 100)
			match = 100;
		if(match < 0)
			match = 0;
		//Threadable end
		
		if(word.GetLiteral().length() != Literal.length())
			match -= Math.round((Math.abs(Literal.length() - word.GetLiteral().length())*105)/(maxLength*(Math.max(Math.min(1f/minLength*3.75f,0.75f),0.5f))));
		
		if(match > 100)
			match = 100;
		if(match < 0)
			match = 0;
		
		return match;
	}
	
	public static int Compare(String word1, String word2)
	{
		if(word1 == null || word2 == null)
			return 0;
		
		int match = 100;
		
		if(word2.equals(word1))
			return match;
		
		if(word2.equalsIgnoreCase(word1))
			return match-1;
		
		int maxLength = word1.length()>=word2.length()?word1.length():word2.length();
		int minLength = word1.length()<=word2.length()?word1.length():word2.length();
		
		//Threadable Letter comparison
		byte[] wb = word2.getBytes();
		byte[] twb = word1.getBytes();

		for(int i = 0; i < minLength; i++)
		{
			if(wb[i] == twb[i] || wb[i]+32 == twb[i] || wb[i]-32 == twb[i])
				match -= 0;
			else if(LEnums.belongsToSameGroup(wb[i], twb[i]))
				match -= Math.round((100/maxLength)/5);
			else
				match -= Math.round((105/maxLength));
		}
		
		if(match > 100)
			match = 100;
		if(match < 0)
			match = 0;
		//Threadable end
		
		//Threadable pattern test
		String shortest = word1.length()<word2.length()?word1:word2;
		String longest = word1.length()>=word2.length()?word1:word2;
		int currentPos = 0;
		int currentPosT = 0;
		
		while(longest.length()-currentPos > 3)
		{
			byte[] lb = longest.getBytes();
			byte[] sb = shortest.getBytes();
			boolean mat = false;
			
			for(int i = 0; i < sb.length; i++)
			{
				if(i+2 >= sb.length)
					break;
				
				if(lb[currentPos] == sb[i] || lb[currentPos]+32 == sb[i] || lb[currentPos]-32 == sb[i] || LEnums.belongsToSameGroup(lb[currentPos], sb[i]))
				{
					if(lb[currentPos+1] == sb[i+1] || lb[currentPos]+32 == sb[i+1] || lb[currentPos+1]-32 == sb[i+1] || LEnums.belongsToSameGroup(lb[currentPos+1], sb[i+1]))
					{
						if(lb[currentPos+2] == sb[i+2] || lb[currentPos]+32 == sb[i+2] || lb[currentPos+2]-32 == sb[i+2] || LEnums.belongsToSameGroup(lb[currentPos+2], sb[i+2]))
						{
							mat = true;
							currentPosT = i;
							break;
						}
					}
				}
			}
			
			currentPos++;
			
			if(mat)
			{
				int currentPatternSize = 3;
				while(true)
				{
					//Check si le prochain pattern est plus grand que la taille du mot
					if(currentPatternSize+1+currentPosT > shortest.length())
						break;
					
					//test de la derniere lettre ajout�e au pattern
					if(lb[currentPosT+currentPatternSize] == sb[currentPosT+currentPatternSize] || lb[currentPosT+currentPatternSize]+32 == sb[currentPosT+currentPatternSize] || lb[currentPosT+currentPatternSize]-32 == sb[currentPosT+currentPatternSize] || LEnums.belongsToSameGroup(lb[currentPosT+currentPatternSize], sb[currentPosT+currentPatternSize]))
						currentPatternSize++;
					else
						break;
				}
				
				match += Math.round((currentPatternSize*100)/(maxLength*4));
				currentPos += currentPatternSize;
			}
		}
		
		if(match > 100)
			match = 100;
		if(match < 0)
			match = 0;
		//Threadable end
		
		//Threadable double letter test
		ArrayList<Integer> kpos = new ArrayList<Integer>();
		boolean mat;
		
		for(int i = 0; i < twb.length-1; i++)
		{
			mat = false;
			if(twb[i] == twb[i+1] || twb[i]+32 == twb[i+1] || twb[i]-32 == twb[i+1] || LEnums.belongsToSameGroup(twb[i], twb[i+1]))
			{
				for(int ii = 0; ii < wb.length-1; ii++)
				{
					if((twb[i] == wb[ii] || twb[i]+32 == wb[ii] || twb[i]-32 == wb[ii] || LEnums.belongsToSameGroup(twb[i], wb[ii])) && (twb[i] == wb[ii+1] || twb[i]+32 == wb[ii+1] || twb[i]-32 == wb[ii+1] || LEnums.belongsToSameGroup(twb[i], wb[ii+1])))
					{
						mat = true;
						kpos.add(ii);
						break;
					}
				}
				
				if(mat)
					match += Math.round(200/(maxLength*5));
				else
					match -= Math.round(200/(maxLength*5));
			}
		}
		
		for(int i = 0; i < wb.length-1; i++)
		{
			if(kpos.contains(i))
			{
				i++;
				continue;
			}
			
			mat = false;
			if(wb[i] == wb[i+1] || wb[i]+32 == wb[i+1] || wb[i]-32 == wb[i+1] || LEnums.belongsToSameGroup(wb[i], wb[i+1]))
			{
				for(int ii = 0; ii < twb.length-1; ii++)
				{
					if((wb[i] == twb[ii] || wb[i]+32 == twb[ii] || wb[i]-32 == twb[ii] || LEnums.belongsToSameGroup(wb[i], twb[ii])) && (wb[i] == twb[ii+1] || wb[i]+32 == twb[ii+1] || wb[i]-32 == twb[ii+1] || LEnums.belongsToSameGroup(wb[i], twb[ii+1])))
					{
						mat = true;
						break;
					}
				}
				
				if(mat)
					match += Math.round(200/(maxLength*5));
				else
					match -= Math.round(200/(maxLength*5));
			}
		}
		
		if(match > 100)
			match = 100;
		if(match < 0)
			match = 0;
		//Threadable end
		
		//Threadable
		if(word2.length() != word1.length())
			match -= Math.round((Math.abs(word1.length() - word2.length())*105)/(maxLength*(Math.max(Math.min(1f/minLength*3.75f,0.75f),0.5f))));
		//threadable end
		
		if(match > 100)
			match = 100;
		if(match < 0)
			match = 0;
		
		return match;
	}
	
	/**
	 * Check equality between this word and the word to compare with.
	 * @param word <b>LWord</b> : Word to check equality with.
	 * @return <b>boolean</b> : <b>true</b> if the two words are equal.
	 */
	public boolean equals(LWord word)
	{
		if(word == null || word.GetLiteral() == null)
			return false;
		
		if(word.GetLiteral().equals(Literal))
			return true;
		else if(word.GetLiteral().equalsIgnoreCase(Literal))
			return true;
		
		int minLength = Literal.length()<=word.GetLiteral().length()?Literal.length():word.GetLiteral().length();
		
		byte[] wb = word.GetLiteral().getBytes();
		byte[] twb = Literal.getBytes();

		for(int i = 0; i < minLength; i++)
			if(wb[i] != twb[i] && wb[i]+32 != twb[i] && wb[i]-32 != twb[i] && !LEnums.belongsToSameGroup(wb[i], twb[i]))
				return false;
		
		return true;
	}
	
	/**
	 * Check equality between this word and the string word to compare with.
	 * @param word <b>String</b> : String word to check equality with.
	 * @return <b>boolean</b> : <b>true</b> if the two words are equal.
	 */
	public boolean equals(String word)
	{
		if(word == null || word == null)
			return false;
		
		if(word.equals(Literal))
			return true;
		else if(word.equalsIgnoreCase(Literal))
			return true;
		
		int minLength = Literal.length()<=word.length()?Literal.length():word.length();
		
		byte[] wb = word.getBytes();
		byte[] twb = Literal.getBytes();

		for(int i = 0; i < minLength; i++)
			if(wb[i] != twb[i] && wb[i]+32 != twb[i] && wb[i]-32 != twb[i] && !LEnums.belongsToSameGroup(wb[i], twb[i]))
				return false;
		
		return true;
	}
	
	public static boolean equals(String word1, String word2)
	{
		if(word1 == null || word2 == null)
			return false;
		
		if(word1.length() != word2.length())
			return false;
		
		if(word1.equals(word2))
			return true;
		else if(word1.equalsIgnoreCase(word2))
			return true;
		
		int minLength = word2.length()<=word1.length()?word2.length():word1.length();
		
		byte[] wb = word1.getBytes();
		byte[] twb = word2.getBytes();

		for(int i = 0; i < minLength; i++)
			if(wb[i] != twb[i] && wb[i]+32 != twb[i] && wb[i]-32 != twb[i] && !LEnums.belongsToSameGroup(wb[i], twb[i]))
				return false;
		
		return true;
	}
	
	public boolean hasAllData()
	{
		return true;
	}
}
