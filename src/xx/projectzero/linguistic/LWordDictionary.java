package xx.projectzero.linguistic;

import java.util.ArrayList;

import xx.projectzero.core.MultiMap;
import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.ui.MainViewController.MVTabs;
import xx.projectzero.linguistic.LEnums.LPerson;
import xx.projectzero.linguistic.LEnums.LVerbType;
import xx.projectzero.linguistic.LEnums.LWordType;

/**
 * Dictionary registering every known words.
 * @author Guillaume Gravetot
 * @version 1.1.0, 19/04/2016
 * @since 1.1.1, 19/04/2016
 */
public class LWordDictionary
{//TODO Load/save from/to DB -> possibility for the AI to add new words
	protected volatile MultiMap Words;
	
	/**
	 * Main constructor of this class.
	 */
	public LWordDictionary()
	{
		Words = new MultiMap();
		
		//load functions
		
		LPersonalPronoun pp1 = new LPersonalPronoun("i", "me", "my", "mine", "myself", LPerson.FSingular);
		LPersonalPronoun pp2 = new LPersonalPronoun("you", "you", "your", "yours", "yourself", LPerson.SSingular);
		LPersonalPronoun pp3 = new LPersonalPronoun("he", "him", "his", "his", "himself", LPerson.FSingular);
		LPersonalPronoun pp3b = new LPersonalPronoun("she", "her", "her", "hers", "herself", LPerson.FSingular);
		LPersonalPronoun pp3t = new LPersonalPronoun("it", "it", "its", "its", "itself", LPerson.FSingular);
		LPersonalPronoun pp4 = new LPersonalPronoun("we", "us", "our", "ours", "ourselves", LPerson.FSingular);
		LPersonalPronoun pp5 = new LPersonalPronoun("you", "you", "your", "yours", "yourselves", LPerson.FSingular);
		LPersonalPronoun pp6 = new LPersonalPronoun("they", "them", "their", "theirs", "themselves", LPerson.FSingular);
		
		LVerb v1 = new LVerb("do", LVerbType.Lang, LVerbType.Action);
		LVerb v2 = new LVerb("know", LVerbType.Knowing);
		LVerb v3 = new LVerb("be", LVerbType.Lang, LVerbType.Person);
		v3.PresentThirdPerson = "is";
		LVerb v4 = new LVerb("learn", LVerbType.Action);
		
		LWord p1 = new LWord("this", LWordType.DemonstrativePronoun);
		LWord p2 = new LWord("each other", LWordType.ReciprocalPronoun);
		LWord p3 = new LWord("one another", LWordType.ReciprocalPronoun);
		LWord p4 = new LWord("who", LWordType.RelativePronoun);
		LWord p5 = new LWord("whom", LWordType.RelativePronoun);
		LWord p6 = new LWord("that", LWordType.RelativePronoun);
		LWord p7 = new LWord("which", LWordType.RelativePronoun);
		LWord p8 = new LWord("whoever", LWordType.RelativePronoun);
		LWord p9 = new LWord("whomever", LWordType.RelativePronoun);
		LWord p10 = new LWord("whichever", LWordType.RelativePronoun);
		LWord p11 = new LWord("such", LWordType.DemonstrativePronoun);
		LWord p12 = new LWord("that", LWordType.DemonstrativePronoun);
		LWord p13 = new LWord("these", LWordType.DemonstrativePronoun);
		LWord p14 = new LWord("those", LWordType.DemonstrativePronoun);
		LWord p15 = new LWord("none", LWordType.DemonstrativePronoun);
		LWord p16 = new LWord("neither", LWordType.DemonstrativePronoun);
		LWord p17 = new LWord("what", LWordType.InterrogativePronoun);
		LWord p18 = new LWord("who", LWordType.InterrogativePronoun);
		
		LWord i1 = new LWord("ah", LWordType.Interjection);
		LWord i2 = new LWord("agreed", LWordType.Interjection);
		LWord i3 = new LWord("alright", LWordType.Interjection);
		LWord i4 = new LWord("amen", LWordType.Interjection);
		LWord i5 = new LWord("argh", LWordType.Interjection);
		LWord i6 = new LWord("ay", LWordType.Interjection);
		LWord i7 = new LWord("aye", LWordType.Interjection);
		LWord i8 = new LWord("bah", LWordType.Interjection);
		LWord i9 = new LWord("ha", LWordType.Interjection);
		LWord i10 = new LWord("hey", LWordType.Interjection);
		LWord i11 = new LWord("hmm", LWordType.Interjection);
		LWord i12 = new LWord("hooray", LWordType.Interjection);
		LWord i13 = new LWord("huh", LWordType.Interjection);
		LWord i14 = new LWord("hum", LWordType.Interjection);
		LWord i15 = new LWord("hurray", LWordType.Interjection);
		LWord i16 = new LWord("meh", LWordType.Interjection);
		LWord i17 = new LWord("nah", LWordType.Interjection);
		LWord i18 = new LWord("oh", LWordType.Interjection);
		LWord i19 = new LWord("okay", LWordType.Interjection);
		LWord i20 = new LWord("oy", LWordType.Interjection);
		LWord i21 = new LWord("psst", LWordType.Interjection);
		LWord i22 = new LWord("gosh", LWordType.Interjection);
		LWord i23 = new LWord("roger", LWordType.Interjection);
		LWord i24 = new LWord("shh", LWordType.Interjection);
		LWord i25 = new LWord("ugh", LWordType.Interjection);
		LWord i26 = new LWord("wah", LWordType.Interjection);
		LWord i27 = new LWord("yeah", LWordType.Interjection);
		LWord i28 = new LWord("yo", LWordType.Interjection);
		LWord i29 = new LWord("yep", LWordType.Interjection);
		LWord i30 = new LWord("yup", LWordType.Interjection);
		
		LWord a1 = new LWord("the", LWordType.Article);
		LWord a2 = new LWord("a", LWordType.Article);
		LWord a3 = new LWord("an", LWordType.Article);
		
		AddWord(pp1);
		AddWord(pp2);
		AddWord(pp3);
		AddWord(pp3b);
		AddWord(pp3t);
		AddWord(pp4);
		AddWord(pp5, true); //Bypass double detection cause 2p and 5p has the same literal(eg. "you")
		AddWord(pp6);
		
		AddWord(v1);
		AddWord(v2);
		AddWord(v3);
		AddWord(v4);
		
		AddWord(p1);
		AddWord(p2);
		AddWord(p3);
		AddWord(p4);
		AddWord(p5);
		AddWord(p6);
		AddWord(p7);
		AddWord(p8);
		AddWord(p9);
		AddWord(p10);
		AddWord(p11);
		AddWord(p12);
		AddWord(p13);
		AddWord(p14);
		AddWord(p15);
		AddWord(p16);
		AddWord(p17);
		AddWord(p18);
		
		AddWord(i1);
		AddWord(i2);
		AddWord(i3);
		AddWord(i4);
		AddWord(i5);
		AddWord(i6);
		AddWord(i7);
		AddWord(i8);
		AddWord(i9);
		AddWord(i10);
		AddWord(i11);
		AddWord(i12);
		AddWord(i13);
		AddWord(i14);
		AddWord(i15);
		AddWord(i16);
		AddWord(i17);
		AddWord(i18);
		AddWord(i19);
		AddWord(i20);
		AddWord(i21);
		AddWord(i22);
		AddWord(i23);
		AddWord(i24);
		AddWord(i25);
		AddWord(i26);
		AddWord(i27);
		AddWord(i28);
		AddWord(i29);
		AddWord(i30);
		
		AddWord(a1);
		AddWord(a2);
		AddWord(a3);
		
		ProgramCore.io().printlntotab("Registered " + String.valueOf(Words.size()) + " words in the dictionary", MVTabs.Analyze);
	}
	
	/**
	 * Return the Word object by the literal word and his type.
	 * @param LiteralWorld <b>String</b> : Literal word.
	 * @param WordType <b>LWordType</b> : Type of the word.
	 * @return <b>LWord</b> : Word Object.
	 */
	public synchronized LWord GetWord(int index)
	{
		if(index >= Words.size() || index == -1)
			return null;
		
		return Words.get(index);
	}
	
	/**
	 * Return the Word object by the literal word and his type.
	 * @param LiteralWorld <b>String</b> : Literal word.
	 * @param WordType <b>LWordType</b> : Type of the word.
	 * @return <b>LWord</b> : Word Object.
	 */
	public synchronized LWord GetWord(String LiteralWorld, LWordType WordType)
	{
		if(!Words.contains(LiteralWorld, WordType))
			return null;
		
		return Words.get(LiteralWorld, WordType);
	}
	
	/**
	 * Return the Word index by the literal word and his type.
	 * @param LiteralWorld <b>String</b> : Literal word.
	 * @param WordType <b>LWordType</b> : Type of the word.
	 * @return <b>int</b> : Word index.
	 */
	public synchronized int GetWordIndex(String LiteralWorld, LWordType WordType)
	{
		if(!Words.contains(LiteralWorld, WordType))
			return -1;
		
		return Words.indexOf(LiteralWorld, WordType);
	}
	
	/**
	 * Add a word to the dictionary.
	 * @param word <b>LWord</b> : Word Object to add.
	 * @return <b>int</b> : Index of the newly placed word.
	 * @see LWordDictionnary#AddWord(LWord, boolean)
	 */
	public synchronized int AddWord(LWord word)
	{
		return AddWord(word, false);
	}
	
	/**
	 * Add a word to the dictionary.
	 * @param word <b>LWord</b> : Word Object to add.
	 * @param overwrite <b>boolean</b> : <b>true</b> to replace if existing.
	 * @return <b>int</b> : Index of the newly placed word.
	 */
	public synchronized int AddWord(LWord word, boolean overwrite)
	{
		if(Words.contains(word.GetLiteral(), word.GetType()) && !overwrite)
			return -1;
		
		return Words.put(word.GetLiteral(), word.GetType(), word);
	}
	
	/** Returns <b>true</b> if the specified word is known in the specified category. */
	public synchronized boolean matchInType(LWordType type, String word)
	{
		ArrayList<LWord> pmatch = Words.getAll(type);
		
		for(LWord lWord : pmatch)
		{
			if(lWord.equals(word))
				return true;
		}
		
		return false;
	}
	
	/** Return the associated Word object if found, null otherwise. */
	public synchronized LWord getMatchInType(LWordType type, String word)
	{
		ArrayList<LWord> pmatch = Words.getAll(type);
		
		for(LWord lWord : pmatch)
		{
			if(lWord.equals(word))
				return Words.get(lWord.GetLiteral(), lWord.GetType());
		}
		
		return null;
	}
}
