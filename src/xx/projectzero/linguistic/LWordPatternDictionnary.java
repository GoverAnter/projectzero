package xx.projectzero.linguistic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

import xx.projectzero.SchedulerThread.AnalyzeObjects.FirstPassObject;
import xx.projectzero.SchedulerThread.ComparatorDataStruct;
import xx.projectzero.core.Pair;
import xx.projectzero.core.ProgramCore;
import xx.projectzero.core.Utils;
import xx.projectzero.core.ui.MainViewController.MVTabs;
import xx.projectzero.linguistic.LEnums.LWordType;

public class LWordPatternDictionnary
{//TODO Load/save from/to DB -> possibility for the AI to add new patterns
	protected volatile ArrayList<LWordPattern> Patterns;
	
	public LWordPatternDictionnary()
	{
		Patterns = new ArrayList<LWordPattern>();
		
		ArrayList<Pair<Integer, Integer>> pw = new ArrayList<Pair<Integer, Integer>>();
		pw.add(new Pair<Integer, Integer>(0, ProgramCore.GetWordDictionary().GetWordIndex("do", LWordType.Verb)));
		pw.add(new Pair<Integer, Integer>(0, ProgramCore.GetWordDictionary().GetWordIndex("you", LWordType.PersonalPronoun)));
		pw.add(new Pair<Integer, Integer>(0, ProgramCore.GetWordDictionary().GetWordIndex("know", LWordType.Verb)));
		
		LWordPattern p = new LWordPattern(pw, 1) {
			@Override
			public synchronized void Treat(String sentence, ArrayList<LWordPattern> otherPatternsFound, char sentencePonctuation, FirstPassObject fpo, int sentenceindex)
			{
				if(otherPatternsFound != null && otherPatternsFound.size() != 0)
				{
					if(otherPatternsFound.get(0).index == 2 || otherPatternsFound.get(0).index == 3)
					{
						if(otherPatternsFound.size() == 1)
						{
							String nudeSentence = otherPatternsFound.get(0).RemovePatternFromSentence(RemovePatternFromSentence(sentence));
							nudeSentence = Utils.CleanSpaces(nudeSentence.trim());
							
							if(nudeSentence.split(" ").length >= 1 && nudeSentence.split(" ").length <= 3)
								ProgramCore.GetScheduler().ScheduleNewComparator(new ComparatorDataStruct(nudeSentence, fpo, sentenceindex));
						}
					}
				}
			}
		};
		AddPattern(p);
		
		pw = new ArrayList<Pair<Integer, Integer>>();
		pw.add(new Pair<Integer, Integer>(0, ProgramCore.GetWordDictionary().GetWordIndex("what", LWordType.InterrogativePronoun)));
		pw.add(new Pair<Integer, Integer>(0, ProgramCore.GetWordDictionary().GetWordIndex("be", LWordType.Verb)));
		pw.add(new Pair<Integer, Integer>(1, LWordType.Article.ordinal()));
		
		p = new LWordPattern(pw, 2) {
			@Override
			public synchronized void Treat(String sentence, ArrayList<LWordPattern> otherPatternsFound, char sentencePonctuation, FirstPassObject fpo, int sentenceindex)
			{
				if(otherPatternsFound != null)
				{
					if(otherPatternsFound.size() == 0)
					{
						String nudeSentence = RemovePatternFromSentence(sentence);
						nudeSentence = Utils.CleanSpaces(nudeSentence.trim());
						
						if(nudeSentence.split(" ").length >= 1 && nudeSentence.split(" ").length <= 3)
							ProgramCore.GetScheduler().ScheduleNewComparator(new ComparatorDataStruct(nudeSentence, fpo, sentenceindex));
					}
				}
			}
		};
		AddPattern(p);
		
		pw = new ArrayList<Pair<Integer, Integer>>();
		pw.add(new Pair<Integer, Integer>(0, ProgramCore.GetWordDictionary().GetWordIndex("who", LWordType.InterrogativePronoun)));
		pw.add(new Pair<Integer, Integer>(0, ProgramCore.GetWordDictionary().GetWordIndex("be", LWordType.Verb)));
		
		p = new LWordPattern(pw, 3) {
			@Override
			public synchronized void Treat(String sentence, ArrayList<LWordPattern> otherPatternsFound, char sentencePonctuation, FirstPassObject fpo, int sentenceindex)
			{
				if(otherPatternsFound != null)
				{
					if(otherPatternsFound.size() == 0)
					{
						String nudeSentence = RemovePatternFromSentence(sentence);
						nudeSentence = Utils.CleanSpaces(nudeSentence.trim());
						
						if(nudeSentence.split(" ").length >= 1 && nudeSentence.split(" ").length <= 3)
							ProgramCore.GetScheduler().ScheduleNewComparator(new ComparatorDataStruct(nudeSentence, fpo, sentenceindex));
					}
				}
			}
		};
		AddPattern(p);
		
		ProgramCore.io().printlntotab("Registered " + String.valueOf(Patterns.size()) + " patterns in the dictionary", MVTabs.Analyze);
	}
	
	/** Return the first pattern matching the sentence. */
	public synchronized LWordPattern FindMatchingPattern(String sentence)
	{
		for(LWordPattern p : Patterns)
		{
			if(p.Match(sentence) != -1)
				return p;
		}
		
		return null;
	}
	
	/** Return all the patterns matching the sentence. */
	public synchronized ArrayList<LWordPattern> FindMatchingPatterns(String sentence)
	{
		ArrayList<LWordPattern> pts = new ArrayList<LWordPattern>();
		TreeMap<Integer,LWordPattern> m = new TreeMap<Integer,LWordPattern>();
		
		ProgramCore.io().printlntotab("Checking pattern matches with " + String.valueOf(Patterns.size()) + " patterns", MVTabs.Analyze);
		long startTime = System.nanoTime();
		
		for(LWordPattern p : Patterns)
		{
			int n = p.Match(sentence);
			if(n != -1)
				m.put(n, p);
		}
		
		for(LWordPattern lWordPattern : m.values())
			pts.add(lWordPattern);
		
		long endTime = System.nanoTime();
		ProgramCore.io().printlntotab("Search done in " + String.valueOf((endTime - startTime)/1000000d) + "ms, found " + String.valueOf(pts.size()) + " matches", MVTabs.Analyze);

		return pts;
	}
	
	/** Register a pattern in the dictionary. */
	public synchronized void AddPattern(LWordPattern p)
	{
		Patterns.add(p);
	}
}
