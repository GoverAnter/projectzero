package xx.projectzero.linguistic;

import xx.projectzero.linguistic.LEnums.LPerson;
import xx.projectzero.linguistic.LEnums.LWordType;

public class LPersonalPronoun extends LWord
{
	private static final long serialVersionUID = 1L;
	
	protected String PAsObject;
	protected String PAsPossessive;
	protected String Possessive;
	protected String Reflexive;
	
	protected LPerson Person;
	
	public String getPAsObject() { return PAsObject; }
	public String getPAsPossessive() { return PAsPossessive; }
	public String getPossessive() { return Possessive; }
	public String getReflexive() { return Reflexive; }
	public LPerson getPerson() { return Person; }
	
	public void setPAsObject(String pAsObject) { PAsObject = pAsObject; }
	public void setPAsPossessive(String pAsPossessive) { PAsPossessive = pAsPossessive; }
	public void setPossessive(String possessive) { Possessive = possessive; }
	public void setReflexive(String reflexive) { Reflexive = reflexive; }
	public void setPerson(LPerson person) { Person = person; }

	public LPersonalPronoun(String LiteralWord)
	{
		super(LiteralWord, LWordType.PersonalPronoun);
	}
	
	public LPersonalPronoun(String Literal, String AsObject, String AsPossessive, String possessive, String reflexive, LPerson person)
	{
		super(Literal, LWordType.PersonalPronoun);
		
		PAsObject = AsObject;
		PAsPossessive = AsPossessive;
		Possessive = possessive;
		Reflexive = reflexive;
		Person = person;
	}
	
	@Override
	public boolean hasAllData()
	{
		if(dataChecked)
			return true;
		
		if(Person != null && PAsObject != null && PAsObject.length() != 0 && PAsPossessive != null && PAsPossessive.length() != 0 && Possessive != null && Possessive.length() != 0 && Reflexive != null && Reflexive.length() != 0)
		{
			dataChecked = true;
			return true;
		}
		
		return false;
	}
}
