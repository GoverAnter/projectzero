package xx.projectzero.linguistic;

import java.util.ArrayList;
import java.util.Arrays;

import xx.projectzero.core.ProgramCore;
import xx.projectzero.linguistic.LEnums.LPerson;
import xx.projectzero.linguistic.LEnums.LTense;
import xx.projectzero.linguistic.LEnums.LVerbType;
import xx.projectzero.linguistic.LEnums.LWordType;

public class LVerb extends LWord
{
	private static final long serialVersionUID = 1L;
	
	public ArrayList<LVerbType> VerbType;
	
	public String PresentThirdPerson;
	public String PastTenseForm;
	public String PastParticipleForm;
	public String GerundForm;

	public LVerb(String InfinitiveForm, LVerbType... verbType)
	{
		super(InfinitiveForm, LWordType.Verb);
		
		VerbType = new ArrayList<LVerbType>();
		VerbType.addAll(Arrays.asList(verbType));
		
		ComputeThirdPersonForm();
		ComputePastTenseForm();
		ComputeGerundForm();
		
		PastParticipleForm = PastTenseForm;
	}
	
	/** Compute the internal infinitive form to the third person present form. */
	public void ComputeThirdPersonForm()
	{
		if(Literal.endsWith("y"))
		{
			byte[] ending = "ies".getBytes();
			byte[] verb = Literal.getBytes();
			
			byte[] Final = new byte[verb.length+2];
			
			for(int i=0; i < verb.length-1;i++)
				Final[i] = verb[i];
			
			for(int i = 0; i < ending.length; i++)
			{
				Final[i+verb.length-1] = ending[i];
			}
			
			PresentThirdPerson = new String(Final);
		}
		else if(Literal.endsWith("s") || Literal.endsWith("z") || Literal.endsWith("ch") || Literal.endsWith("sh") || Literal.endsWith("o"))
			PresentThirdPerson = Literal + "es";
		else
			PresentThirdPerson = Literal + "s";
	}
	
	/** Compute the internal infinitive form to the past tense form. */
	public void ComputePastTenseForm()
	{
		if(Literal.endsWith("y"))
		{
			byte[] ending = "ied".getBytes();
			byte[] verb = Literal.getBytes();
			
			byte[] Final = new byte[verb.length+2];
			
			for(int i=0; i < verb.length-1;i++)
				Final[i] = verb[i];
			
			for(int i = 0; i < ending.length; i++)
			{
				Final[i+verb.length-1] = ending[i];
			}
			
			PastTenseForm = new String(Final);
		}
		else if(Literal.endsWith("e"))
			PastTenseForm = Literal + "d";
		else if(Literal.endsWith("c"))
			PastTenseForm = Literal + "ked";
		else if(Literal.endsWith("l") || Literal.endsWith("b") || Literal.endsWith("p") || Literal.endsWith("s") || Literal.endsWith("t") || Literal.endsWith("m"))
		{
			byte[] verb = Literal.getBytes();
			
			byte[] Final = new byte[verb.length+1];
			
			for(int i=0; i < verb.length+1;i++)
			{
				if(i != verb.length)
					Final[i] = verb[i];
				else
					Final[i] = verb[i-1];
			}
			
			PastTenseForm = (new String(Final))+"ed";
		}
		else
			PastTenseForm = Literal + "ed";
	}
	
	/** Compute the internal infinitive form to the gerund form. */
	public void ComputeGerundForm()
	{
		if(Literal.endsWith("ie"))
		{
			byte[] verb = Literal.getBytes();
			
			byte[] Final = new byte[verb.length-2];
			
			for(int i=0; i < verb.length-2;i++)
				Final[i] = verb[i];
			
			GerundForm = (new String(Final))+"ying";
		}
		else if(Literal.endsWith("e"))
		{
			byte[] verb = Literal.getBytes();
			
			byte[] Final = new byte[verb.length-1];
			
			for(int i=0; i < verb.length-1;i++)
				Final[i] = verb[i];
			
			GerundForm = (new String(Final))+"ing";
		}
		else if(Literal.endsWith("c"))
			GerundForm = Literal + "king";
		else if(Literal.endsWith("l") || Literal.endsWith("b") || Literal.endsWith("p") || Literal.endsWith("s") || Literal.endsWith("t") || Literal.endsWith("m"))
		{
			byte[] verb = Literal.getBytes();
			
			byte[] Final = new byte[verb.length+1];
			
			for(int i=0; i < verb.length+1;i++)
			{
				if(i != verb.length)
					Final[i] = verb[i];
				else
					Final[i] = verb[i-1];
			}
			
			GerundForm = (new String(Final))+"ing";
		}
		else
			GerundForm = Literal + "ing";
	}
	
	/** Returns <b>true</b> if the specified word is a verb(known or procedurally tested). */
	public static boolean IsVerb(String word)
	{
		if(ProgramCore.GetWordDictionary().matchInType(LWordType.Verb, word) || word.endsWith("ing") || word.endsWith("ed"))
			return true;
		
		//TODO Improve this.
		
		return false;
	}
	
	/** Returns the verb represented by this object conjugated with the person and the tense provided. */
	public String GetConjugatedVerb(LPerson Person, LTense Tense)
	{
		switch(Tense)
		{
			case Infinitive:
				return "to " + Literal;
			case Imperative:
			case Subjonctive:
				return Literal;
			case SimplePresent:
				if(Person == LPerson.TSingular)
					return PresentThirdPerson;
				else
					return Literal;
			default:
				break;
		}
		
		return Literal;
	}
	
	@Override
	public boolean equals(String word)
	{
		if(super.equals(word) || LWord.equals(word, PastTenseForm) || LWord.equals(word, GerundForm) || LWord.equals(word, PresentThirdPerson) || LWord.equals(word, PastParticipleForm))
			return true;
		else
			return false;
	}
	
	public static LVerb GetCorrespondingVerb(String verbalWord)
	{
		return (LVerb)ProgramCore.GetWordDictionary().getMatchInType(LWordType.Verb, verbalWord);
	}
}
