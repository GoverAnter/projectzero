package xx.projectzero.learning.core;

import java.io.Serializable;

/**
 * Interface parentes de toutes les classes d'apprentissage.
 * @author Guillaume Gravetot
 * @version 1.2, 02/08/2016
 * @since 1.0.0, 12/04/2016
 */
public interface MasterClass extends Serializable
{
	@Override
	public abstract String toString();
	
	public abstract void reloadInstance();
	
	public abstract void loadInstance(String InstanceName);
	
	public default void saveCurrentInstance()
	{
		saveCurrentInstance(true);
	}
	
	public abstract void saveCurrentInstance(boolean override);
	
	public abstract String safeSaveCurrentInstance();
}
