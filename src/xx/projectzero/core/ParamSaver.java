package xx.projectzero.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.TreeMap;

import xx.projectzero.core.Archiver.Type;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.exceptions.InvalidArgumentException;
import xx.projectzero.exceptions.OverrideException;
import xx.projectzero.exceptions.UnknownKeyException;

/**
 * Class managing isolated params save.
 * @author Guillaume Gravetot
 * @version 1.0.0, 08/10/2016
 * @since 1.2.0, 08/10/2016
 */
public class ParamSaver
{
	private volatile TreeMap<String, String> savedParams;
	
	/** Main constructor of this class. */
	public ParamSaver()
	{
	}
	
	public void Init()
	{
		Load();
	}
	
	private void Load()
	{
		savedParams = new TreeMap<String, String>();
		
		try
		{
			savedParams = (TreeMap<String, String>)Utils.Deserialize(new String(Files.readAllBytes(Paths.get(ProgramCore.BasePath + "params.sp"))));
		} catch (IOException e)
		{
			if(ProgramCore.GetArchiver().RestoreAnyArchive(Type.ParamSavedFile))
				Load();
		}
	}
	
	/**
	 * Method to save a new param.
	 * @param paramName <b>String</b> : The name of the param to save.
	 * @param serializedParam <b>String</b> : The param to save in a serialized form.
	 * @param override <b>boolean</b> : Specifies if the save should override an existing value.
	 * @throws InvalidArgumentException Thrown when the param name is null or empty and when the serialized param is null.
	 * @throws OverrideException Thrown when the param is already registered and override is set to false.
	 * @see Utils#Serialize(Object)
	 * @see ParamSaver#Load(String)
	 */
	public synchronized void Save(String paramName, String serializedParam, boolean override) throws InvalidArgumentException, OverrideException
	{
		if(paramName == null || paramName.length() == 0)
			throw new InvalidArgumentException("paramName", paramName==null?"null":"");
		if(serializedParam == null)
			throw new InvalidArgumentException("serializedParam", "null");
		
		if(savedParams.containsKey(paramName) && !override)
			throw new OverrideException();
		
		savedParams.put(paramName, serializedParam);
		
		try
		{
			Files.write(Paths.get(ProgramCore.BasePath + "params.sp"), Utils.Serialize(savedParams).getBytes());
		} catch (IOException e)
		{
			ProgramCore.io().println("Error Code : 0x101000");
			ProgramCore.GetLogger().log(new LogObject("0x101000 : Cannot save params to file : " + e.getMessage(), "ParamSaver", LogType.Error, Priority.High));
			return;
		}
		
		ProgramCore.GetArchiver().archive(Archiver.Type.ParamSavedFile, false, true);
	}
	
	/**
	 * Method to load a saved param.
	 * @return <b>String</b> : Serialized form of the saved param.
	 * @param paramName <b>String</b> : The name of the param to load.
	 * @throws InvalidArgumentException The param name is null or empty.
	 * @throws UnknownKeyException The param name is unknown.
	 * @see Utils#Deserialize(String)
	 * @see ParamSaver#Save(String, String, boolean)
	 */
	public synchronized String Load(String paramName) throws InvalidArgumentException, UnknownKeyException
	{
		if(paramName == null || paramName.length() == 0)
			throw new InvalidArgumentException("paramName", paramName==null?"null":"");
		
		if(!savedParams.containsKey(paramName))
			throw new UnknownKeyException();
		
		return savedParams.get(paramName);
	}
}
