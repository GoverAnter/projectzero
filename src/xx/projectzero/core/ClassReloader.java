package xx.projectzero.core;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

/**
 * Classe gerant le chargement dynamique des classes <i>Learning</i>.
 * @author Guillaume Gravetot
 * @version 1.0.0, 12/04/2016
 * @since 1.0.0, 12/04/2016
 */
@SuppressWarnings("rawtypes")
public class ClassReloader
{
	/**
	 * ClassLoader r�impl�ment� pr�vu pour �tre utilise en interne par <i>ClassReloader</i>
	 * @version 1.0.0, 10/04/2016
	 * @see ClassReloader
	 */
	private class CClassLoader extends ClassLoader
	{
		public CClassLoader(ClassLoader parent)
		{
			super(parent);
		}
		
		public Class LoadClass(String completeClass) throws ClassNotFoundException
		{
			try
			{
				URL classURL = new URL("file:" + ProgramCore.BaseBINPath +Utils.GetCompletePathFromCompleteName(completeClass)+".class");
				URLConnection conn = classURL.openConnection();
				InputStream input = conn.getInputStream();
				ByteArrayOutputStream buffer = new ByteArrayOutputStream();
				int data = input.read();
				
				while(data != -1)
				{
					buffer.write(data);
					data = input.read();
				}
				
				input.close();
				
				byte[] classData = buffer.toByteArray();
				
				return defineClass(completeClass, classData, 0, classData.length);
				
			} catch(IOException e)
			{
				ProgramCore.io().println("Error Code : 0x021000");
			}
			
			return null;
		}
	}
	
	/**
	 * Structure de sauvegarde des classes � recharger au d�marrage du programe.<br>
	 * Pr�vu pour �tre utilis� en interne par <i>ClassReloader</i>
	 * @see ClassReloader
	 */
	private class SaveStruct
	{
		private List<String> classLoaded;
		
		public SaveStruct()
		{
			classLoaded = new ArrayList<String>();
		}
		
		public void addClassToLoad(String completeClass)
		{
			if(!classLoaded.contains(completeClass))
				classLoaded.add(completeClass);
		}
		
		public String[] getClasses()
		{
			return classLoaded.toArray(new String[classLoaded.size()]);
		}
		
		public void setClasses(String[] cl)
		{
			classLoaded = new ArrayList<String>(Arrays.asList(cl));
		}
	}
	
	private SaveStruct loadStruct;
	private String XMLPath;
	private TreeMap<String, CClassLoader> AssociatedClassLoaders;
	
	/**
	 * Constructeur principal de ClassReloader.
	 */
	public ClassReloader()
	{
		XMLPath = "C:/Users/GoverAnter/workspace/ProjectZero/classes.xml";
		loadStruct = new SaveStruct();
		AssociatedClassLoaders = new TreeMap<String, CClassLoader>();
	}
	
	/**
	 * M�thode � appeler quand tous les objets prinpipaux ont �t� instanti�s.
	 */
	public void init()
	{
		loadClassFromXML();
		
		try
		{
			ProgramCore.GetEventManager().Register("Exit", this, this.getClass().getMethod("Stop"));
		} catch (NoSuchMethodException | SecurityException e)
		{
			ProgramCore.io().println("Error Code : 0x021500");
		}
	}
	
	public void Stop()
	{
		saveClasses();
		ProgramCore.GetEventManager().Fire("Exiting");
		System.out.println("Exiting ClassReloader");
	}
	
	/** Sauvegarde la classe sp�cifi�e pour rechargement ulterieur */
	private void saveClassToXML(String completeClass)
	{
		loadStruct.addClassToLoad(completeClass);
		
		try
		{
			XMLEncoder encoder;
			encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(XMLPath)));
			encoder.writeObject(loadStruct.getClasses());
			encoder.close();
		} catch (FileNotFoundException e)
		{
			ProgramCore.io().println("Error Code : 0x021100");
		}
	}
	
	/**
	 * Force la sauvegarde des classes en m�moire.<br>
	 * <i><b>M�thode de secours et de test.</b></i>
	 */
	public void saveClasses()
	{
		try
		{
			XMLEncoder encoder;
			encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(XMLPath)));
			encoder.writeObject(loadStruct.getClasses());
			encoder.close();
		} catch (FileNotFoundException e)
		{
			ProgramCore.io().println("Error Code : 0x021200");
			return;
		}
		
		ProgramCore.GetArchiver().archive(Archiver.Type.ReloaderClassList, false, true);
	}
	
	private void loadClassFromXML()
	{
		if(Files.exists(Paths.get(XMLPath), LinkOption.NOFOLLOW_LINKS))
		{
			try
			{
				XMLDecoder decoder;
				decoder = new XMLDecoder(new FileInputStream(XMLPath));
				String[] array = (String[])decoder.readObject();
				loadStruct.setClasses(array);
			    decoder.close();
			    
			    for(int i = 0; i < array.length; i++)
			    {
					loadClass(array[i]);
			    }
			    
			} catch (IOException e)
			{
				ProgramCore.io().println("Error Code : 0x021300");
				if(ProgramCore.GetArchiver().RestoreAnyArchive(Archiver.Type.ReloaderClassList))
					loadClassFromXML();
			}
		}
		else if(ProgramCore.GetArchiver().RestoreAnyArchive(Archiver.Type.ReloaderClassList))
			loadClassFromXML();
	}//TODO Try to stop loop if archives are corrupted
	
	/**
	 * M�thode pour charger une classe dynamiquement.
	 * @param ClassName <b>String</b> : Nom complet de la classe (eg. com.package.class).
	 * @param saveForReload <b>boolean</b> : <b>true</b> pour enregistrer la classe pour qu'elle soit recharg�e a chaque <i>init</i>.
	 * @param registerNewClass <b>boolean</b> : <b>true</b> pour enregistrer la classe au pr�s de <i>ClassManager</i>.
	 * @return <b>Class</b> : La classe charg�e dynamiquement.
	 * @see CClassLoader#loadClass(String)
	 */
	public Class loadClass(String ClassName, boolean saveForReload, boolean registerNewClass)
	{
		AssociatedClassLoaders.put(ClassName, new CClassLoader(ClassReloader.class.getClassLoader()));
		
		try
		{
			Class c = AssociatedClassLoaders.get(ClassName).LoadClass(ClassName);
			
			if(saveForReload)
				saveClassToXML(ClassName);
			
			if(registerNewClass)
				ProgramCore.GetClassManager().AddClassToReference(c);
			else return c;
		} catch(ClassNotFoundException e)
		{
			ProgramCore.io().println("Error Code : 0x021400");
		}
		
		return null;
	}
	
	/**
	 * Appelle implicitement la m�thode loadClass(ClassName, true, true).
	 * @param ClassName <b>String</b> : Nom de la classe � charger.
	 * @see ClassReloader#loadClass(String, boolean, boolean)
	 */
	public void loadClass(String ClassName)
	{
		loadClass(ClassName, true, true);
	}
}
