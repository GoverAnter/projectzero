package xx.projectzero.core;

public class Pair <K,V>
{
	private K Key;
	private V Value;
	
	public Pair(K key, V value)
	{
		Key = key;
		Value = value;
	}
	
	public K GetKey()
	{
		return (K)Key;
	}
	
	public V GetValue()
	{
		return (V)Value;
	}
}
