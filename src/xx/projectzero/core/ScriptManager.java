package xx.projectzero.core;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import xx.projectzero.console.Command;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.Utils.ProgramClass;
import xx.projectzero.core.ui.MainViewController.MVTabs;

public class ScriptManager
{
	private ScriptEngineManager sem;
	private ScriptEngine se;
	private String baseScriptPath;
	
	public ScriptManager()
	{
		sem = new ScriptEngineManager();
		se = sem.getEngineByName("JavaScript");
		baseScriptPath = ProgramCore.BasePath + "scripts/";
		
		//REGION Context
		se.put("classWriter", ProgramCore.GetClassWriter());
		se.put("eventManager", ProgramCore.GetEventManager());
		se.put("logger", ProgramCore.GetLogger());
		se.put("io", ProgramCore.io());
		//ENDREGION
		
		try
		{
			ProgramCore.GetEventManager().Register("Exit", this, this.getClass().getMethod("onclose"));
			ProgramCore.GetLogger().log(new LogObject("Event \"Exit\" registered successfully", "ScriptManager", LogType.Info, Priority.Low));
		} catch(NoSuchMethodException | SecurityException e)
		{
			ProgramCore.io().println("Error Code : 0x0F1000");
			ProgramCore.GetLogger().log(new LogObject("Error while registering event \"Exit\"", "ScriptManager", LogType.Error, Priority.High));
		}
		
		ProgramCore.GetLogger().log(new LogObject("Adding command \"script\" to console", "ScriptManager", LogType.Action, Priority.Low));
		ProgramCore.GetConsole().AddCommand(new Command("script") {
			@Override
			public void Exec(String[] params)
			{
				if(params.length == 0)
					PrintHelp();
				else if(Utils.ArrayContains(params,"-h") || Utils.ArrayContains(params,"--help"))
					PrintHelp();
				else if(Utils.ArrayContains(params,"-n") || Utils.ArrayContains(params,"--name"))
				{
					int ind = Utils.GetIndex(params, "-n");
					
					if(ind == -1)
					{
						ind = Utils.GetIndex(params, "--name");
						
						if(ind == -1)
						{
							ProgramCore.io().printlntotab("Unknown argument", MVTabs.Console);
							PrintHelp();
							return;
						}
					}
					
					if(ind +1 == params.length)
					{
						ProgramCore.io().printlntotab("Unknown argument", MVTabs.Console);
						PrintHelp();
						return;
					}
					
					String script = params[ind+1];
					script = baseScriptPath + script + ".js";
					Call(Paths.get(script));
				}
				else if(Utils.ArrayContains(params, "-l") || Utils.ArrayContains(params, "--list"))
				{
					String[] files = new File(baseScriptPath).list();
					
					if(files.length <= 2)
						ProgramCore.io().printlntotab("No scripts found", MVTabs.Console);
					else
						ProgramCore.io().printlntotab("Scripts list :", MVTabs.Console);
					
					for(String s : files)
						if(s.endsWith((".js")))
							ProgramCore.io().printlntotab("\t" + s.replace(".js", ""), MVTabs.Console);
				}
				else if(Utils.ArrayContains(params, "-f") || Utils.ArrayContains(params, "--folder"))
				{
					ProgramCore.io().printlntotab("Opening script folder...", MVTabs.Console);
					try { Desktop.getDesktop().open(new File(baseScriptPath)); } catch (IOException e) { ProgramCore.io().printlntotab("Cannot open script folder", MVTabs.Console); }
				}
			}
			
			private void PrintHelp()
			{
				ProgramCore.io().printlntotab("Help text for script command.\n\nscript [-option]\n\nOptions:\n\t-h, --help : Display this help. Ignores other options.\n\t-n, --name : Followed by the name of the script to execute.\n\tThe script must be placed in the \"scripts\" folder, and the name should not contains any extensions.\n\t-f, --folder : Opens the script folder in the explorer.\n\t-l, --list : Lists all scripts in script folder.", MVTabs.Console);
			}
		});
	}
	
	public void onstart()
	{
		ProgramCore.GetLogger().log(new LogObject("Calling onstart scripts", "ScriptManager", LogType.Action, Priority.Medium));
		
		File onstart = new File(baseScriptPath + "onstart/");
		File[] filelist = onstart.listFiles();
		
		for(File file : filelist)
			if(file.isFile())
				Call(file);
		
		ProgramCore.GetLogger().log(new LogObject("onstart scripts called", "ScriptManager", LogType.Info, Priority.Low));
	}
	
	public void onclose()
	{
		ProgramCore.GetLogger().log(new LogObject("Calling onclose scripts", "ScriptManager", LogType.Action, Priority.Medium));
		
		File onclose = new File(baseScriptPath + "onclose/");
		
		File[] filelist = onclose.listFiles();
		
		for(File file : filelist)
			if(file.isFile())
				Call(file);
		
		ProgramCore.GetLogger().log(new LogObject("onclose scripts called, calling event back...", "ScriptManager", LogType.Info, Priority.Low));
		
		//Event callback
		ProgramCore.GetEventManager().Fire("Exiting");
		System.out.println("Exiting ScriptManager");
	}
	
	public Object Call(String script)
	{
		ProgramCore.GetLogger().log(new LogObject("Calling new script", "ScriptManager", LogType.Action, Priority.Medium));
		
		try { se.eval(script);
		} catch (ScriptException e) { ProgramCore.GetErrorReporter().Report(script, e, ProgramClass.ScriptManager); ProgramCore.io().println("Error Code : 0x0E1100"); }
		
		Invocable inv = (Invocable)se;
		
        try { return inv.invokeFunction("main");
		} catch (NoSuchMethodException | ScriptException e) { ProgramCore.GetErrorReporter().Report(script, e, ProgramClass.ScriptManager); ProgramCore.io().println("Error Code : 0x0E1101"); }
        
        return null;
	}
	
	public Object Call(Path path)
	{
		byte[] fileArray = null;
		
		try
		{
			fileArray = Files.readAllBytes(path);
		} catch (IOException e)
		{
			ProgramCore.io().println("Error Code : 0x0F1200");
			return null;
		}
		
		return Call(new String(fileArray));
	}
	
	public Object Call(File file)
	{
		if(!file.exists() && !file.isFile())
			return null;
		
		return Call(file.toPath());
	}
}
