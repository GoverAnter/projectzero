package xx.projectzero.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.TreeMap;

/**
 * Classe g�rant les fichiers <i>.snf</i> (<b>SN</b>ippet<b>F</b>ile).
 * @author Guillaume Gravetot
 * @version 1.0.0, 12/04/2016
 * @since 1.0.0, 12/04/2016
 */
public class SnippetsFilesManager
{
	/**
	 * Structure des donn�es pour le maintien des fichiers de snippets.
	 * @version 1.0.0, 12/04/2016
	 * @see SnippetsFilesManager
	 * @see SnippetFileTemplate#SnippetFileTemplate(String, TreeMap)
	 */
	public class SnippetFileTemplate
	{
		/**
		 * Constructeur par d�faut de la structure de donn�es.<br>
		 * Cr�� par commodit�.
		 * @see SnippetFileTemplate#SnippetFileTemplate(String, TreeMap)
		 */
		public SnippetFileTemplate() {}
		
		/**
		 * Constructeur complet de la structure de donn�es.
		 * @param SnippetFilePath <b>String</b> : Path complet du fichier snippet.
		 * @param SnippetTokenList <b>TreeMap(String, String)</b> : Liste des tokens contenus dans le snippet.<br><ul><li><i>Key</i> : Token</li><li><i>Value</i> : Explication plain text</li></ul>
		 */
		public SnippetFileTemplate(String SnippetFilePath, TreeMap<String, String> SnippetTokenList)
		{
			FilePath = SnippetFilePath;
			TokenList = SnippetTokenList;
		}
		
		public String FilePath;
		public TreeMap<String, String> TokenList;
	}
	
	private TreeMap<String, SnippetFileTemplate> SnippetsFiles;
	
	/**
	 * Constructeur principal de la classe.
	 */
	public SnippetsFilesManager()
	{
		SnippetsFiles = new TreeMap<String, SnippetFileTemplate>();
		loadedSnippets = new TreeMap<String, Integer>();
		
		TreeMap<String, String> tm = new TreeMap<String, String>();
		tm.put("#classname", "Nom de la classe dans laquelle la fonction etre ecrite.");
		tm.put("#members", "Liste des membres a afficher en sortie, structure : <tab * 2>s += '<tab>VarName, VarType:' + valueOf(Var) + '<newline>';<newline>");
		SnippetsFiles.put("toStringMasterClassMethod", new SnippetFileTemplate(ProgramCore.BaseSnippetsPath + "ToString.snf", tm));
		
		tm = new TreeMap<String, String>();
		tm.put("#classname", "Nom de la class dans laquelle la fonction va etre ecrite.");
		tm.put("#vars", "Liste des variables a recuperer, structure : <newline><tab * 4>VarName = rs.getType('VarName');");
		SnippetsFiles.put("loadInstanceMasterClassMethod", new SnippetFileTemplate(ProgramCore.BaseSnippetsPath + "LoadInstance.snf", tm));
		
		tm = new TreeMap<String, String>();
		tm.put("#classname", "Nom de la class dans laquelle la fonction va etre ecrite.");
		tm.put("#vars", "Liste des variables a recuperer, structure : VarName=?, VarName=?");
		tm.put("#nval", "Liste de ? du nombre de variables, structure : ?, ?, ?");
		tm.put("#setvars", "Liste des variables a enregistrer avec leur type d'insertion, structure : <newline><tab * 5>st.setType(Number, VarName); Number > 1, incremental");
		tm.put("#setupvars", "Liste des variables a update, avec leur type d'insertion, structure : <newline><tab * 5>st.setType(Number, VarName); Number > 0, incremental, last : CurrentInstanceName");
		tm.put("#insvars", "Liste des variables a inserer, structure : VarName, VarName");
		SnippetsFiles.put("saveCurrentInstanceMasterClassMethod", new SnippetFileTemplate(ProgramCore.BaseSnippetsPath + "SaveCurrentInstance.snf", tm));
		
		tm = new TreeMap<String, String>();
		tm.put("#version", "Version du fichier.");
		tm.put("#programversion", "Version du programme lors duquel a ete ecrit le fichier.");
		tm.put("#date", "Date a laquelle le fichier a ete modifie pour la derniere fois.");
		tm.put("#firstdate", "Date a laquelle le fichier a ete cree.");
		SnippetsFiles.put("ClassBeginingComment", new SnippetFileTemplate(ProgramCore.BaseSnippetsPath + "begComment.snf", tm));
		
		tm = new TreeMap<String, String>();
		tm.put("#version", "Version du fichier.");
		tm.put("#programversion", "Version du programme lors duquel a ete ecrit le fichier.");
		tm.put("#date", "Date a laquelle le fichier a ete modifie pour la derniere fois.");
		tm.put("#firstdate", "Date a laquelle le fichier a ete cree.");
		SnippetsFiles.put("InterfaceBeginingComment", new SnippetFileTemplate(ProgramCore.BaseSnippetsPath + "begCommentInterface.snf", tm));
		
		tm = new TreeMap<String, String>();
		SnippetsFiles.put("reloadInstanceMasterClassMethod", new SnippetFileTemplate(ProgramCore.BaseSnippetsPath + "ReloadInstance.snf", tm));
		
		tm = new TreeMap<String, String>();
		tm.put("#classname", "Nom de la class dans laquelle la fonction va etre ecrite.");
		tm.put("#vars", "Liste des variables a recuperer, structure : VarName=? AND VarName=?");
		tm.put("#setupvars", "Liste des variables a recuperer, structure : <newline><tab * 3>ps.setType(n, VarName);");
		SnippetsFiles.put("safeSaveCurrentInstanceMasterClassMethod", new SnippetFileTemplate(ProgramCore.BaseSnippetsPath + "SafeSaveCurrentInstance.snf", tm));
	}
	
	/**
	 * V�rifie l'existence d'un fichier snippet.
	 * @param SnippetName <b>String</b> : Nom du snippet.
	 * @return <b>boolean</b> : <b>true</b> si le snippet existe.
	 * @see SnippetsFilesManager#LoadSnippet(String)
	 */
	public boolean SnippetExists(String SnippetName)
	{
		return SnippetsFiles.containsKey(SnippetName);
	}
	
	TreeMap<String, Integer> loadedSnippets;
	
	/**
	 * Charge un fichier snippet.
	 * @param SnippetName <b>String</b> : Nom du fichier snippet.
	 * @return <b>String</b> : Snippet.
	 * @see SnippetsFilesManager#SnippetExists(String)
	 */
	public String LoadSnippet(String SnippetName)
	{
		if(!SnippetsFiles.containsKey(SnippetName))
			return "";
		
		if(!loadedSnippets.containsKey(SnippetName))
		{
			byte[] fileArray = null;
			
			try
			{
				fileArray = Files.readAllBytes(Paths.get(SnippetsFiles.get(SnippetName).FilePath));
			} catch (IOException e)
			{
				ProgramCore.io().println("Error Code : 0x091000");
				return "";
			}
			
			String s = new String(fileArray);
			loadedSnippets.put(SnippetName, ProgramCore.GetCache().CacheObject(s));
					
			return s;
		}
		else
			return ProgramCore.GetCache().Get(loadedSnippets.get(SnippetName), String.class);
	}
}