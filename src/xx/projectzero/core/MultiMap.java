package xx.projectzero.core;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import xx.projectzero.linguistic.LEnums.LWordType;
import xx.projectzero.linguistic.LEnums;
import xx.projectzero.linguistic.LWord;

public class MultiMap implements Serializable
{
	private static final long serialVersionUID = 1L;
	private volatile String[] MRow;
	private volatile LWordType[] MRow2;
	private volatile LWord[] Values;
	
	private volatile int lastIndex;
	
	private volatile int initialcapacity;
	
	public MultiMap()
	{
		MRow = new String[10];
		MRow2 = new LWordType[10];
		Values = new LWord[10];
		
		lastIndex = 0;
	}
	
	public MultiMap(int initialCapacity)
	{
		initialcapacity = initialCapacity;
		
		clear();
	}
	
	private void clear()
	{
		MRow = new String[initialcapacity];
		MRow2 = new LWordType[initialcapacity];
		Values = new LWord[initialcapacity];
		
		lastIndex = 0;
	}
	
	/** Save the current instance of this class in the specified table of the program database. */
	public synchronized void SaveToDB(String TableName)
	{
		Connection c = Utils.PrepareConnection();
		ResultSet rs = null;
		
		/*
		 * workflow :
		 * suppression de la table indiqu�e si existante
		 * cr�ation d'une nouvelle table avec structure appropri�e avec le nom sp�cifi�
		 * remplissage de la table avec les donn�es de l'instance de cet objet
		 */
		
		//Suppression de la table
		try
		{
			rs = c.createStatement().executeQuery("DROP TABLE "+TableName);
		} catch(SQLException e)
		{
			
		}
		
		try
		{
			rs.close();
			rs = c.createStatement().executeQuery("CREATE TABLE " + TableName + " (id INT NOT NULL, literal VARCHAR(255), wordtype INT, wordobject MEDIUMTEXT, PRIMARY KEY (id))");
		} catch (SQLException e)
		{
			
		}
		
		for(int i=0; i < MRow.length; i++)
		{
			try
			{
				PreparedStatement s = c.prepareStatement("INSERT INTO ?(id, literal, wordtype, wordobject) VALUES (?,?,?,?)");
				s.setString(1, TableName);
				s.setInt(2, i);
				s.setString(3, MRow[i]);
				s.setInt(4, MRow2[i].ordinal());
				s.setString(5, Utils.Serialize(Values[i]));
				s.executeQuery();
			} catch (SQLException e)
			{
				
			}
		}
		
		try
		{
			rs.close();
			c.close();
		} catch (SQLException e)
		{
			
		}
	}
	
	/** Load the specified table into this instance from the program database. */
	public synchronized void LoadFromDB(String TableName)
	{
		Connection c = Utils.PrepareConnection();
		ResultSet rs = null;
		
		/*
		 * workflow :
		 * chargement complet de la table sp�cifi�e dans la base de donn�es du programme
		 * si la table n'existe pas, r�initialiser l'instance, no data
		 * si la table existe, effectuer le chargement des donn�es dans une instance vierge
		 */
		
		try
		{
			rs = c.createStatement().executeQuery("SELECT * FROM "+TableName);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		if(lastIndex != 0)
			clear();
		
		if(rs == null)
			return;
		else
		{
			try
			{
				while(rs.next())
				{
					int id = rs.getInt("id");
					
					MRow[id] = rs.getString("literal");
					MRow2[id] = LEnums.LWordType.values()[rs.getInt("wordtype")];
					Values[id] = (LWord)Utils.Deserialize(rs.getString("wordobject"));
				}
			} catch (SQLException e)
			{
				
			}
		}
	}
	
	private synchronized void ExpendInternalArray()
	{
		String[] b1 = new String[Math.round(MRow.length*1.2f)];
		LWordType[] b2 = new LWordType[Math.round(MRow.length*1.2f)];
		LWord[] b3 = new LWord[Math.round(MRow.length*1.2f)];
		
		for(int ai = 0; ai < 3; ai++)
		{
			for(int i = 0; i < MRow.length; i++)
			{
				switch(ai)
				{
					case 0:
						b1[i] = MRow[i];
						break;
					case 1:
						b2[i] = MRow2[i];
						break;
					case 2:
						b3[i] = Values[i];
						break;
				}
			}
		}
		
		MRow = b1;
		MRow2 = b2;
		Values = b3;
		
		System.gc();
	}
	
	public synchronized int put(String mRow, LWordType mRow2, LWord value)
	{
		if(lastIndex == MRow.length)
			ExpendInternalArray();
		
		MRow[lastIndex] = mRow;
		MRow2[lastIndex] = mRow2;
		Values[lastIndex] = value;
		
		return lastIndex++;
	}
	
	public synchronized LWord get(int index)
	{
		if(index >= Values.length)
			return null;
		
		return Values[index];
	}
	
	public synchronized LWord get(String mRow, LWordType mRow2)
	{
		for(int i = 0; i < lastIndex; i++)
		{
			if(LWord.equals(MRow[i], mRow))
			{
				if(MRow2[i].equals(mRow2))
					return Values[i];
			}
		}
		
		return null;
	}
	
	public synchronized boolean contains(String mRow, LWordType mRow2)
	{
		for(int i = 0; i < lastIndex; i++)
		{
			if(LWord.equals(MRow[i], mRow))
			{
				if(MRow2[i].equals(mRow2))
					return true;
			}
		}
		
		return false;
	}
	
	public synchronized int indexOf(String mRow, LWordType mRow2)
	{
		for(int i = 0; i < lastIndex; i++)
		{
			if(LWord.equals(MRow[i], mRow))
			{
				if(MRow2[i].equals(mRow2))
					return i;
			}
		}
		
		return -1;
	}
	
	public synchronized int size()
	{
		return lastIndex;
	}
	
	public synchronized ArrayList<LWord> getAll(LWordType type)
	{
		ArrayList<LWord> mwords = new ArrayList<LWord>();

		for(int i = 0; i < lastIndex; i++)
		{
			if(MRow2[i].equals(type))
			{
				mwords.add(Values[i]);
			}
		}
		
		return mwords;
	}
}
