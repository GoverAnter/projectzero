package xx.projectzero.core;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;

import xx.projectzero.core.Utils.ProgramClass;
import xx.projectzero.sourcewriter.ClassWriter.ClassTemplate;

public class ErrorReporter
{
	public static class ClassWriterReportObject
	{
		public ClassTemplate ct;
		public boolean isInterface;
		public int writerstate; //1: writing, 2: writing to file, 3: compliling, 4: moving compiled file, 5: SQL stuff(only on classes)
		public Path pathToSource;
		
		public ClassWriterReportObject(ClassTemplate CT, boolean IsInterface, int WriterState, Path PathToSource)
		{
			ct = CT;
			isInterface = IsInterface;
			writerstate = WriterState;
			pathToSource = PathToSource;
		}
	}
	
	public ErrorReporter()
	{
		initDir();
	}
	
	public synchronized void initDir()
	{
		StringBuilder filename = new StringBuilder();
		filename.append(ProgramCore.BaseReportPath);
		filename.append(String.valueOf(LocalDate.now().get(ChronoField.DAY_OF_MONTH)).length()==1?"0"+String.valueOf(LocalDate.now().get(ChronoField.DAY_OF_MONTH)):LocalDate.now().get(ChronoField.DAY_OF_MONTH));
		filename.append("-");
		filename.append(String.valueOf(LocalDate.now().get(ChronoField.MONTH_OF_YEAR)).length()==1?"0"+String.valueOf(LocalDate.now().get(ChronoField.MONTH_OF_YEAR)):String.valueOf(LocalDate.now().get(ChronoField.MONTH_OF_YEAR)));
		filename.append("-");
		filename.append(LocalDate.now().get(ChronoField.YEAR));
		filename.append("/");
		
		dir = filename.toString();
		
		File direct = new File(dir);
		
		if(!direct.exists())
		{
			direct.mkdir();
			
			direct = new File(dir+"ScriptManager/");
			if(!direct.exists())
				direct.mkdir();
			
			direct = new File(dir+"ClassWriter/");
			if(!direct.exists())
				direct.mkdir();
		}
	}
	
	private String dir;
	
	private void WriteFile(String content, String fpath)
	{
		try
		{
			Files.write(Paths.get(fpath), content.getBytes());
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public synchronized void Report(Object o, Exception e, ProgramClass c)
	{
		switch(c)
		{
			case ScriptManager:
				reportSM((String)o, e);
				break;
			case ClassWriter:
				reportCW((ClassWriterReportObject)o, e);
			default:
				break;
		}
	}
	
	private void reportCW(ClassWriterReportObject o, Exception e)
	{
		String fdir = dir + "ClassWriter/" + (String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)):LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)) + "h" + (String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)):LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)) + "/";
		
		File f = new File(fdir);
		if(!f.exists())
			f.mkdir();
		
		String fpath = fdir + Utils.GetClassNameFromCompleteName(o.ct.CompleteName) + ".report";
		String spath = fdir + Utils.GetClassNameFromCompleteName(o.ct.CompleteName) + ".java";
		
		StringBuilder content = new StringBuilder();
		
		content.append("-------------------------------- Report generated ");
		content.append(String.valueOf(LocalDate.now().get(ChronoField.DAY_OF_MONTH)).length()==1?"0"+String.valueOf(LocalDate.now().get(ChronoField.DAY_OF_MONTH)):LocalDate.now().get(ChronoField.DAY_OF_MONTH));
		content.append("/");
		content.append(String.valueOf(LocalDate.now().get(ChronoField.MONTH_OF_YEAR)).length()==1?"0"+String.valueOf(LocalDate.now().get(ChronoField.MONTH_OF_YEAR)):LocalDate.now().get(ChronoField.MONTH_OF_YEAR));
		content.append("/"); content.append(LocalDate.now().get(ChronoField.YEAR)); content.append(" at ");
		content.append(String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)):LocalDateTime.now().get(ChronoField.HOUR_OF_DAY));
		content.append("h");
		content.append(String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)):LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR));
		content.append(" by ErrorReporter for "); content.append(o.isInterface?"InterfaceWriterWorker":"ClassWriterWorker"); content.append(" --------------------------------\n\nError : \"");
		
		switch(o.writerstate)
		{
			case 2:
				content.append("Error while creating source file :\n\n\n");
				content.append(e.toString());
				content.append(" :\n");
				for(StackTraceElement el : e.getStackTrace())
				{
					content.append("\n"); content.append(el.toString());
				}
				content.append("\n\n\nHere is the serialized ClassTemplate object, as stored in cache with address "); content.append(Integer.toHexString(ProgramCore.GetCache().CacheObject(o.ct)).toUpperCase()); content.append("\n");
				content.append(Utils.Serialize(o.ct));
				break;
			case 3:
				content.append("Error while compiling file.\"\nFile should be in the same directory as the present report.");
				content.append("\n\n\nHere is the serialized ClassTemplate object, as stored in cache with address "); content.append(Integer.toHexString(ProgramCore.GetCache().CacheObject(o.ct)).toUpperCase()); content.append("\n");
				content.append(Utils.Serialize(o.ct));
				try { Files.move(o.pathToSource, Paths.get(spath)); } catch (IOException e1) { e1.printStackTrace(); }
				break;
			case 4:
				content.append("Error while moving compiled file to its final destination :\n(source file should be in the same directory as the report)\n\n\n");
				content.append(e.toString());
				content.append(" :\n");
				for(StackTraceElement el : e.getStackTrace())
				{
					content.append("\n"); content.append(el.toString());
				}
				try { Files.move(o.pathToSource, Paths.get(spath)); } catch (IOException e1) { e1.printStackTrace(); }
				break;
			default:
				break;
		}
		
		WriteFile(content.toString(), fpath);
	}
	
	private void reportSM(String script, Exception e)
	{
		String fpath = dir + "ScriptManager/" + (String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)):LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)) + "h" + (String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)):LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)) + ".report";
	
		StringBuilder content = new StringBuilder();
		
		content.append("-------------------------------- Report generated ");
		content.append(String.valueOf(LocalDate.now().get(ChronoField.DAY_OF_MONTH)).length()==1?"0"+String.valueOf(LocalDate.now().get(ChronoField.DAY_OF_MONTH)):LocalDate.now().get(ChronoField.DAY_OF_MONTH));
		content.append("/");
		content.append(String.valueOf(LocalDate.now().get(ChronoField.MONTH_OF_YEAR)).length()==1?"0"+String.valueOf(LocalDate.now().get(ChronoField.MONTH_OF_YEAR)):LocalDate.now().get(ChronoField.MONTH_OF_YEAR));
		content.append("/"); content.append(LocalDate.now().get(ChronoField.YEAR)); content.append(" at ");
		content.append(String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)):LocalDateTime.now().get(ChronoField.HOUR_OF_DAY));
		content.append("h");
		content.append(String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)):LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR));
		content.append(" by ErrorReporter for ScriptManager --------------------------------\n\nError : \"");
		content.append(e.toString()); content.append("\" in script :\n\nCode : JAVASCRIPT -----------------------------------------------------------------------------------------------------------------------\n\n");
		content.append(script); content.append("\n\n-----------------------------------------------------------------------------------------------------------------------------------------\n\n\nDetailed StackTrace :\n\n\n");

		content.append(e.toString()); content.append(" :\n\n");
		for(StackTraceElement el : e.getStackTrace())
		{
			content.append(el.toString()); content.append("\n");
		}
		
		WriteFile(content.toString(), fpath);
	}
}
