package xx.projectzero.core;

import java.util.ArrayList;

import xx.projectzero.console.Command;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.ui.MainViewController.MVTabs;

public class Cache
{
	private volatile Object[] cache;
	private volatile int LastAddress;
	private volatile ArrayList<Integer> availableAddresses;
	
	/** Initial capacity of the internal array */
	private final int initialSize = 100;
	/** Expansion rule of the internal array.<br>If the array is full, the array will get a new size of <i>expansionRule x size</i>. */
	private volatile float expansionRule = 1.5f;
	
	public Cache()
	{
		cache = new Object[initialSize];
		availableAddresses = new ArrayList<Integer>();
		LastAddress = 0x000000;
		
		try
		{
			ProgramCore.GetLogger().log(new LogObject("Scheduling timer for resize every 20s", "Cache", LogType.Action, Priority.Medium));
			ProgramCore.GetEventManager().NewInfiniteShotTimer(20000, this, this.getClass().getMethod("Resize"));
		} catch (NoSuchMethodException | SecurityException e)
		{
			ProgramCore.io().println("Error Code : 0x0C1000");
			ProgramCore.GetLogger().log(new LogObject("Cannot register resize timer", "Cache", LogType.Error, Priority.High));
		}
		
		ProgramCore.GetLogger().log(new LogObject("Adding command \"cache\" to console", "Cache", LogType.Action, Priority.Low));
		ProgramCore.GetConsole().AddCommand(new Command("cache") {
			@Override
			public void Exec(String[] params)
			{
				if(params.length == 0)
					PrintHelp();
				else if(Utils.ArrayContains(params,"-h") || Utils.ArrayContains(params,"--help"))
					PrintHelp();
				else if(Utils.ArrayContains(params,"-s") || Utils.ArrayContains(params,"--size"))
					ProgramCore.io().printlntotab("Cache size is " + cache.length, MVTabs.Console);
				else if(Utils.ArrayContains(params,"-u") || Utils.ArrayContains(params,"--utilisation"))
					ProgramCore.io().printlntotab("The cache is currently used at " + (LastAddress*1.0f/cache.length)*100 + "% of its capacity", MVTabs.Console);
				else if(Utils.ArrayContains(params,"-e") || Utils.ArrayContains(params,"--expansion"))
				{
					if(Utils.ArrayContains(params,"-v") || Utils.ArrayContains(params,"--verbose"))
						ProgramCore.io().printlntotab("Expansion rule is " + expansionRule + "\nNext size will be " + (int)(expansionRule*cache.length), MVTabs.Console);
					else if(Utils.ArrayContains(params,"-f") || Utils.ArrayContains(params,"--force"))
					{
						Expand();
						ProgramCore.io().printlntotab("Forcing expansion of the cache\nNew size is " + cache.length, MVTabs.Console);
					}
					else if(Utils.ArrayContains(params,"-r") || Utils.ArrayContains(params,"--resize"))
					{
						Resize();
						ProgramCore.io().printlntotab("Forcing resize of the cache\nNew size is " + cache.length, MVTabs.Console);
					}
					else
					{
						int ind = Utils.GetIndex(params, "-e");
						
						if(ind == -1)
						{
							ind = Utils.GetIndex(params, "--expansion");
							
							if(ind == -1)
							{
								ProgramCore.io().printlntotab("Unknown option", MVTabs.Console);
								return;
							}
						}
						
						if(ind +1 == params.length)
						{
							PrintHelp();
							return;
						}
						
						if(Utils.TryParse(params[ind+1]).getClass().getName() == "String")
						{
							ProgramCore.io().printlntotab("Error while parsing argument for option \"Expansion\"", MVTabs.Console);
							return;
						}
						
						float lastRule = expansionRule;
						expansionRule = Float.parseFloat(params[ind+1]);
						
						if(expansionRule <= 1)
						{
							expansionRule = lastRule;
							ProgramCore.io().printlntotab("Expansion rule must be greater than 1", MVTabs.Console);
							return;
						}
						
						ProgramCore.io().printlntotab("Expansion rule changed to " + expansionRule + "\nNext size will be " + (int)(expansionRule*cache.length), MVTabs.Console);
					}
				}
			}
			
			private void PrintHelp()
			{
				ProgramCore.io().printlntotab("Help text for cache command.\n\ncache [-options]\n\nOptions :\n\t-h, --help : Display this help. Ignores other options.\n\t-s, --size : Display the current size of the cache. Ignores other options.\n\t-u, --utilisation : Display the current utilisation of the cache. Ignores other options.\n\t-e, --expansion :\n\t\tUse with -v or --verbose to get the expansion rule\n\t\tUse with -f or --force to force a new expansion\n\t\tUse with a number as argument to assign a new expansion rule.\n", MVTabs.Console);
			}
		});
	}
	
	private synchronized void Expand()
	{
		ProgramCore.GetLogger().log(new LogObject("Expanding size of cache from " + cache.length + " to " + (int)(expansionRule*cache.length) + ", making it used by " + (LastAddress/expansionRule*cache.length)*100 + "%", "Cache", LogType.Action, Priority.Medium));
		Object[] narr = new Object[(int)(expansionRule*cache.length)];
		
		for(int i = 0; i < cache.length; i++)
			narr[i] = cache[i];
		
		cache = narr;
		
		System.gc();
	}
	
	/** Method only called by timer.\nCall it if you want to resize the cache to a more appropriate size. */
	public synchronized void Resize()
	{
		ProgramCore.GetLogger().log(new LogObject("Resizing cache", "Cache", LogType.Action, Priority.Low));
		
		int olsize = cache.length;
		
		int remainingc = availableAddresses.size() + (cache.length - LastAddress);
		int remainingp = (int)((1.0f*remainingc/cache.length)*100);

		if(remainingp < 20)
			Expand();
		else if(((cache.length-LastAddress)*1.0f/cache.length)*100 > 60)
		{
			Object[] narr = new Object[((int)(LastAddress*1.2f)>10)?(int)(LastAddress*1.2f):10];
			
			for(int i = 0; i < narr.length; i++)
				narr[i] = cache[i];
			
			cache = narr;
			
			ProgramCore.GetLogger().log(new LogObject("Resize done, resized from " + olsize + " to " + cache.length, "Cache", LogType.Info, Priority.Low));
		}
		else
			ProgramCore.GetLogger().log(new LogObject("No resize needed", "Cache", LogType.Info, Priority.Low));
	}
	
	private synchronized boolean needExpansion(int number)
	{
		if(number+LastAddress > cache.length)
			return true;
		else
			return false;
	}
	
	/** Caches the specified object and returns its address. */
	public synchronized int CacheObject(Object o)
	{
		if(availableAddresses.size() == 0)
		{
			if(needExpansion(1))
				Expand();
			
			cache[LastAddress] = o;
			return LastAddress++;
		}
		else
		{
			int cAddr = availableAddresses.get(0);
			availableAddresses.remove(0);
			
			cache[cAddr] = o;
			return cAddr;
		}
	}
	
	/** Frees the memory at the specified address. */
	public synchronized void Free(int Address)
	{
		cache[Address] = null;
		availableAddresses.add(new Integer(Address));
	}
	
	/** Get the object at the specified address. */
	public synchronized Object GetObject(int Address)
	{
		return cache[Address];
	}
	
	/** Get the object at the specified address. */
	public synchronized <T> T Get(int Address, Class<T> c)
	{
		return c.cast(cache[Address]);
	}
}
