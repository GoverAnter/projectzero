package xx.projectzero.core;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.TreeMap;

import javafx.application.Platform;
import xx.projectzero.annotations.JSCallable;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.ui.MainViewController.MVTabs;

/**
 * Class managing events.
 * @author Guillaume Gravetot
 * @version 1.0.0, 28/04/2016
 * @since 1.1.1, 28/04/2016
 */
public class EventManager
{
	private class EventObject
	{
		String objectName;
		String methodName;
		
		Object object;
		Method method;
		
		public EventObject(Object o, Method m)
		{
			object = o;
			method = m;
			objectName = o.getClass().getSimpleName();
			methodName = m.getName();
		}
		
		public synchronized void call() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
		{
			if(object != null)
				method.invoke(object);
		}
	}
	
	private abstract class TimerObject implements Runnable
	{
		protected Thread t;
		protected int index;
		protected int time;
		protected EventObject eo;
		
		public TimerObject(int Time, Object object, Method method)
		{
			time = Time;
			eo = new EventObject(object, method);
		}
		
		/** Method to call to start a new TimerEventObject. */
		public void start(int Index)
		{
			if(t == null)
			{
				index = Index;
				String name = "TimerObjectThread"+Index;
				t = new Thread(this, name);
				t.setDaemon(true);
				t.start();
			}
		}
	}
	
	private class OneShotTimer extends TimerObject
	{
		/** Main constructor of this object. */
		public OneShotTimer(int Time, Object object, Method method)
		{
			super(Time, object, method);
		}
		
		/** Dont call this.<br>Call start instead. 
		 * @see TimerObject#start(int)*/
		@Override
		public void run()
		{
			//waiting for the specified time
			try { Thread.sleep(new Long(time).longValue()); } catch (InterruptedException e) {}
			//calling back the object
			try { eo.call(); } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
			{
				ProgramCore.io().println("Error Code : 0x061200");
				ProgramCore.GetLogger().log(new LogObject("Error while calling back method " + eo.methodName + " on object " + eo.objectName + " in oneshot timer " + index, "Event Manager", LogType.Error, Priority.High));
			}
		}
	}
	
	private class MultiShotTimer extends TimerObject
	{
		private int shotnumber;
		
		/** Main constructor of this object. */
		public MultiShotTimer(int Time, int shotNumber, Object object, Method method)
		{
			super(Time, object, method);
			shotnumber = shotNumber;
		}
		
		/** Dont call this.<br>Call start instead. 
		 * @see TimerObject#start(int)*/
		@Override
		public void run()
		{
			do {
				//waiting for the specified time
				try { Thread.sleep(new Long(time).longValue()); } catch (InterruptedException e) {}
				//calling back the object
				try { eo.call(); } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
				{
					ProgramCore.io().println("Error Code : 0x061300");
					ProgramCore.GetLogger().log(new LogObject("Error while calling back method " + eo.methodName + " on object " + eo.objectName + " in multishot timer " + index, "Event Manager", LogType.Error, Priority.High));
				}
				shotnumber--;
			} while(shotnumber >= 1);
			
			DestroyTimer(index);
		}
	}
	
	private class InfiniteShotTimer extends TimerObject
	{
		public volatile boolean stop = false;
		
		/** Main constructor of this object. */
		public InfiniteShotTimer(int Time, Object object, Method method)
		{
			super(Time, object, method);
		}
		
		/** Dont call this.<br>Call start instead. 
		 * @see TimerObject#start(int)*/
		@Override
		public void run()
		{
			do {
				//waiting for the specified time
				try { Thread.sleep(new Long(time).longValue()); } catch (InterruptedException e) {}
				//calling back the object
				try { eo.call(); } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
				{
					ProgramCore.io().println("Error Code : 0x061400");
					ProgramCore.GetLogger().log(new LogObject("Error while calling back method " + eo.methodName + " on object " + eo.objectName + " in infiniteshot timer " + index, "Event Manager", LogType.Error, Priority.High));
				}
			} while(!stop);
			
			DestroyTimer(index);
		}
	}
	
	/** List of registered events. */
	private volatile TreeMap<String, ArrayList<EventObject>> regEvents;
	/** List of active timers. */
	private volatile TreeMap<Integer, TimerObject> regTimers;
	
	/** Main constructor of this class. */
	public EventManager()
	{
		ProgramCore.io().printlntotab("EventManager initialization", MVTabs.EventManager);
		ProgramCore.GetLogger().log(new LogObject("Initialization", "Event Manager", LogType.Info, Priority.Medium));
		regEvents = new TreeMap<String, ArrayList<EventObject>>();
		regTimers = new TreeMap<Integer, TimerObject>();
		
		try
		{
			ProgramCore.io().printlntotab("Registering Exit-related event", MVTabs.EventManager);
			Register("Exiting", this, this.getClass().getMethod("Exiting"));
		} catch(NoSuchMethodException | SecurityException e)
		{
			ProgramCore.io().println("Error Code : 0x061100");
			ProgramCore.io().printlntotab("Error Code : 0x061100", MVTabs.EventManager);
		}
	}
	
	/** Fire the specified event to all registered objects. */
	@JSCallable
	public synchronized void Fire(String event)
	{
		if(!Created(event))
			return;
		
		ProgramCore.io().printlntotab("Firing event " + event, MVTabs.EventManager);
		ProgramCore.GetLogger().log(new LogObject("Firing event " + event, "Event Manager", LogType.Action, Priority.Medium));
		
		for(EventObject eo : regEvents.get(event))
		{
			try
			{
				eo.call();
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
			{
				ProgramCore.io().println("Error Code : 0x061000");
			}
		}
	}
	
	/** Create an event with the specified name. */
	@JSCallable
	public synchronized void Create(String event)
	{
		ProgramCore.io().printlntotab("Creating event event " + event, MVTabs.EventManager);
		
		if(event == null || event.length() == 0 || Created(event))
			return;
		
		regEvents.put(event, new ArrayList<EventObject>());
	}
	
	/** Returns <b>true</b> if the event with the specified name is created, <b>false</b> otherwise. */
	@JSCallable
	public synchronized boolean Created(String event)
	{
		if(event == null || event.length() == 0)
			return false;
		
		return regEvents.containsKey(event);
	}
	
	/** Clear the list of registered objects of this event. */
	@JSCallable
	public synchronized void Clear(String event)
	{
		ProgramCore.io().printlntotab("Clearing event " + event, MVTabs.EventManager);
		
		if(!Created(event))
			return;
		
		regEvents.put(event, new ArrayList<EventObject>());
	}
	
	/** Destroy the specified event. */
	@JSCallable
	public synchronized void Destroy(String event)
	{
		ProgramCore.io().printlntotab("Destroying event type " + event, MVTabs.EventManager);
		
		if( !Created(event))
			return;
		
		regEvents.remove(event);
	}
	
	/** Returns <b>true</b> if the specified entry is registered. */
	public synchronized boolean Exists(String event, Object o, String MethodName)
	{
		if(o == null || event == null || event.length() == 0 || MethodName == null || MethodName.length() == 0 || !regEvents.containsKey(event))
			return false;
		
		ArrayList<EventObject> eo = regEvents.get(event);
		
		if(eo == null || eo.isEmpty())
			return false;
		
		for(EventObject oe : eo)
		{
			if(oe.objectName.equals(o.getClass().getSimpleName()) && oe.methodName.equals(MethodName))
			{
				if(o.hashCode() == oe.object.hashCode())
					return true;
			}
		}
		
		return false;
	}
	
	/** Register a method call to the specified event. */
	public synchronized void Register(String event, Object o, Method m)
	{
		ProgramCore.io().printlntotab("\nRegistering event " + event + " from object " + o.getClass().getSimpleName(), MVTabs.EventManager);
		ProgramCore.GetLogger().log(new LogObject("Registering event " + event + " from object " + o.getClass().getSimpleName(), "Event Manager", LogType.Action, Priority.Low));
		
		if(o == null || event == null || event.length() == 0 || m == null || Exists(event, o, m.getName()))
			return;
		
		EventObject eo = new EventObject(o, m);
		
		if(!Created(event))
			Create(event);
		
		regEvents.get(event).add(eo);
		
		ProgramCore.io().printlntotab("Event " + event + " registered", MVTabs.EventManager);
	}
	
	/** Remove the specified entry associated to this event. */
	public synchronized void Remove(String event, Object o, String MethodName)
	{
		ProgramCore.io().printlntotab("\nRemoving event " + event + " from object " + o.getClass().getSimpleName(), MVTabs.EventManager);
		
		if(o == null || event == null || event.length() == 0 || MethodName == null || MethodName.length() == 0 || Created(event) || !Exists(event, o, MethodName))
			return;
		
		ArrayList<EventObject> eo = regEvents.get(event);
		
		for(EventObject oe : eo)
		{
			if(oe.objectName.equals(o.getClass().getSimpleName()) && oe.methodName.equals(MethodName))
			{
				if(o.hashCode() == oe.object.hashCode())
				{
					regEvents.get(event).remove(oe);
					break;
				}
			}
		}
		
		ProgramCore.io().printlntotab("Event " + event + " removed", MVTabs.EventManager);
	}
	
	//REGION Timers
	private volatile int lastIndex = 0;
	
	/** Creates a one-time timer that calls method <i>m</i> on object <i>o</i> after <i>time</i> ms. */
	public synchronized void NewOneShotTimer(int time, Object o, Method m)
	{
		ProgramCore.GetLogger().log(new LogObject("Creating " + time + "ms oneshot timer for object " + o.getClass().getSimpleName() + ", calling method " + m.getName(), "Event Manager", LogType.Action, Priority.Medium));
		
		new OneShotTimer(time, o, m).start(-1);
	}
	
	/** Creates a timer that calls <i>nbshots</i> times method <i>m</i> on object <i>o</i> every <i>time</i> ms. */
	public synchronized int NewMultiShotTimer(int time, int nbshots, Object o, Method m)
	{
		ProgramCore.GetLogger().log(new LogObject("Creating " + time + "ms " + nbshots + " shots timer for object " + o.getClass().getSimpleName() + ", calling method " + m.getName(), "Event Manager", LogType.Action, Priority.Medium));
		
		regTimers.put(new Integer(lastIndex), new MultiShotTimer(time, nbshots, o, m));
		regTimers.get(new Integer(lastIndex)).start(lastIndex);
		return lastIndex++;
	}
	
	/** Creates a timer that calls method <i>m</i> on object <i>o</i> every <i>time</i> ms until <i>top</i> is set to <b>true</b>. */
	public synchronized int NewInfiniteShotTimer(int time, Object o, Method m)
	{
		ProgramCore.GetLogger().log(new LogObject("Creating " + time + "ms infinite shots timer for object " + o.getClass().getSimpleName() + ", calling method " + m.getName(), "Event Manager", LogType.Action, Priority.Medium));
		
		regTimers.put(new Integer(lastIndex), new InfiniteShotTimer(time, o, m));
		regTimers.get(new Integer(lastIndex)).start(lastIndex);
		return lastIndex++;
	}
	
	/** Method to destroy a timer, implicitly used by timer object. */
	private synchronized void DestroyTimer(int index)
	{
		regTimers.remove(new Integer(index));
		
		if(regTimers.isEmpty())
			lastIndex = 0;
	}
	//ENDREGION
	
	//REGION ProgramExit	
	private volatile int ex = 0;
	public synchronized void Exiting()
	{
		ex++;
		
		if(ex == regEvents.get("Exit").size())
		{
			if(!ProgramCore.exiting)
			{
				ProgramCore.exiting = true;
				Platform.exit();
			}
			else
				ProgramCore.readytoexit = true;
		}
	}
	//ENDREGION
}
