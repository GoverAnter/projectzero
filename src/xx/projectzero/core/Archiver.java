package xx.projectzero.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Archiver
{
	public static enum Type
	{
		ReloaderClassList,
		ParamSavedFile
	}
	
	private static final class paths
	{
		public static final String ReloaderPath = ProgramCore.BasePath + "classes.xml";
		public static final String ReloaderAPath = ProgramCore.BaseArchivePath + "classes.xml";
		public static final String ReloaderIPath = ProgramCore.BaseArchivePath + "ReloaderClassList/classes%.xml";
		
		public static final String ParamSavedFilePath = ProgramCore.BasePath + "params.sp";
		public static final String ParamSavedFileAPath = ProgramCore.BaseArchivePath + "params.sp";
		public static final String ParamSavedFileIPath = ProgramCore.BaseArchivePath + "ParamsFiles/params%.xml";
	}
	
	/** Returns the path of the specified type.<br>Set archive to <b>true</b> to get archive path. */
	private Path GetPath(Type type, boolean archive)
	{
		switch(type)
		{
			case ReloaderClassList:
				if(archive)
					return Paths.get(paths.ReloaderAPath);
				else
					return Paths.get(paths.ReloaderPath);
			case ParamSavedFile:
				if(archive)
					return Paths.get(paths.ParamSavedFileAPath);
				else
					return Paths.get(paths.ParamSavedFilePath);
			default:
				return Paths.get("C:");
		}
	}
	
	/** Returns the path of the specified type, with <i>iteration</i> at the end of the name. */
	private Path GetPath(Type type, int iteration)
	{
		switch(type)
		{
			case ReloaderClassList:
				return Paths.get(paths.ReloaderIPath.replace("%", String.valueOf(iteration)));
			case ParamSavedFile:
				return Paths.get(paths.ParamSavedFileIPath.replace("%", String.valueOf(iteration)));
			default:
				return Paths.get("C:");
		}
	}
	
	/** Returns the hash of the specified type file.<br>Set <i>archive</i> to <b>true</b> to get the hash of the archive file. */
	public String GetHash(Type type, boolean archive)
	{
		if(!Files.exists(GetPath(type, archive), LinkOption.NOFOLLOW_LINKS))
			return "";
		
		MessageDigest md = null;
		try { md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) { ProgramCore.io().println("Error Code : 0x0D1000"); return ""; }
		
		try { md.update(Files.readAllBytes(GetPath(type, archive)));
		} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D0001"); return ""; }
		
		return Arrays.toString(md.digest());
	}
	
	/** Returns the hash of the specified archive iteration type file. */
	public String GetHash(Type type, int iteration)
	{
		if(!Files.exists(GetPath(type, iteration), LinkOption.NOFOLLOW_LINKS))
			return "";
		
		MessageDigest md = null;
		try { md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) { ProgramCore.io().println("Error Code : 0x0D1100"); return ""; }

		try { md.update(Files.readAllBytes(GetPath(type, iteration)));
		} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D0101"); return ""; }
		
		return Arrays.toString(md.digest());
	}
	
	/** Compares hashes between archive and original file. */
	public boolean CompareHashes(Type type)
	{
		return GetHash(type, true).equals(GetHash(type, false));
	}
	
	/** Compares hashes between archive iteration and original file. */
	public boolean CompareHashes(Type type, int iteration)
	{
		return GetHash(type, iteration).equals(GetHash(type, false));
	}
	
	/** Compares hashes between archive iteration and archive file. */
	public boolean CompareAHashes(Type type, int iteration)
	{
		return GetHash(type, iteration).equals(GetHash(type, true));
	}
	
	/** Compares hashes between archive iteration and original file, returning the iteration number, or 0 if none. */
	public int CompareAllHashes(Type type)
	{
		for(int i = 1; i < 4; i++)
		{
			if(CompareHashes(type, i))
				return i;
		}
		
		return 0;
	}
	
	/** Returns <b>true</b> if the specified type has an archive. */
	public boolean ArchiveExists(Type type)
	{
		return Files.exists(GetPath(type, true));
	}
	
	/** Returns <b>true</b> if the specified type has the specified archive iteration. */
	public boolean ArchiveExists(Type type, int iteration)
	{
		return Files.exists(GetPath(type, iteration));
	}
	
	/** Returns <b>true</b> if the specified type has an archive or an archive iteration. */
	public boolean AnyArchiveExists(Type type)
	{
		return Files.exists(GetPath(type, 1)) || Files.exists(GetPath(type, 2)) || Files.exists(GetPath(type, 3)) || Files.exists(GetPath(type, true));
	}
	
	/** Makes a room in the backups : backup2 become backup3 and backup1 become backup2. */
	private void IterateBackup(Type type)
	{
		if(Files.exists(GetPath(type, 2)))
		{
			try { Files.move(GetPath(type, 2), GetPath(type, 3), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1200"); }
		}
		
		if(Files.exists(GetPath(type, 1)))
		{
			try { Files.move(GetPath(type, 1), GetPath(type, 2), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1201"); }
		}
	}
	
	/** Archive the provided type.
	 * @param type <b>Archiver.Type</b> : Type of file to archive.
	 * @param overwrite <b>boolean</b> : If file already exists, overwrite it.
	 * @param store <b>boolean</b> : If file already created, rename old file. Ignores param <i>overwrite</i>.
	 */
	public void archive(Type type, boolean overwrite, boolean store)
	{
		if(Files.exists(GetPath(type, false), LinkOption.NOFOLLOW_LINKS))
		{
			if(Files.exists(GetPath(type, true), LinkOption.NOFOLLOW_LINKS))
			{
				if(!CompareHashes(type))
				{
					if(store)
					{
						int iter = CompareAllHashes(type);
						
						if(iter != 0)
						{
							for(int i = 1; i < iter; i++)
							{
								try { Files.move(GetPath(type, iter), GetPath(type, iter+1), StandardCopyOption.REPLACE_EXISTING);
								} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1300"); }
							}
							
							try { Files.move(GetPath(type, true), GetPath(type, 1), StandardCopyOption.REPLACE_EXISTING);
							} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1300"); }
							
							try { Files.copy(GetPath(type, false), GetPath(type, true), StandardCopyOption.REPLACE_EXISTING);
							} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1301"); }
						}
						else
						{
							IterateBackup(type);
							
							try { Files.move(GetPath(type, true), GetPath(type, 1), StandardCopyOption.REPLACE_EXISTING);
							} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1300"); }
							
							try { Files.copy(GetPath(type, false), GetPath(type, true), StandardCopyOption.REPLACE_EXISTING);
							} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1301"); }
						}
					}
					else if(overwrite)
					{
						try { Files.copy(GetPath(type, false), GetPath(type, true), StandardCopyOption.REPLACE_EXISTING);
						} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1302"); }
					}
				}
			}
			else
			{
				try { Files.copy(GetPath(type, false), GetPath(type, true));
				} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1303"); }
			}
		}
	}
	
	/** Restore the archive of the specified type. */
	public boolean RestoreArchive(Type type)
	{
		if(!ArchiveExists(type) || CompareHashes(type))
			return false;
		
		try { Files.copy(GetPath(type, true), GetPath(type, false), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1400"); return false; }
		
		return true;
	}
	
	/** Restore the archive of the specified type. */
	public boolean RestoreArchive(Type type, int iteration)
	{
		if(!ArchiveExists(type, iteration) || CompareHashes(type, iteration))
			return false;
		
		try { Files.copy(GetPath(type, iteration), GetPath(type, false), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1500"); return false; }
		
		return true;
	}
	
	/** Restore the last archive or archive iteration of the specified type. */
	public boolean RestoreAnyArchive(Type type)
	{
		if(!RestoreArchive(type))
		{
			if(!RestoreArchive(type, 1))
			{
				if(!RestoreArchive(type, 2))
				{
					if(!RestoreArchive(type, 3))
						return false;
					else
					{
						try { Files.copy(GetPath(type, 3), GetPath(type, true), StandardCopyOption.REPLACE_EXISTING);
						} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1608"); }
						
						try { Files.delete(GetPath(type, 3));
						} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1609"); }
						
						try { Files.delete(GetPath(type, 2));
						} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D160A"); }
						
						try { Files.delete(GetPath(type, 1));
						} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D160B"); }
					}
				}
				else
				{
					try { Files.copy(GetPath(type, 2), GetPath(type, true), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1604"); }
					
					try { Files.copy(GetPath(type, 3), GetPath(type, 1), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1605"); }
					
					try { Files.delete(GetPath(type, 3));
					} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1606"); }
					
					try { Files.delete(GetPath(type, 2));
					} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1607"); }
				}
			}
			else
			{
				try { Files.copy(GetPath(type, 1), GetPath(type, true), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1600"); }
				
				try { Files.copy(GetPath(type, 2), GetPath(type, 1), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1601"); }
				
				try { Files.copy(GetPath(type, 3), GetPath(type, 2), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1602"); }
				
				try { Files.delete(GetPath(type, 3));
				} catch (IOException e) { ProgramCore.io().println("Error Code : 0x0D1603"); }
			}
		}
		
		return true;
	}
}
