package xx.projectzero.core;

import java.util.ArrayList;

import xx.projectzero.SchedulerThread.*;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.ui.MainViewController.MVTabs;
import xx.projectzero.exceptions.InvalidArgumentException;
import xx.projectzero.exceptions.OverrideException;

/**
 * Class scheduling maintenance tasks and ordering common tasks.
 * @author Guillaume Gravetot
 * @version 1.0.0, 28/04/2016
 * @since 1.1.1, 28/04/2016
 */
public class Scheduler
{
	private volatile AnalyzeThread AT;
	private volatile ArrayList<ComparatorThread> CT;
	private ArrayList<Integer> EmptyCTIndexes;
	
	private volatile ArrayList<String> InputStack;
	
	private int CurrentUserResponseID;
	
	/** Add a new entry to the input stack. */
	public synchronized void AddToInputStack(String object)
	{
		InputStack.add(object);
	}
	
	/** Returns the next intput of the stack. */
	public String GetNextInput()
	{
		String in = "";
		
		synchronized (InputStack)
		{
			in = InputStack.get(0);
			InputStack.remove(0);
		}
		
		CurrentUserResponseID++;
		return in;
	}
	
	/** Returns <b>true</b> if the stack got a next intput. */
	public boolean HasNextInput()
	{
		return !InputStack.isEmpty();
	}
	
	/**
	 * Main constructor of this class.
	 */
	public Scheduler()
	{
		ProgramCore.io().printlntotab("Starting Scheduler", MVTabs.Scheduler);
		
		CurrentUserResponseID = 0;
		EmptyCTIndexes = new ArrayList<Integer>();
		CT = new ArrayList<ComparatorThread>();
		
		InputStack = new ArrayList<String>();
		
		try
		{
			ProgramCore.io().printlntotab("Registering Exit event", MVTabs.Scheduler);
			ProgramCore.GetLogger().log(new LogObject("Registering Exit event", "Scheduler", LogType.Action, Priority.Medium));
			ProgramCore.GetEventManager().Register("Exit", this, this.getClass().getMethod("Stop"));
		} catch(NoSuchMethodException | SecurityException e)
		{
			ProgramCore.io().println("Error Code : 0x071000");
			ProgramCore.io().printlntotab("Error Code : 0x071000", MVTabs.Scheduler);
			ProgramCore.GetLogger().log(new LogObject("Error while registering Exit event : " + e.getMessage(), "Scheduler", LogType.Error, Priority.High));
		}
		
		ProgramCore.io().printlntotab("Creating and starting Analyze Thread", MVTabs.Scheduler);
		ProgramCore.GetLogger().log(new LogObject("Creating and starting Analyze Thread", "Scheduler", LogType.Action, Priority.Medium));
		AT = new AnalyzeThread();
		AT.start();
	}
	
	/** Event Callback function to stop the scheduler. */
	public void Stop()
	{
		ProgramCore.GetEventManager().Fire("Exiting");
		System.out.println("Exiting Scheduler");
	}
	
	/** Method to call to create and schedule a new ComparatorThread. */
	public synchronized void ScheduleNewComparator(ComparatorDataStruct cds)
	{
		ProgramCore.io().printlntotab("Scheduling new Comparator", MVTabs.Scheduler);
		ProgramCore.GetLogger().log(new LogObject("Scheduling new Comparator", "Scheduler", LogType.Action, Priority.Medium));
		if(!EmptyCTIndexes.isEmpty())
		{
			int index = EmptyCTIndexes.get(0).intValue();
			CT.add(index, new ComparatorThread(cds, CurrentUserResponseID));
			CT.get(index).start(index);
			
			EmptyCTIndexes.remove(0);
		}
		else
		{
			CT.add(new ComparatorThread(cds, CurrentUserResponseID));
			int index = CT.size() - 1;
			CT.get(index).start(index);
		}
		
		ProgramCore.GetLogger().log(new LogObject("Comparator scheduled", "Scheduler", LogType.Info, Priority.Low));
	}
	
	/** Internal method used for destruction callback.<br>Call this this the index member of a ComparatorThread to stop and destroy it. */
	public synchronized void ScheduleComparatorDestruction(int index)
	{
		CT.add(index, null);
		EmptyCTIndexes.add(index);
		
		ProgramCore.io().printlntotab("Comparator " + String.valueOf(index) + " destroyed", MVTabs.Scheduler);
		ProgramCore.GetLogger().log(new LogObject("Comparator destroyed", "Scheduler", LogType.Info, Priority.Low));
	}
}
