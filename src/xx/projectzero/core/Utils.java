package xx.projectzero.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.apache.commons.codec.binary.Base64;

import xx.projectzero.learning.core.MasterClass;

/**
 * Classe de fonctions et de structures statiques.
 * @author Guillaume Gravetot
 * @version 1.0.1, 18/04/2016
 * @since 1.0.0, 12/04/2016
 */
@SuppressWarnings("rawtypes")
public class Utils
{
	/**
	 * Structure de donn�es des principaux <i>packages</i> du programme.
	 */
	public static class Packages
	{
		public static final String Core = "xx.projectzero.core";
		public static final String IOModule = "xx.projectzero.iomodule";
		public static final String LearningClasses = "xx.projectzero.learning.classes";
		public static final String LearningCore = "xx.projectzero.learning.core";
	}
	
	public static enum Priority
	{
		High,
		Medium,
		Low
	}
	
	public static enum ProgramClass
	{
		ScriptManager,
		ClassWriter
	}
	
	/** Correspond � \n 			*/
	public static final String nl = "\n";
	/** Correspond � \n\n 			*/
	public static final String nlnl = "\n\n";
	/** Correspond � \n\n\t			*/
	public static final String nlnlt = "\n\n\t";
	/** Correspond � \n\n\t\t		*/
	public static final String nlnltt = "\n\n\t\t";
	/** Correspond � \n\t			*/
	public static final String nlt = "\n\t";
	/** Correspond � \n\t\t			*/
	public static final String nltt = "\n\t\t";
	/** Correspond � \n\t\t\t		*/
	public static final String nlttt = "\n\t\t\t";
	/** Correspond � \n\t\t\t\t		*/
	public static final String nltttt = "\n\t\t\t\t";
	/** Correspond � \n\t\t\t\t\t	*/
	public static final String nlttttt = "\n\t\t\t\t\t";
	/** Correspond � \t 			*/
	public static final String t = "\t";
	/** Correspond � \t\t			*/
	public static final String tt = "\t\t";
	
	public static <T> T[] concatenate (T[] a, T[] b) {
	    int aLen = a.length;
	    int bLen = b.length;

	    @SuppressWarnings("unchecked")
	    T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen+bLen);
	    System.arraycopy(a, 0, c, 0, aLen);
	    System.arraycopy(b, 0, c, aLen, bLen);

	    return c;
	}
	
	/**
	 * Compile le fichier sp�cifi�.
	 * @param FilePath <b>String</b> : Chemin vers le fichier � compiler.
	 * @return <b>boolean</b> : <b>true</b> si le fichier a �t� compil� avec succ�s.
	 */
	public static boolean CompileFile(String FilePath)
	{
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
		Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromStrings(Arrays.asList(FilePath));
		JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, diagnostics, null, null, compilationUnits);
		boolean success = task.call();
		
		try
		{
			fileManager.close();
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		
		return success;
	}
	
	/**
	 * M�thode qui permet de d�s�rialiser un objet s�rialis� par la fonction Serialize.
	 * @param serializedObject <b>String</b> : Objet s�rialis�.
	 * @return <b>Object</b> : Objet d�serialis�.
	 * @see Utils#Serialize(Object)
	 */
	public static Object Deserialize(String serializedObject)
	{
		ByteArrayInputStream in = new ByteArrayInputStream(Base64.decodeBase64(serializedObject.getBytes()));
	    try
		{
			return new ObjectInputStream(in).readObject();
		} catch (ClassNotFoundException | IOException e)
		{
			ProgramCore.io().println("Error Code : 0x051300");
		}
	    
	    return null;
	}
	
	/**
	 * Retourne le nom r�duit de la classe pass�e en argument.
	 * @param c <b>Class</b> : Classe.
	 * @return <b>String</b> : Nom r�duit de la classe.
	 * @see Utils#GetClassNameFromCompleteName(String)
	 */
	public static String GetClassName(Class c)
	{
		return c.getSimpleName();
	}
	
	/**
	 * Retourne le nom r�duit de la classe � partir du nom complet.
	 * @param completeName <b>String</b> : Nom complet de la classe.
	 * @return <b>String</b> : Nom r�duit de la classe.
	 * @see Utils#GetClassName(Class)
	 */
	public static String GetClassNameFromCompleteName(String completeName)
	{
		String[] t = completeName.split("[.]");
		
		return t[t.length-1];
	}
	
	/**
	 * Retourne le chemin relatif en fonction du nom complet.
	 * @param completeName <b>String</b> : Nom complet de la classe.
	 * @return <b>String</b> : Chemin relatif vers le fichier.
	 * @see Utils#GetPackagePathFromCompleteName(String)
	 */
	public static String GetCompletePathFromCompleteName(String completeName)
	{
		String[] t = completeName.split("[.]");
		String s = "";
		
		for(int i = 0; i < t.length; i++)
		{
			if(i != 0)
				s += "/";
			
			s += t[i];
		}
		
		return s;
	}
	
	/**
	 * Appelle la fonction <i>GetCorrespondingPrimitiveType(o, true)</i>
	 * @param o <b>Object</b> : Objet � tester.
	 * @return <b>String</b> : Type primitif de l'objet.
	 * @see Utils#GetCorrespondingPrimitiveType(Object, boolean)
	 */
	public static String GetCorrespondingPrimitiveType(Object o)
	{
		return GetCorrespondingPrimitiveType(o, true);
	}
	
	/**
	 * Retourne le type de l'objet.
	 * @param o <b>Object</b> : Objet � tester.
	 * @param includeObjects <b>boolean</b> : <b>true</b> pour inclure les objets autres que primitifs dans le retour.
	 * @return <b>String</b> : Type de l'objet.
	 */
	public static String GetCorrespondingPrimitiveType(Object o, boolean includeObjects)
	{
		if(o == null)
			return "";
		
		if(!IsPrimitiveType(o) && includeObjects)
		{
			if(!isArray(o) || isPrimitiveArray(o))
				return GetClassName(o.getClass());
			
			String t = GetClassName(o.getClass());
			if(o instanceof Map)
			{
				if(((Map)o).size() > 0)
				{
					t += "<" + ((Map)o).keySet().toArray()[0].getClass().getSimpleName() + ", " + ((Map)o).values().toArray()[0].getClass().getSimpleName() + ">";
				}
				return t;
			}
			else
			{
				if(((List)o).size() > 0)
					t += "<" + ((List)o).get(0).getClass().getSimpleName() + ">";
				
				return t;
			}
		}
		
		if(o instanceof Integer)
			return "int";
		else if(o instanceof Float)
			return "float";
		else if(o instanceof Double)
			return "double";
		else if(o instanceof Boolean)
			return "boolean";
		else return "String";
	}
	
	/**
	 * Retourne le nom du package en fonction du nom complet de la classe.
	 * @param completeName <b>String</b> : Nom complet de la classe.
	 * @return <b>String</b> : Nom du package.
	 * @see Utils#GetClassNameFromCompleteName(String)
	 */
	public static String GetPackageNameFromCompleteName(String completeName)
	{
		String[] t = completeName.split("[.]");
		String s = "";
		
		for(int i = 0; i < t.length-1; i++)
		{
			if(i != 0)
				s += ".";
			
			s += t[i];
		}
		
		return s;
	}
	
	/**
	 * Retourne le chemin relatif vers le package � partir du nom complet.
	 * @param completeName <b>String</b> : Nom complet de la classe.
	 * @return <b>String</b> : Chemin relatif vers la package.
	 * @see Utils#GetPackagePathFromPackageName(String)
	 */
	public static String GetPackagePathFromCompleteName(String completeName)
	{
		String[] t = completeName.split("[.]");
		String s = "";
		
		for(int i = 0; i < t.length-1; i++)
		{
			if(i != 0)
				s += "/";
			
			s += t[i];
		}
		
		return s;
	}
	
	/**
	 * Retourne le chemin relatif vers le package � partir du package.
	 * @param packageName <b>String</b> : Nom du package.
	 * @return <b>String</b> : Chemin relatif vers le package.
	 * @see Utils#GetPackagePathFromCompleteName(String)
	 */
	public static String GetPackagePathFromPackageName(String packageName)
	{
		String[] t = packageName.split("[.]");
		String s = "";
		
		for(int i = 0; i < t.length; i++)
		{
			if(i != 0)
				s += "/";
			
			s += t[i];
		}
		
		return s;
	}
	
	/**
	 * Retourne le type complet SQL correspondant � l'objet envoy�.
	 * @param o <b>Object</b> : Objet � d�terminer le type.
	 * @return <b>String</b> : Type SQL complet correspondant.
	 */
	public static String GetSQLType(Object o)
	{
		if(IsPrimitiveType(o))
		{
			String s = GetCorrespondingPrimitiveType(o, false);

			if(s.equals("String"))
				return "VARCHAR(255)";
			else return s.toUpperCase();
		}
		else
			return "MEDIUMTEXT";
	}
	
	/**
	 * Appelle implicitement <i>GetStringFromObject(o, false)</i>
	 * @see Utils#GetStringFromObject(Object, boolean)
	 */
	public static String GetStringFromObject(Object o) { return GetStringFromObject(o, false); }
	
	/**
	 * Retourne la valeur de l'objet sous la forme d'un string.
	 * @param o <b>Object</b> : Objet.
	 * @param beauty <b>boolean</b> : Si L'objet n'est pas un type primitif, <b>true</b> pour envoyer un <i>Human Readable</i> String.
	 * @return <b>String</b> : La valeur de l'objet.
	 */
	public static String GetStringFromObject(Object o, boolean beauty)
	{
		if(IsPrimitiveType(o))
		{
			if(o instanceof String)
				return "\"" + o.toString() + "\"";
			
			if(o instanceof Float)
				return o.toString() + "f";
			
			return o.toString();
		}
		else
		{
			String Temp = "";
			
			if(o instanceof MasterClass)
			{
				if(beauty)
				{
					String IName = "main";
					
					try
					{
						IName = (String)o.getClass().getField("CurrentInstanceName").get(o);
					} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e)
					{
						ProgramCore.io().println("Error Code : " + "0x050000");
					}
					
					Temp = "Instance name : " + IName;
				}
				else
				{
					Temp = "new " + GetClassName(o.getClass()) + "(";
					
					String IName = "main";
					
					try
					{
						IName = (String)o.getClass().getField("CurrentInstanceName").get(o);
					} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e)
					{
						ProgramCore.io().println("Error Code : " + "0x050001");
					}
					
					Temp += IName + ")";
				}
			}
			else if(isArray(o))
			{
				if(isPrimitiveArray(o))
				{
					Object[] oArray = GetValuesFromArray(o);
					
					if(beauty)
					{
						Temp = "[";
						
						for(int i = 0; i < oArray.length; i++)
						{
							if(i > 0)
								Temp += ",";
							
							Temp += " " + GetStringFromObject(oArray[i]);
						}
						
						Temp += "]";
					}
					else
					{
						Temp = "new " + o.getClass().getSimpleName() + " {";
						
						for(int i = 0; i < oArray.length; i++)
						{
							if(i > 0)
								Temp += ",";
							
							Temp += " " + GetStringFromObject(oArray[i]);
						}
						
						Temp += "}";
					}
				}
				else
				{
					if(o instanceof Map)
					{
						if(beauty)
						{
							Temp = "{";
							Map m = (Map)o;
							Object[] arr = m.keySet().toArray();
							
							for(int i = 0; i < arr.length; i++)
							{
								if(i > 0)
									Temp += ", ";
								
								Temp += "[" + GetStringFromObject(arr[i], true) + " - " + GetStringFromObject(m.get(arr[i]), true) + "]";
							}
							
							Temp += "}";
						}
						else
						{
							Temp = "new " + o.getClass().getSimpleName();
							if(((Map)o).size() > 0)
							{
								Temp += "<" + ((Map)o).keySet().toArray()[0].getClass().getSimpleName() + ", " + ((Map)o).values().toArray()[0].getClass().getSimpleName() + ">()";
							}
							else
							{
								 Temp += "()";
							}
						}
					}
					else if(o instanceof List)
					{
						if(beauty)
						{
							Temp = "[";
							
							for(int i = 0; i < ((List)o).size(); i++)
							{
								if(i > 0)
									Temp += ",";
								
								Temp += " " + GetStringFromObject(((List)o).get(i));
							}
							
							Temp += "]";
						}
						else
						{
							Temp = "new " + o.getClass().getSimpleName();
							if(((List)o).size() > 0)
							{
								Temp += "<" + ((List)o).get(0).getClass().getSimpleName() + ">(Arrays.asList(";
								
								for(int i = 0; i < ((List)o).size(); i++)
								{
									if(i > 0)
										Temp += ", ";
									
									Temp += GetStringFromObject(((List)o).get(i));
								}
								
								Temp += "))";
							}
							else
							{
								 Temp += "()";
							}
						}
					}
				}
			}
			else
			{
				Temp = "new " + GetClassName(o.getClass()) + "()";
			}
			
			return Temp;
		}
	}
	
	/**
	 * Retourne les valeurs du tableau 'primitif' en param�tre.
	 * @param array <b>Object</b> : Tableau 'primitif', eg. <i>int[]</i>
	 * @return <b>Object[]</b> : Valeurs contenues dans le tableau.
	 */
	public static Object[] GetValuesFromArray(Object array)
	{
		if(array == null || !isPrimitiveArray(array))
			return null;
		
		if(array instanceof int[])
			return Arrays.stream((int[])array).boxed().toArray(Integer[]::new);
		else if(array instanceof double[])
			return Arrays.stream((double[])array).boxed().toArray(Double[]::new);
		else if(array instanceof long[])
			return Arrays.stream((long[])array).boxed().toArray(Long[]::new);
		else if(array instanceof boolean[])
		{
			boolean[] b = (boolean[])array;
			Boolean[] bb = new Boolean[b.length];
			
			for(int i = 0; i < b.length; i++) bb[i] = new Boolean(b[i]);
			
			return bb;
		}
		else if(array instanceof float[])
		{
			float[] f = (float[])array;
			Float[] ff = new Float[f.length];
			
			for(int i = 0; i < f.length; i++) ff[i] = new Float(f[i]);
			
			return ff;
		}
		else
			return (Object[])array;
	}
	
	/**
	 * Permet d'incr�menter la valeur a la fin d'un String.<br>
	 * <ul><li>"str" retournera "str1"</li><li>"str2" retournera "str3"</li><li>"str9" retournera "str10"</li></ul>
	 * @param incStr <b>String</b> : String � incr�menter.
	 * @return <b>String</b> : String incr�menter.
	 */
	public static String IncrementString(String incStr)
	{
		String num = "";
		
		while(true)
		{
			char c = incStr.charAt(incStr.length()-(num.length()+1));
			if(c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9')
				num = c + num;
			else
				break;
		}
		
		if(num == "")
			return incStr + "1";
		
		int i = Integer.parseInt(num)+1;
		
		return incStr+String.valueOf(i);
	}
	
	/**
	 * Retourne <b>true</b> si l'objet est un tableau (statique ou dynamique).
	 * @param o <b>Object</b> : Objet � tester.
	 */
	public static boolean isArray(Object o)
	{
		if(o.getClass().isArray())
			return true;
		else if(o instanceof List)
			return true;
		else if(o instanceof Map)
			return true;
		else
			return false;
	}
	
	/**
	 * Retourne <b>true</b> si l'objet est un tableau primitif (eg. <i>int[]</i>).
	 * @param o <b>Object</b> : Objet � tester.
	 */
	public static boolean isPrimitiveArray(Object o)
	{
		return o.getClass().isArray();
	}
	
	/**
	 * Retourne si le type de l'objet pass� en argument est primitif.
	 * @param obj <b>Object</b> : Objet � tester.
	 * @return <b>boolean</b> : <b>true</b> si le type de l'objet est primitif.
	 */
	public static boolean IsPrimitiveType(Object obj)
	{
		if(obj instanceof Integer || obj instanceof Float || obj instanceof Double || obj instanceof Boolean || obj instanceof String)
			return true;
		else
			return false;
	}
	
	/**
	 * Ouvre une connexion vers la base de donn�es.
	 */
	public static Connection PrepareConnection()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e)
		{
			ProgramCore.io().println("Error Code : 0x051100");
		}
		
	    try
		{
			return DriverManager.getConnection("jdbc:mysql://127.0.0.1/projectzero", "root", "root");
		} catch (SQLException e)
		{
			ProgramCore.io().println("Error Code : 0x051101");
		}
	    
	    return null;
	}
	
	/**
	 * M�thode qui permet de s�rialiser un objet s�rialisable.
	 * @param o <b>Object</b> : Objet � s�rialiser.
	 * @return <b>String</b> : Objet s�rialis�.
	 * @see Utils#Deserialize(String)
	 */
	public static String Serialize(Object o)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		if(o instanceof MasterClass)
			return ((MasterClass)o).safeSaveCurrentInstance();
		
	    try
		{
			new ObjectOutputStream(out).writeObject(o);
		} catch(NotSerializableException e)
	    {
			ProgramCore.io().println("Error Code : 0x051200");
			return "";
		} catch(IOException e)
		{
			ProgramCore.io().println("Error Code : 0x050201");
		}

	    return new String(Base64.encodeBase64(out.toByteArray()));
	}
	
	/**
	 * Renvoi l'objet pars� dans un objet de type primitif.
	 * @param object <b>String</b> : Valeur � parser.
	 * @return <b>Object</b> : Valeur pars�e.
	 */
	public static Object TryParse(String object)
	{
		try
		{
			Integer i = Integer.parseInt(object);
			
			return i;
		} catch(NumberFormatException e)
		{
			try
			{
				Float f = Float.parseFloat(object);
				
				return f;
			} catch(NumberFormatException e1)
			{
				if(object.equals("true") || object.equals("false"))
					return Boolean.parseBoolean(object);
				else return object;
			}
		}
	}
	
	/**
	 * Removes any ponctuation marks in the provided sentence.
	 * @param sentence <b>String</b> : Sentence to use.
	 * @return <b>String</b> : Ponctuationless sentence.
	 */
	public static String RemovePonctuation(String sentence)
	{
		return sentence.replaceAll("[.!;:/.?,\\-'\"&()=+*%$��]", " ");
	}
	
	/**
	 * Removes any superfluous spaces in the provided sentence.
	 * @param sentence <b>String</b> : Sentence to work with.
	 * @return <b>String</b> : Space-correct sentence.
	 */
	public static String CleanSpaces(String sentence)
	{
		return sentence.replaceAll("\\s+", " ");
	}
	
	/**
	 * Clean the provided sentence.
	 * @param sentence <b>String</b> : Sentence to clean.
	 * @return <b>String</b> : Cleaned sentence.
	 * @see Utils#RemovePonctuation(String)
	 * @see Utils#CleanSpaces(String)
	 */
	public static String CleanSentence(String sentence)
	{
		return CleanSpaces(RemovePonctuation(sentence));
	}
	
	/** Returns <b>true</b> if the <i>match</i> is found in the provided array.
	 * @param array <b>String[]</b> : Array to check.
	 * @param match <b>String</b> : Object to check.
	 */
	public static boolean ArrayContains(String[] array, String match)
	{
		if(array == null || array.length == 0)
			return false;
		
		for(String string : array)
		{
			if(string.equals(match))
				return true;
		}
		
		return false;
	}
	
	/** Returns the index of the provided element in the provided array.
	 * @param array <b>String[]</b> : Array to check.
	 * @param match <b>String</b> : Object to check.
	 */
	public static int GetIndex(String[] array, String match)
	{
		if(!ArrayContains(array, match))
			return -1;
		
		for(int i = 0; i < array.length; i++)
		{
			if(array[i].equals(match))
				return i;
		}
		
		return -1;
	}
}
