package xx.projectzero.core;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.TreeMap;

import xx.projectzero.annotations.SingleInstance;
import xx.projectzero.console.Command;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.ui.MainViewController.MVTabs;
import xx.projectzero.learning.core.MasterClass;

/**
 * Classe gerant le r�f�rencement et l'appel aux classes <i>Learning</i>.
 * @author Guillaume Gravetot
 * @version 1.0.0, 12/04/2016
 * @since 1.0.0, 12/04/2016
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class ClassManager
{
	private class ClassListCacher implements Runnable
	{
		private Thread t;
		public volatile boolean speedup = false;
		
		/** Dont call this.<br>Call start instead. 
		 * @see ClassListCacher#start()*/
		@Override
		public void run()
		{
			String[] buffer = new String[registeredClasses.size()];
			int i = 0;

			for(String str : registeredClasses.keySet())
			{
				buffer[i++] = str;
				
				if(!speedup)
					try { Thread.sleep(100L); } catch (InterruptedException e) {}
			}
			
			classListAddr = ProgramCore.GetCache().CacheObject(buffer);
			classListCached = true;
		}
		
		/** Method to call to start a new ClassListCacher. */
		public void start()
		{
			if(t == null)
			{
				String name = "ClassListCacherThread";
				t = new Thread(this, name);
				t.setDaemon(true);
				t.start();
			}
		}
	}
	
	private volatile TreeMap<String, MasterClass> referencedClasses;
	private volatile TreeMap<String, Class> registeredClasses;
	
	private ClassListCacher cacher;
	private volatile boolean classListCached;
	private volatile int classListAddr;
	
	/**
	 * Constructeur principal de la classe.
	 * @see ClassManager
	 */
	public ClassManager()
	{
		referencedClasses = new TreeMap<String, MasterClass>();
		registeredClasses = new TreeMap<String, Class>();
		cacher = new ClassListCacher();
		cacher.start();
		
		ProgramCore.GetLogger().log(new LogObject("Adding command \"classmanager\" to console", "ScriptManager", LogType.Action, Priority.Low));
		ProgramCore.GetConsole().AddCommand(new Command("classmanager") {
			@Override
			public void Exec(String[] params)
			{
				if(params.length == 0)
					PrintHelp();
				else if(Utils.ArrayContains(params,"-h") || Utils.ArrayContains(params,"--help"))
					PrintHelp();
				else if(Utils.ArrayContains(params,"-l") || Utils.ArrayContains(params,"--list"))
				{
					StringBuilder line = new StringBuilder();
					line.append("Currently registered and referenced classes :");
					for(String s : GetClassList())
						line.append("\n\t" + s);
					
					ProgramCore.io().printlntotab(line.toString(), MVTabs.Console);
				}
				else if(Utils.ArrayContains(params,"-c") || Utils.ArrayContains(params,"--class"))
				{
					int ind = Utils.GetIndex(params, "-c");
					
					if(ind == -1)
					{
						ind = Utils.GetIndex(params, "--class");
						
						if(ind == -1)
						{
							ProgramCore.io().printlntotab("Unknown class argument", MVTabs.Console);
							PrintHelp();
							return;
						}
					}
					
					if(ind +1 == params.length)
					{
						ProgramCore.io().printlntotab("Unknown class argument", MVTabs.Console);
						PrintHelp();
						return;
					}
					
					String classname = params[ind+1];
					String instancename = "";
					
					if(Utils.ArrayContains(params,"-i") || Utils.ArrayContains(params,"--instance"))
					{
						ind = Utils.GetIndex(params, "-i");
						
						if(ind == -1)
						{
							ind = Utils.GetIndex(params, "--instance");
							
							if(ind != -1 && ind +1 < params.length)
								instancename = params[ind];
						}
						else if(ind +1 < params.length)
							instancename = params[ind];
					}
					
					if(Utils.ArrayContains(params,"-e") || Utils.ArrayContains(params,"--exists"))
						ProgramCore.io().printlntotab("Class " + classname + " is " +  (IsClassReferenced(classname)?"":"not ") + "referenced", MVTabs.Console);
					else if(Utils.ArrayContains(params,"-p") || Utils.ArrayContains(params,"--print"))
					{
						if(!instancename.equals(("")))
							ProgramCore.io().printlntotab(GetRegisteredClass(classname, instancename).toString(), MVTabs.Console);
						else
							ProgramCore.io().printlntotab(GetReferencedClass(classname).toString(), MVTabs.Console);
					}
				}
			}
			
			private void PrintHelp()
			{
				ProgramCore.io().printlntotab("Help text for classmanager command.\n\nclassmanager [-arguments] [-options]\n\nArguments:\n\t-h, --help : Display this help. Ignores other options.\n\t-l, --list : Lists all registered and referenced classes.\n\t-c, --class : Specifies a class to use with the following options :\n\tOptions:\n\t\t"
						+ "-i, --instance : Specifies an instance to use with this class.\n\t\t"
						+ "-e, --exists : Returns wether this class exists and is registered, or not.\n\t\t"
						+ "-p, --print : Calls toString methods and print the result.\n\t\t", MVTabs.Console);
			}
		});
	}
	
	/** Returns a list of all classes registered. */
	public synchronized String[] GetClassList()
	{
		if(!classListCached)
		{
			cacher.speedup = true;
			
			while(!classListCached)
				try { Thread.sleep(50L); } catch (InterruptedException e) {}
		}
		
		return ProgramCore.GetCache().Get(classListAddr, String[].class);
	}
	
	/**
	 * Permet de r�f�rencer l'objet dans ClassManager.
	 * @param classToReference <b>MasterClass</b> : Objet � r�f�rencer.
	 * @see ClassManager#AddClassToReference(Class)
	 */
	public synchronized void AddClassToReference(MasterClass classToReference)
	{
		referencedClasses.put(Utils.GetClassName(classToReference.getClass()), classToReference);
		registeredClasses.put(Utils.GetClassName(classToReference.getClass()), classToReference.getClass());
		
		if(classListCached)
		{
			classListCached = false;
			ProgramCore.GetCache().Free(classListAddr);
			classListAddr = -1;
			
			cacher = new ClassListCacher();
			cacher.start();
		}
	}
	
	/**
	 * Permet de r�f�rencer la classe dans ClassManager.
	 * @param classToReference <b>Class</b> : Classe � r�f�rencer.
	 * @see ClassManager#AddClassToReference(MasterClass)
	 */
	public synchronized void AddClassToReference(Class classToReference)
	{
		String className = Utils.GetClassName(classToReference);
		
		if(referencedClasses.containsKey(className))
		{
			referencedClasses.remove(className);
			registeredClasses.remove(className);
		}
		
		try
		{
			referencedClasses.put(className, (MasterClass)classToReference.newInstance());
		} catch(InstantiationException | IllegalAccessException e)
		{
			ProgramCore.io().println("Error Code : 0x010000");
		}
		
		registeredClasses.put(className, classToReference);
		
		if(classListCached)
		{
			classListCached = false;
			ProgramCore.GetCache().Free(classListAddr);
			classListAddr = -1;
			
			cacher = new ClassListCacher();
			cacher.start();
		}
	}
	
	/**
	 * Permet de r�cup�rer un objet r�f�renc�.
	 * @param name <b>String</b> : Nom de l'objet r�f�renc� � retourner.
	 * @return <b>MasterClass</b> : Pointeur vers l'objet r�f�renc�e.
	 * @see ClassManager#GetRegisteredClass(String)
	 */
	public synchronized MasterClass GetReferencedClass(String name)
	{
		return referencedClasses.get(name);
	}
	
	/**
	 * Permet de r�cup�rer une classe r�f�renc�e.
	 * @param name <b>String</b> : Nom de la classe r�f�renc�e � retourner.
	 * @return <b>Class</b> : Pointeur vers la classe r�f�renc�e.
	 * @see ClassManager#GetReferencedClass(String)
	 */
	public synchronized Class GetRegisteredClass(String name)
	{
		return registeredClasses.get(name);
	}
	
	/**
	 * Permet de r�cup�rer une classe r�f�renc�e.
	 * @param name <b>String</b> : Nom de la classe r�f�renc�e � retourner.
	 * @return <b>Class</b> : Pointeur vers la classe r�f�renc�e.
	 * @see ClassManager#GetReferencedClass(String)
	 */
	public synchronized MasterClass GetRegisteredClass(String name, String instanceName)
	{
		MasterClass mc = null;
		try
		{
			mc = (MasterClass)registeredClasses.get(name).newInstance();
		} catch (InstantiationException | IllegalAccessException e)
		{
			ProgramCore.io().println("Error Code : 0x011400");
			return null;
		}
		
		mc.loadInstance(instanceName);
		return mc;
	}
	
	/**
	 * Permet de savoir si l'objet est r�f�renc�e.
	 * @param mclass <b>MasterClass</b> : Objet � tester.
	 * @return <b>boolean</b> : <b>true</b> si l'objet est r�f�renc�.
	 * @see ClassManager#IsClassReferenced(Class)
	 * @see ClassManager#IsClassReferenced(String)
	 */
	public synchronized boolean IsClassReferenced(MasterClass mclass)
	{
		return referencedClasses.containsValue(mclass);
	}
	
	/**
	 * Permet de savoir si la classe est r�f�renc�e.
	 * @param c <b>Class</b> : Classe � tester.
	 * @return <b>boolean</b> : <b>true</b> si la classe est r�f�renc�e.
	 * @see ClassManager#IsClassReferenced(MasterClass)
	 * @see ClassManager#IsClassReferenced(String)
	 */
	public synchronized boolean IsClassReferenced(Class c)
	{
		return registeredClasses.containsValue(c);
	}
	
	/**
	 * Permet de savoir si la classe et l'objet portant le nom indiqu� sont r�f�renc�s.
	 * @param name <b>String</b> : Nom de l'objet et de la classe.
	 * @return <b>boolean</b> : <b>true</b> si l'objet et la classe sont r�f�renc�s.
	 * @see ClassManager#IsClassReferenced(Class)
	 * @see ClassManager#IsClassReferenced(MasterClass)
	 */
	public synchronized boolean IsClassReferenced(String name)
	{
		return referencedClasses.containsKey(name) && registeredClasses.containsKey(name);
	}
	
	/**
	 * Permet v�rifier l'existence de la m�hode indiqu�e dans l'objet indiqu�.
	 * @param ClassName <b>String</b> : Nom de l'objet/classe � tester.
	 * @param MethodName <b>String</b> : Nom de la m�thode � tester.
	 * @return <b>boolean</b> : </b>true</b> si l'objet contient la m�thode.
	 * @see ClassManager#ObjectContainsMember(String, String)
	 * @see ClassManager#CallMethodFromObject(String, String)
	 */
	public synchronized boolean ObjectContainsMethod(String ClassName, String MethodName)
	{
		if(!IsClassReferenced(ClassName))
			return false;
		
		Class mc = GetRegisteredClass(ClassName);
		
		try
		{
			mc.getMethod(MethodName);
			return true;
		} catch(NoSuchMethodException | SecurityException e)
		{
			return false;
		}
	}
	
	/**
	 * Appelle la fonction avec le nom donn� sur l'objet avec le nom donn�.
	 * @param ObjectName <b>String</b> : Nom de l'objet � utiliser.
	 * @param FunctionName <b>String</b> : Nom de la fonction � appeler.
	 * @return <b>Object</b> : Retour de la fonction appel�e.
	 * @see ClassManager#ObjectContainsMethod(String, String)
	 */
	public synchronized Object CallMethodFromObject(String ObjectName, String FunctionName)
	{
		MasterClass mc = referencedClasses.get(ObjectName);
		
		try
		{
			return mc.getClass().getMethod(FunctionName).invoke(mc);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			ProgramCore.io().println("Error Code : 0x011100");
			return null;
		}
	}
	
	/**
	 * Permet v�rifier l'existence du membre indiqu�e dans l'objet indiqu�.
	 * @param ObjectName <b>String</b> : Nom de l'objet/classe � tester.
	 * @param MemberName <b>String</b> : Nom du membre � tester.
	 * @return <b>boolean</b> : </b>true</b> si l'objet contient le membre.
	 * @see ClassManager#ObjectContainsMethod(String, String)
	 * @see ClassManager#GetObjectMember(String, String)
	 * @see ClassManager#GetObjectMember(String, String, String)
	 */
	public synchronized boolean ObjectContainsMember(String ObjectName, String MemberName)
	{
		if(!IsClassReferenced(ObjectName))
			return false;
		
		try
		{
			GetRegisteredClass(ObjectName).getField(MemberName);
			return true;
		} catch(NoSuchFieldException | SecurityException e)
		{
			return false;
		}
	}
	
	/**
	 * Permet de r�cup�rer la valeur du membre donn�e dans l'objet r�f�renc� indiqu�.
	 * @param ObjectName <b>String</b> : Nom de l'objet r�f�renc�.
	 * @param MemberName <b>String</b> : Nom du membre � r�cup�rer.
	 * @return <b>Object</b> : Valeur du membre.
	 * @see ClassManager#ObjectContainsMember(String, String)
	 * @see ClassManager#GetObjectMember(String, String, String)
	 */
	public synchronized Object GetObjectMember(String ObjectName, String MemberName)
	{
		if(!ObjectContainsMember(ObjectName, MemberName))
		{
			ProgramCore.io().println("Error Code : 0x011200");
			return null;
		}
		
		try
		{
			return GetRegisteredClass(ObjectName).getField(MemberName).get(GetReferencedClass(ObjectName));
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e)
		{
			ProgramCore.io().println("Error Code : 0x011201");
			return null;
		}
	}
	
	/**
	 * Permet de r�cup�rer la valeur du membre donn� dans l'objet avec l'instance indiqu�.
	 * @param ObjectName <b>String</b> : Nom de l'objet.
	 * @param MemberName <b>String</b> : Nom du membre � r�cup�rer.
	 * @param InstanceName <b>String</b> : Nom de l'instance � utiliser sur l'objet.
	 * @return <b>Object</b> : Valeur du membre.
	 * @see ClassManager#ObjectContainsMember(String, String)
	 * @see ClassManager#GetObjectMember(String, String)
	 */
	public synchronized Object GetObjectMember(String ObjectName, String MemberName, String InstanceName)
	{
		if(!ObjectContainsMember(ObjectName, MemberName))
		{
			ProgramCore.io().println("Error Code : 0x011300");
			return null;
		}
		
		Class c = GetRegisteredClass(ObjectName);
		
		if(c.getAnnotation(SingleInstance.class) != null)
		{
			if(!InstanceName.equals("main"))
			{
				ProgramCore.io().println("Error Code : 0x011301");
				return null;
			}
		}
		
		Constructor constr = null; Object o = null;
		
		try
		{
			constr = c.getConstructor(String.class);
			o = constr.newInstance(InstanceName);
		} catch(NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			ProgramCore.io().println("Error Code : 0x011302");
			return null;
		}
		
		if(o== null)
		{
			ProgramCore.io().println("Error Code : 0x011303");
			return null;
		}
		
		try
		{
			return o.getClass().getField(MemberName).get(o);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e)
		{
			ProgramCore.io().println("Error Code : 0x011304");
			return null;
		}
	}
}
