package xx.projectzero.core;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.util.ArrayList;

import xx.projectzero.annotations.JSCallable;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.ui.MainViewController.MVTabs;

/**
 * Class managing logs.
 * @author Guillaume Gravetot
 * @version 1.0.0, 28/04/2016
 * @since 1.1.2, 28/04/2016
 */
public class Logger
{
	public static class LogObject
	{
		private String LiteralExplanation;
		private String Sender;
		private LogType logType;
		private Priority priority;
		private ZonedDateTime date;
		
		private String Formated;
		
		public LogObject(String literalExplanation, String sender, LogType type, Priority p)
		{
			LiteralExplanation = literalExplanation;
			Sender = sender;
			logType = type;
			priority = p;
			date = ZonedDateTime.now();
			
			Formate();
		}
		
		private void Formate()
		{
			StringBuilder logString = new StringBuilder();
			String separator = " ";

			switch(priority)
			{
				case High:
					separator = " ----- ";
					break;
				case Low:
					break;
				case Medium:
					separator = " --- ";
					break;
				default:
					break;
			}

			logString.append(String.valueOf(date.get(ChronoField.HOUR_OF_DAY)).length()==1?"0"+String.valueOf(date.get(ChronoField.HOUR_OF_DAY)):date.get(ChronoField.HOUR_OF_DAY));
			logString.append("h");
			logString.append(String.valueOf(date.get(ChronoField.MINUTE_OF_HOUR)).length()==1?"0"+String.valueOf(date.get(ChronoField.MINUTE_OF_HOUR)):date.get(ChronoField.MINUTE_OF_HOUR));
			logString.append("m");
			logString.append(String.valueOf(date.get(ChronoField.SECOND_OF_MINUTE)).length()==1?"0"+String.valueOf(date.get(ChronoField.SECOND_OF_MINUTE)):date.get(ChronoField.SECOND_OF_MINUTE));
			logString.append(separator);
			
			switch(logType)
			{
				case Action:
					logString.append("|");
					break;
				case Error:
					logString.append("*");
					break;
				case Info:
					break;
				default:
					break;
			}
			
			logString.append(separator);
			logString.append(Sender);
			logString.append(" : ");
			logString.append(LiteralExplanation);
			
			Formated = logString.toString();
		}

		public String getLiteralExplanation() { return LiteralExplanation; }
		public String getSender() { return Sender; }
		public LogType getLogType() { return logType; }
		public Priority getPriority() { return priority; }
		public ZonedDateTime GetLogDate() { return date; }
		
		public String GetFormated() { return Formated; }
	}
	
	public static enum LogType
	{
		Action,
		Error,
		Info
	}
	
	private volatile ArrayList<LogObject> Logs;
	private volatile FileWriter writer;
	
	public Logger()
	{
		Logs = new ArrayList<LogObject>();
		
		StringBuilder filename = new StringBuilder();
		filename.append(ProgramCore.BasePath);
		filename.append("logs/");
		filename.append(String.valueOf(LocalDate.now().get(ChronoField.DAY_OF_MONTH)).length()==1?"0"+String.valueOf(LocalDate.now().get(ChronoField.DAY_OF_MONTH)):LocalDate.now().get(ChronoField.DAY_OF_MONTH));
		filename.append("-");
		filename.append(String.valueOf(LocalDate.now().get(ChronoField.MONTH_OF_YEAR)).length()==1?"0"+String.valueOf(LocalDate.now().get(ChronoField.MONTH_OF_YEAR)):LocalDate.now().get(ChronoField.MONTH_OF_YEAR));
		filename.append("-");
		filename.append(LocalDate.now().get(ChronoField.YEAR));
		filename.append("_");
		filename.append(String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.HOUR_OF_DAY)):LocalDateTime.now().get(ChronoField.HOUR_OF_DAY));
		filename.append("h");
		filename.append(String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)).length()==1?"0"+String.valueOf(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)):LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR));
		filename.append(".log");
		
		try
		{
			writer = new FileWriter(filename.toString());
		} catch (IOException e)
		{ ProgramCore.io().printlntotab("Error Code : 0x0B0001", MVTabs.Logger); }
		
		stopped = false;
	}
	
	public void init()
	{
		try
		{
			ProgramCore.io().printlntotab("Registering Exit event", MVTabs.Logger);
			ProgramCore.GetEventManager().Register("Exit", this, this.getClass().getMethod("Stop"));
			ProgramCore.GetLogger().log(new LogObject("Event \"Exit\" registered successfully", "Logger", LogType.Info, Priority.Low));
		} catch(NoSuchMethodException | SecurityException e)
		{
			ProgramCore.io().println("Error Code : 0x0B0000");
			ProgramCore.io().printlntotab("Error Code : 0x0B0000", MVTabs.Logger);
			ProgramCore.GetLogger().log(new LogObject("Error while registering event \"Exit\"", "Logger", LogType.Error, Priority.High));
		}
	}
	
	public void Stop()
	{
		stopped = true;
		try
		{
			writer.close();
		} catch(IOException e)
		{
			System.out.println("Error Code : 0x0B0100");
		}
				
		//Event callback
		ProgramCore.GetEventManager().Fire("Exiting");
		System.out.println("Exiting Logger");
	}
	
	private volatile boolean stopped = true;
	
	@JSCallable
	public synchronized void log(LogObject log)
	{
		if(!stopped)
		{
			Logs.add(log);
			
			try
			{
				writer.write(log.GetFormated() + "\r\n");
			} catch (IOException e)
			{
				ProgramCore.io().printlntotab("Error Code : 0x0B0200", MVTabs.Logger);
			}
			
			ProgramCore.io().printlntotab(log.Formated, MVTabs.Logger);
		}
	}
}
