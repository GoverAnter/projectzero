package xx.projectzero.core;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import xx.projectzero.console.Console;
import xx.projectzero.core.Logger.LogObject;
import xx.projectzero.core.Logger.LogType;
import xx.projectzero.core.Utils.Priority;
import xx.projectzero.core.ui.MainViewController;
import xx.projectzero.iomodule.IOCore;
import xx.projectzero.learning.classes.TestClass;
import xx.projectzero.linguistic.LWordDictionary;
import xx.projectzero.linguistic.LWordPatternDictionnary;
import xx.projectzero.network.DatabaseManager;
import xx.projectzero.sourcewriter.ClassWriter;

public class ProgramCore extends Application
{
	private static ClassManager cm;
	private static ClassReloader cr;
	private static ClassWriter cw;
	private static IOCore io;
	private static SnippetsFilesManager sfm;
	private static EventManager em;
	private static Scheduler sc;
	private static MainViewController mainViewController;
	private static LWordDictionary wDictionary;
	private static LWordPatternDictionnary pDictionary;
	private static Logger logger;
	private static Console console;
	private static Cache ca;
	private static Archiver arch;
	private static ScriptManager sm;
	private static ErrorReporter er;
	private static ParamSaver pm;
	
	/** Getter for ClassManager main instance */
	public static ClassManager GetClassManager() { return cm; }
	/** Getter for ClassWriter main instance */
	public static ClassWriter GetClassWriter() { return cw; }
	/** Getter for ClassReloader main instance */
	public static ClassReloader GetClassReloader() { return cr; }
	/** Getter for SnippetsFilesManager main instance */
	public static SnippetsFilesManager GetSnippetsFilesManager() { return sfm; }
	/** Getter for EventManager main instance */
	public static EventManager GetEventManager() { return em; }
	/** Getter for Scheduler main instance */
	public static Scheduler GetScheduler() { return sc; }
	/** Getter for Word Dictionary main instance */
	public static LWordDictionary GetWordDictionary() { return wDictionary; }
	/** Getter for Pattern Dictionnary main instance */
	public static LWordPatternDictionnary GetPatternDictionary() { return pDictionary; }
	/** Getter for Logger main instance */
	public static Logger GetLogger() { return logger; }
	/** Getter for Console main instance */
	public static Console GetConsole() { return console; }
	/** Getter for Cache main instance */
	public static Cache GetCache() { return ca; }
	/** Getter for Archiver main instance */
	public static Archiver GetArchiver() { return arch; }
	/** Getter for ScriptManager main instance */
	public static ScriptManager GetScriptManager() { return sm; }
	/** Getter for ErrorReporter main instance */
	public static ErrorReporter GetErrorReporter() { return er; }
	/** Getter for ParamSaver main instance */
	public static ParamSaver GetParamSaver() { return pm; }
	
	/** Getter for MainViewController main instance */
	public static MainViewController GetMainViewController() { return mainViewController; }
	
	/** Getter for IO module main instance. */
	public static IOCore io() { return io; }
	
	/** Base paths of this project */
	public static final String BasePath = "./";
	public static final String BaseSnippetsPath = BasePath + "SnippetsFiles/";
	public static final String BaseArchivePath = BasePath + "archive/";
	public static final String BaseReportPath = BasePath + "reports/";
	public static final String BaseSRCPath = BasePath + "src/";
	public static final String BaseBINPath = BasePath + "bin/";
	
	/** Version String of the current version of the version. */
	public static final String Version = "1.2.0";
	
	/** Wether the init of the program is done or not. */
	public static boolean initDone = false;
	
	private Stage primaryStage;
	private BorderPane rootLayout;
	
	/** Returns the main stage. */
    public Stage getPrimaryStage() { return primaryStage; }
	
	/*
	 * Liste des dépendances :
	 * IO : aucune dépendance
	 * ClassReloader : depends on ClassManager
	 * ClassManager : no dependence
	 * ClassWriter : depends on ClassReloader
	 * Stacker : depends on EventManager
	 * Scheduler : depends on Stacker, EventManager
	 * EventManager : no dependence
	 * Logger : depends on EventManager
	 * DatabaseManager : depends on ParamSaver
	 * 
	 * linguistics dependences :
	 * LWordDictionnary : no dependence
	 * LWordPatternDictionnary : depends on LWordDictionnary
	 * 
	 * Error codes composition :
	 * 0x
	 * FF	Class Identifier
	 * 			00 : ProgramCore
	 * 			01 : ClassManager
	 * 			02 : ClassReloader
	 * 			03 : ClassWriter
	 * 			04 : IOCore
	 * 			05 : Utils
	 * 			06 : EventManager
	 * 			07 : Scheduler
	 * 			08 : 
	 * 			09 : SnippetsFilesManager
	 * 			0A : LearningClasses
	 * 			0B : Logger
	 * 			0C : Cache
	 * 			0D : Archiver
	 * 			0E : ScriptManager
	 * 			0F : DatabaseManager
	 * 			10 : ParamSaver
	 * F	Error Type :
	 * 			0 : exception
	 *			1 : critical
	 * F	Function Identifier
	 * FF	Error Identifier
	 */
    
    /*
     * Ports list :
     * 	- 34561 : Database Management
     */
	
	public static void main(String[] args) throws IOException
	{
		launch(args);
	}
	
	/** Method to init the root layout (main frame). */
	public void initRootLayout()
	{
        try
        {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ProgramCore.class.getResource("ui/RootLayout.fxml"));
            rootLayout = (BorderPane)loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) { e.printStackTrace(); }
    }

    /** Shows the person overview inside the root layout. */
    public void showOverview()
    {
        try
        {
            // Load overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ProgramCore.class.getResource("ui/MainView.fxml"));
            AnchorPane Overview = (AnchorPane)loader.load();
            mainViewController = (MainViewController)loader.getController();

            // Set overview into the center of root layout.
            rootLayout.setCenter(Overview);
        } catch (IOException e) { e.printStackTrace(); }
    }
	
	@Override
	public void start(Stage PrimaryStage) throws Exception
	{
		primaryStage = PrimaryStage;
        primaryStage.setTitle("ProjectZero " + Version);
        
        LogObject l = new LogObject("Launching ProjectZero v" + Version + "...", "ProgramCore", LogType.Action, Priority.High);
        LogObject l2 = new LogObject("Starting layouts", "ProgramCore", LogType.Action, Priority.High);
        
        initRootLayout();
        showOverview();
        
        LogObject l3 = new LogObject("Starting classes", "ProgramCore", LogType.Action, Priority.High);
        
        pm = new ParamSaver();
        er = new ErrorReporter();
        io = new IOCore();
        logger = new Logger();
        logger.log(l); logger.log(l2); logger.log(l3);
        arch = new Archiver();
        pm.Init();
        console = new Console();
        em = new EventManager();
        logger.init();
        ca = new Cache();
		sc = new Scheduler();
		cm = new ClassManager();
		cr = new ClassReloader();
		sfm = new SnippetsFilesManager();
		cw = new ClassWriter();
		sm = new ScriptManager();
		
		wDictionary = new LWordDictionary();
		pDictionary = new LWordPatternDictionnary();
		
		cr.init();
		
		initDone = true;
		logger.log(new LogObject("Main init ended", "ProgramCore", LogType.Info, Priority.Medium));
		
		sm.onstart();
		
		//Forcing addition of hand-writed class
		cm.AddClassToReference(TestClass.class);
	}
	
	public static boolean exiting = false;
	public static boolean readytoexit = false;
	
	@Override
	public void stop()
	{
		if(!exiting)
		{
			exiting = true;
			em.Fire("Exit");
			
			while(!readytoexit)
				try { Thread.sleep(100L); } catch (InterruptedException e) {}
		}
	}
}
