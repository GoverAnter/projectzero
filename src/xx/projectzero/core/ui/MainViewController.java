package xx.projectzero.core.ui;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import xx.projectzero.core.ProgramCore;

public class MainViewController
{
	public static enum MVTabs
	{
		Scheduler,
		Analyze,
		EventManager,
		Writer,
		Logger,
		Main,
		Console
	}
	
	@FXML private Text mainTextPanel;
	@FXML private TextField MainInputField;
	@FXML private Text SchedulerThreadText;
	@FXML private Text AnalyzeThreadText;
	@FXML private Text EventManagerText;
	@FXML private Text WriterText;
	@FXML private Text LoggerText;
	@FXML private Text ConsoleText;
	@FXML private TextField ConsoleInputField;
	
	@FXML
	private void initialize()
	{
	}
	
	@FXML
	public void ConsoleHandleKeyPressed(KeyEvent event)
	{
		if(event.getCode().equals(KeyCode.UP))
		{
			event.consume();
			ConsoleInputField.setText(ProgramCore.GetConsole().GetNextHistory());
			ConsoleInputField.selectRange(ConsoleInputField.getText().length(), ConsoleInputField.getText().length());
		}
		else if(event.getCode().equals(KeyCode.DOWN))
		{
			event.consume();
			ConsoleInputField.setText(ProgramCore.GetConsole().GetPreviousHistory());
			ConsoleInputField.selectRange(ConsoleInputField.getText().length(), ConsoleInputField.getText().length());
		}
	}
	
	@FXML
	private void TextInputed()
	{
		ProgramCore.io().println(MainInputField.getText());
		ProgramCore.GetScheduler().AddToInputStack(MainInputField.getText());
		MainInputField.setText("");
	}
	
	@FXML
	private void ConsoleInputed()
	{
		ProgramCore.io().printlntotab("$ " + ConsoleInputField.getText(), MVTabs.Console);
		ProgramCore.GetConsole().Input(ConsoleInputField.getText());
		ConsoleInputField.setText("");
	}
	
	public synchronized void addTextToTab(String text, MVTabs tab)
	{
		switch(tab)
		{
			case Analyze:
				AnalyzeThreadText.setText(AnalyzeThreadText.getText() + text);
				break;
			case Scheduler:
				SchedulerThreadText.setText(SchedulerThreadText.getText() + text);
				break;
			case EventManager:
				EventManagerText.setText(EventManagerText.getText() + text);
				break;
			case Writer:
				WriterText.setText(WriterText.getText() + text);
				break;
			case Logger:
				LoggerText.setText(LoggerText.getText() + text);
				break;
			case Console:
				ConsoleText.setText(ConsoleText.getText() + text);
				break;
			default:
				mainTextPanel.setText(mainTextPanel.getText() + text);
				break;
		}
	}
	
	public synchronized boolean isTabPanelEmpty(MVTabs tab)
	{
		switch(tab)
		{
			case Analyze:
				return AnalyzeThreadText.getText().equals("");
			case Scheduler:
				return SchedulerThreadText.getText().equals("");
			case EventManager:
				return EventManagerText.getText().equals("");
			case Writer:
				return WriterText.getText().equals("");
			case Logger:
				return LoggerText.getText().equals("");
			case Console:
				return ConsoleText.getText().equals("");
			default:
				return mainTextPanel.getText().equals("");
		}
	}
}
