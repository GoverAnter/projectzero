package xx.projectzero.exceptions;

/** Thrown when a value is present but should not be overriden. */
public class OverrideException extends Exception
{
	private static final long serialVersionUID = 1L;
}
