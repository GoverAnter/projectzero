package xx.projectzero.exceptions;

/** Thrown when an argument is not valid. */
public class InvalidArgumentException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	public InvalidArgumentException(String ArgumentName, String Value)
	{
		super("Argument \"" + ArgumentName + "\" is not valid. Current value : \"" + Value + "\"");
	}
}
