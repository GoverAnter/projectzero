package xx.projectzero.exceptions;

/** Thrown when a key is not registered. */
public class UnknownKeyException extends Exception
{
	private static final long serialVersionUID = 1L;
}
